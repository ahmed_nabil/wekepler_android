package com.wekepler.viewcontrollerslayer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wekepler.R;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;

import java.io.Serializable;

/**
 * Created by Mohammad Sayed on 8/17/2015.
 */
public abstract class BaseActivity extends AppCompatActivity implements OnModelResponseListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public abstract void initializeViews();


    protected void hideActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.hide();
    }

    protected void customizeActionBar() {
        Toolbar actionBar = (Toolbar) findViewById(R.id.action_bar);
        actionBar.setTitle("");
        setSupportActionBar(actionBar);
    }


    protected void underLineText(TextView textView) {
        SpannableString content = new SpannableString(textView.getText());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        textView.setText(content);
    }

    protected void toastLong(String string) {
        Toast.makeText(this, string, Toast.LENGTH_LONG).show();
    }

    protected void toastLong(int stringId) {
        Toast.makeText(this, stringId, Toast.LENGTH_LONG).show();
    }

    protected String getTextFromEditText(EditText editText, boolean trim) {
        String text = editText.getText().toString();
        if (trim)
            return text.trim();
        else
            return text;
    }

    public <T extends AppCompatActivity> void goToActivity(Class<T> activityClazz, boolean finish) {
        Intent intent = new Intent(this, activityClazz);
        startActivity(intent);
        if (finish)
            finish();
    }

    public <T extends AppCompatActivity> void goToActivity(Class<T> activityClazz, boolean finish, String objectTag, Object object) {
        Intent intent = new Intent(this, activityClazz);
        intent.putExtra(objectTag, (Serializable) object);
        startActivity(intent);
        if (finish)
            finish();
    }

    public <T extends AppCompatActivity> void goToActivity(Class<T> activityClazz, boolean finish, Bundle bundle) {
        Intent intent = new Intent(this, activityClazz);
        intent.putExtra(Constants.BUNDLE_TAG, bundle);
        startActivity(intent);
        if (finish)
            finish();
    }

    public <T extends AppCompatActivity> void goToActivityForResult(Class<T> activityClazz, int requestCode) {
        Intent intent = new Intent(this, activityClazz);
        startActivityForResult(intent, requestCode);
    }

    public <T extends AppCompatActivity> void goToActivityForResult(Class<T> activityClazz, int requestCode, Bundle bundle) {
        Intent intent = new Intent(this, activityClazz);
        intent.putExtra(Constants.BUNDLE_TAG, bundle);
        startActivityForResult(intent, requestCode);
    }
}
