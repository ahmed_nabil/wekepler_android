package com.wekepler.viewcontrollerslayer;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.wekepler.R;
import com.wekepler.modelslayer.RepostVideoModel;
import com.wekepler.viewcontrollerslayer.entities.Post;
import com.wekepler.viewcontrollerslayer.entities.parameters.RepostVideoParameters;
import com.wekepler.viewcontrollerslayer.entities.requests.RepostVideoRequest;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RepostVideoActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack;
    private TextView mTvPostVideo;
    private TextView mTvActionBarTitle;
    private TextView mTvVideoCaption;
    private EditText mEtComment;
    private ProgressBar mPbLoading;
    private Post mPost;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repost);
        initializeViews();
    }

    @Override
    public void initializeViews() {
        mIvBack = (ImageView) findViewById(R.id.iv_left_icon_action_bar);
        mTvPostVideo = (TextView) findViewById(R.id.ib_right_icon_action_bar);
        mTvActionBarTitle = (TextView) findViewById(R.id.tv_center_title);
        mTvVideoCaption = (TextView) findViewById(R.id.tv_video_caption);
        mEtComment = (EditText) findViewById(R.id.et_comment);
        mPbLoading = (ProgressBar) findViewById(R.id.pb_loading);

        getPost();
        mTvActionBarTitle.setText(mPost.getUser().getScreenName());
        mTvVideoCaption.setText(mPost.getVideoCaption());
        mTvPostVideo.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
    }

    private Post getPost() {
        Bundle bundle = getIntent().getBundleExtra(Constants.BUNDLE_TAG);
        mPost = (Post) bundle.getSerializable(Constants.POST_TAG);
        return mPost;
    }

    @Override
    public void onSuccess(String operationTag, Object... objects) {
        mPbLoading.setVisibility(View.INVISIBLE);
        if (operationTag.equals(Constants.REPOST_VIDEO_TAG)) {
            toastLong(R.string.video_reposted);
            onBackPressed();
        }
    }

    @Override
    public void onError(String operationTag, String error, Object... objects) {
        mPbLoading.setVisibility(View.INVISIBLE);
        toastLong(error);
    }

    @Override
    public void onClick(View v) {
        if (v == mIvBack)
            onBackPressed();
        else if (v == mTvPostVideo) {
            mPbLoading.setVisibility(View.VISIBLE);
            RepostVideoRequest repostVideoRequest = new RepostVideoRequest(mPost.getId(), getTextFromEditText(mEtComment, true));
            new RepostVideoModel(this, mPost, repostVideoRequest, this);
        }
    }
}
