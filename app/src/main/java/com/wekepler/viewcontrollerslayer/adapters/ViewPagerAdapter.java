package com.wekepler.viewcontrollerslayer.adapters;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;

import com.wekepler.R;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.MainActivity;
import com.wekepler.viewcontrollerslayer.SearchFriendsActivity;
import com.wekepler.viewcontrollerslayer.fragments.HomeTab;
import com.wekepler.viewcontrollerslayer.fragments.MessagesTab;
import com.wekepler.viewcontrollerslayer.fragments.ProfileTab;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Mohammad Sayed on 8/20/2015.
 */
public class ViewPagerAdapter extends SmartFragmentStatePagerAdapter {
    private ArrayList<String> mTitles; // This will Store the mTitles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    private MainActivity mMainActivity;
    private HomeTab mHomeTab;
    private ProfileTab mProfileTab;
    private MessagesTab mMessagesTab;

    // Build a Constructor and assign the passed Values to appropriate values in the class
    public ViewPagerAdapter(MainActivity mainActivity, FragmentManager fm, ArrayList<String> titles) {
        super(fm);
        mTitles = titles;
        mMainActivity = mainActivity;
        /*mImages = new ArrayList<>();
        mImages.add(R.drawable.home_normal);
        mImages.add(R.drawable.profile_normal);
        mImages.add(R.drawable.messages_drawable);*/
    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        if (position == 0) // if the position is 0 we are returning the First tab
        {
            if (mHomeTab == null)
                mHomeTab = new HomeTab();
            return mHomeTab;
        } else if (position == 1)            // As we are having 2 tabs if the position is now 0 it must be 1 so we are returning second tab
        {
            if (mProfileTab == null)
                mProfileTab = new ProfileTab();
            return mProfileTab;
        } else {
            if (mMessagesTab == null)
                mMessagesTab = new MessagesTab();
            return mMessagesTab;
        }

    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public String getPageTitle(int position) {
        return mTitles.get(position);
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        int count = mTitles.size();
        return count;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
        if (position == 0) {
            View.OnClickListener onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int vId = v.getId();
                    if (vId == R.id.iv_left_icon_action_bar) {
                        mMainActivity.goToActivity(SearchFriendsActivity.class, false);
                    } else if (vId == R.id.ib_right_icon_action_bar) {
                        displayTakingVideoDialog();
                    }
                }
            };
            mMainActivity.displayToolbarWekeplerLogo();
            mMainActivity.changeToolbarIconProperties(false, R.drawable.search_icon, onClickListener);
            mMainActivity.changeToolbarIconProperties(true, R.drawable.add_video_drawable, onClickListener);
            mMainActivity.setBaseListFragment(mHomeTab);

        } else if (position == 1) {
            View.OnClickListener onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int vId = v.getId();
                    if (vId == R.id.ib_right_icon_action_bar) {
                        displayTakingVideoDialog();
                    }
                }
            };
            mMainActivity.hideToolbarIcon(false);
            mMainActivity.displayToolbarWekeplerLogo();
            mMainActivity.changeToolbarIconProperties(true, R.drawable.add_video_drawable, onClickListener);
            mMainActivity.setBaseListFragment(mProfileTab);

        } else if (position == 2) {

            View.OnClickListener onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int vId = v.getId();
                    if (vId == R.id.ib_right_icon_action_bar) {

                    }
                }
            };
            mMainActivity.displayToolbarCenterTitle(mMainActivity.getString(R.string.messages));
            mMainActivity.hideToolbarIcon(false);
            mMainActivity.changeToolbarIconProperties(true, R.drawable.options_hover, onClickListener);
            mMainActivity.setBaseListFragment(mMessagesTab);
        }
    }

    private void displayTakingVideoDialog() {
        final Dialog dialog = new Dialog(mMainActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_video_dialog);
        ImageButton ivCamera = (ImageButton) dialog.findViewById(R.id.ib_camera);
        ImageButton ivGallery = (ImageButton) dialog.findViewById(R.id.ib_gallery);
        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startVideoRecording();
                dialog.dismiss();
            }
        });

        ivGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);//, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                intent.setType("video/*");
                mMainActivity.startActivityForResult(intent, Constants.VIDEO_PICK_REQUEST_CODE);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void startVideoRecording() {
        File mediaFile =
                new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
                        + "/wekepler_video_" + System.currentTimeMillis() + ".mp4");
        mMainActivity.setVideoFile(mediaFile);
        Uri videoUri = Uri.fromFile(mediaFile);
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
        mMainActivity.startActivityForResult(intent, Constants.VIDEO_CAPTURE_REQUEST_CODE);

    }
}
