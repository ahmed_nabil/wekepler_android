package com.wekepler.viewcontrollerslayer.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wekepler.R;
import com.wekepler.viewcontrollerslayer.MainActivity;
import com.wekepler.viewcontrollerslayer.MessageDetailsActivity;
import com.wekepler.viewcontrollerslayer.entities.Message;

import java.util.ArrayList;

/**
 * Created by Mohammad Sayed on 8/22/2015.
 */
public class MessageListAdapter extends BaseAdapter {

    private Activity mActivity;
    private ArrayList<Message> mItems;
    private boolean mIsDetailed;
    private DisplayImageOptions mUserProfileOptions = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(R.drawable.default_profile_picture)
            .showImageOnFail(R.drawable.default_profile_picture)
            .showImageOnLoading(R.drawable.default_profile_picture)
            .build();

    public MessageListAdapter(Activity activity, ArrayList<Message> items) {
        this.mActivity = activity;
        this.mItems = items;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final MessageItemHolder messageItemHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.message_cell, parent, false);
            messageItemHolder = new MessageItemHolder();
            messageItemHolder.llMessageLayout = (LinearLayout) convertView.findViewById(R.id.ll_message);
            messageItemHolder.ivProfilePicture = (ImageView) convertView.findViewById(R.id.iv_profile_picture);
            messageItemHolder.tvTitle = (TextView) convertView.findViewById(R.id.tv_screenName);
            messageItemHolder.tvUserName = (TextView) convertView.findViewById(R.id.tv_user_name);
            messageItemHolder.tvDate = (TextView) convertView.findViewById(R.id.tv_post_since);
            messageItemHolder.tvMessageBody = (TextView) convertView.findViewById(R.id.tv_body);
            convertView.setTag(messageItemHolder);

        } else {
            messageItemHolder = (MessageItemHolder) convertView.getTag();
        }


        final Message item = (Message) getItem(position);
        ImageLoader.getInstance().displayImage(item.getProfilePictureUrl(), messageItemHolder.ivProfilePicture, mUserProfileOptions);
        messageItemHolder.tvTitle.setText(item.getTitle());
        messageItemHolder.tvUserName.setText(item.getUserName());
        messageItemHolder.tvDate.setText(item.getDate());
        messageItemHolder.tvMessageBody.setText(item.getMessage());

        messageItemHolder.llMessageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) mActivity).goToActivity(MessageDetailsActivity.class, false);
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
