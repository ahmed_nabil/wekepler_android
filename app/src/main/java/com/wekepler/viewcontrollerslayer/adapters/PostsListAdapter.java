package com.wekepler.viewcontrollerslayer.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wekepler.R;
import com.wekepler.SharedPreferencesUtil;
import com.wekepler.modelslayer.BlockVideoModel;
import com.wekepler.modelslayer.DeleteVideoModel;
import com.wekepler.modelslayer.FavoriteVideoModel;
import com.wekepler.modelslayer.FollowUserModel;
import com.wekepler.modelslayer.LikeVideoModel;
import com.wekepler.modelslayer.ReportVideoModel;
import com.wekepler.viewcontrollerslayer.BaseActivity;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.FriendProfileActivity;
import com.wekepler.viewcontrollerslayer.MainActivity;
import com.wekepler.viewcontrollerslayer.RepostVideoActivity;
import com.wekepler.viewcontrollerslayer.SharePostDialog;
import com.wekepler.viewcontrollerslayer.VideoDetailsActivity;
import com.wekepler.viewcontrollerslayer.VideoViewerActivity;
import com.wekepler.viewcontrollerslayer.entities.Friend;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.Post;
import com.wekepler.viewcontrollerslayer.entities.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Mohammad Sayed on 8/22/2015.
 */
public class PostsListAdapter extends BaseAdapter {

    private Activity mActivity;
    private ArrayList<Post> mItems;
    private OnModelResponseListener mOnModelResponseListener;
    private String mUserName;

    private DisplayImageOptions mUserProfileOptions = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(R.drawable.default_profile_picture)
            .showImageOnFail(R.drawable.default_profile_picture)
            .showImageOnLoading(R.drawable.default_profile_picture)
            .build();

    private DisplayImageOptions mVideoThumbnailsOptions = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(R.drawable.video_background)
            .showImageOnFail(R.drawable.video_background)
            .showImageOnLoading(R.drawable.video_background)
            .build();

    private Date mCurrentDate;

    public PostsListAdapter(Activity activity, ArrayList<Post> items, OnModelResponseListener onModelResponseListener) {
        this.mActivity = activity;
        this.mItems = items;
        this.mOnModelResponseListener = onModelResponseListener;
        this.mCurrentDate = Calendar.getInstance().getTime();
        mUserName = SharedPreferencesUtil.getInstance(mActivity).getToken().getUserName();
    }

    protected View getConvertView(LayoutInflater inflater, ViewGroup viewGroup) {
        return inflater.inflate(R.layout.post_cell, viewGroup, false);
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final PostItemHolder postItemHolder;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            /*if (position == 2 || position == 3) {
                convertView = inflater.inflate(R.layout.repost_cell, parent, false);
                return convertView;
            } else*/
            convertView = getConvertView(inflater, parent);
            postItemHolder = new PostItemHolder();
            postItemHolder.ivProfilePicture = (ImageView) convertView.findViewById(R.id.iv_profile_picture);
            postItemHolder.tvTitle = (TextView) convertView.findViewById(R.id.tv_screenName);
            postItemHolder.tvUserName = (TextView) convertView.findViewById(R.id.tv_user_name);
            postItemHolder.ibFollow = (ImageButton) convertView.findViewById(R.id.ib_follow);                     //this
            postItemHolder.tvPostSince = (TextView) convertView.findViewById(R.id.tv_post_since);
            postItemHolder.ivVideoThumbnail = (ImageView) convertView.findViewById(R.id.iv_video_thumbnail);
            postItemHolder.ibPlay = (ImageButton) convertView.findViewById(R.id.ib_play);
            postItemHolder.tvVideoCaption = (TextView) convertView.findViewById(R.id.tv_video_caption);
            postItemHolder.ibOptions = (ImageButton) convertView.findViewById(R.id.ib_options);
            postItemHolder.tvViewersNumber = (TextView) convertView.findViewById(R.id.tv_viewers_number);
            postItemHolder.llLike = (LinearLayout) convertView.findViewById(R.id.ll_like);
            postItemHolder.ibLike = (ImageButton) convertView.findViewById(R.id.ib_like);
            postItemHolder.tvLikesNumber = (TextView) convertView.findViewById(R.id.tv_likes_number);
            postItemHolder.llComment = (LinearLayout) convertView.findViewById(R.id.ll_comment);
            postItemHolder.ibComment = (ImageButton) convertView.findViewById(R.id.ib_comment);
            postItemHolder.tvCommentsNumber = (TextView) convertView.findViewById(R.id.tv_comments_number);
            postItemHolder.llRepost = (LinearLayout) convertView.findViewById(R.id.ll_repost);
            postItemHolder.ibRepost = (ImageButton) convertView.findViewById(R.id.ib_repost);
            postItemHolder.tvRepostsNumber = (TextView) convertView.findViewById(R.id.tv_reposts_number);
            postItemHolder.llShare = (LinearLayout) convertView.findViewById(R.id.ll_share);
            postItemHolder.ibShare = (ImageButton) convertView.findViewById(R.id.ib_share);
            //postItemHolder.tvSharesNumber = (TextView) convertView.findViewById(R.id.tv_shares_number);

            convertView.setTag(postItemHolder);

        } else {
            postItemHolder = (PostItemHolder) convertView.getTag();
        }


        if (postItemHolder != null) {
            final Post post = (Post) getItem(position);
            final User user = post.getUser();

            ImageLoader.getInstance().displayImage(user.getProfilePictureUrl(), postItemHolder.ivProfilePicture, mUserProfileOptions);

            postItemHolder.tvTitle.setText(user.getScreenName());
            postItemHolder.tvUserName.setText(user.getUserName());

            if (!mUserName.equals(user.getUserName())) {

                postItemHolder.ivProfilePicture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Friend friend = getFriend(post);
                        ((MainActivity) mActivity).goToActivity(FriendProfileActivity.class, false, Constants.FRIEND_TAG, friend);
                    }
                });

                postItemHolder.ibFollow.setVisibility(View.VISIBLE);
                if (user.isFollowed())
                    postItemHolder.ibFollow.setImageResource(R.drawable.follow_icon_hover);
                else
                    postItemHolder.ibFollow.setImageResource(R.drawable.follow_icon_normal);
                postItemHolder.ibFollow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new FollowUserModel(mActivity, user, post, postItemHolder, mOnModelResponseListener);
                        if (user.isFollowed()) {
                            user.setFollowed(false);
                            postItemHolder.ibFollow.setImageResource(R.drawable.follow_icon_normal);
                        } else {
                            user.setFollowed(true);
                            postItemHolder.ibFollow.setImageResource(R.drawable.follow_icon_hover);
                        }
                    }
                });

                /*postItemHolder.ibOptions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PopupMenu popupMenu = new PopupMenu(mActivity, v);
                        popupMenu.getMenuInflater().inflate(R.menu.menu_other_users_video_settings, popupMenu.getMenu());
                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                String menuItemTitle = item.getTitle().toString();
                                if (menuItemTitle.equalsIgnoreCase(mActivity.getString(R.string.favorite))) {
                                    new FavoriteVideoModel(mActivity, post, mOnModelResponseListener);
                                    return true;
                                } else if (menuItemTitle.equalsIgnoreCase(mActivity.getString(R.string.report))) {
                                    new ReportVideoModel(mActivity, post, mOnModelResponseListener);
                                    return true;
                                } else if (menuItemTitle.equalsIgnoreCase(mActivity.getString(R.string.block))) {
                                    new BlockVideoModel(mActivity, post, mOnModelResponseListener);
                                    return true;
                                }
                                return false;
                            }
                        });
                        popupMenu.show();
                    }
                });*/
            } else {
                postItemHolder.ibFollow.setVisibility(View.INVISIBLE);
                postItemHolder.ivProfilePicture.setOnClickListener(null);
                /*postItemHolder.ibOptions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PopupMenu popupMenu = new PopupMenu(mActivity, v);
                        popupMenu.getMenuInflater().inflate(R.menu.menu_user_video_settings, popupMenu.getMenu());
                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                String menuItemTitle = item.getTitle().toString();
                                if (menuItemTitle.equalsIgnoreCase(mActivity.getString(R.string.favorite))) {
                                    new FavoriteVideoModel(mActivity, post, mOnModelResponseListener);
                                    return true;
                                } else if (menuItemTitle.equalsIgnoreCase(mActivity.getString(R.string.delete))) {
                                    new DeleteVideoModel(mActivity, post.getId(), mOnModelResponseListener);
                                    return true;
                                }
                                return false;
                            }
                        });
                        popupMenu.show();
                    }
                });*/
            }

            postItemHolder.ibOptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popupMenu = new PopupMenu(mActivity, v);
                    popupMenu.getMenuInflater().inflate(R.menu.menu_other_users_video_settings, popupMenu.getMenu());
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            String menuItemTitle = item.getTitle().toString();
                            if (menuItemTitle.equalsIgnoreCase(mActivity.getString(R.string.favorite))) {
                                new FavoriteVideoModel(mActivity, post, mOnModelResponseListener);
                                return true;
                            } else if (menuItemTitle.equalsIgnoreCase(mActivity.getString(R.string.report))) {
                                new ReportVideoModel(mActivity, post, mOnModelResponseListener);
                                return true;
                            } else if (menuItemTitle.equalsIgnoreCase(mActivity.getString(R.string.block))) {
                                new BlockVideoModel(mActivity, post, mOnModelResponseListener);
                                return true;
                            } else if (menuItemTitle.equalsIgnoreCase(mActivity.getString(R.string.delete))) {
                                new DeleteVideoModel(mActivity, post.getId(), mOnModelResponseListener);
                                return true;
                            }
                            return false;
                        }
                    });
                    popupMenu.show();
                }
            });

            postItemHolder.tvPostSince.setText(convertDateToText(post.getPostSince()));


            ImageLoader.getInstance().displayImage(post.getVideoThumbnailUrl(), postItemHolder.ivVideoThumbnail, mVideoThumbnailsOptions);

            postItemHolder.ibPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, VideoViewerActivity.class);
                    intent.putExtra(Constants.VIDEO_PATH_TAG, post.getVideoUrl());
                    mActivity.startActivity(intent);
                }
            });


            postItemHolder.tvVideoCaption.setText(post.getVideoCaption());

            postItemHolder.tvViewersNumber.setText("" + post.getViewersNumber());

            if (post.isLiked())
                postItemHolder.ibLike.setImageResource(R.drawable.like_hover);
            else
                postItemHolder.ibLike.setImageResource(R.drawable.like_normal);

            postItemHolder.llLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new LikeVideoModel(mActivity, post, postItemHolder, mOnModelResponseListener);
                    if (post.isLiked()) {
                        post.setIsLiked(false);
                        postItemHolder.ibLike.setImageResource(R.drawable.like_normal);
                        post.setLikesNumber(post.getLikesNumber() - 1);
                        postItemHolder.tvLikesNumber.setText("" + post.getLikesNumber());

                    } else {
                        post.setIsLiked(true);
                        postItemHolder.ibLike.setImageResource(R.drawable.like_hover);
                        post.setLikesNumber(post.getLikesNumber() + 1);
                        postItemHolder.tvLikesNumber.setText("" + post.getLikesNumber());
                    }
                }
            });
            postItemHolder.tvLikesNumber.setText("" + post.getLikesNumber());


            if (post.isCommented())
                postItemHolder.ibComment.setImageResource(R.drawable.comments_hover);
            else
                postItemHolder.ibComment.setImageResource(R.drawable.comments_normal);

            postItemHolder.llComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.POST_TAG, post);
                    ((MainActivity) mActivity).goToActivityForResult(VideoDetailsActivity.class,
                            Constants.VIEW_VIDEO_DETAILS_REQUEST_CODE, bundle);
                }
            });
            postItemHolder.tvCommentsNumber.setText("" + post.getCommentsNumber());

            if (post.isReposted()) {
                postItemHolder.ibRepost.setImageResource(R.drawable.repost_hover);
                //postItemHolder.ibRepost.setEnabled(false);
            } else {
                postItemHolder.ibRepost.setImageResource(R.drawable.repost_normal);
                //postItemHolder.ibRepost.setEnabled(true);
            }

            postItemHolder.llRepost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //displayRepostDialog(mActivity, post, postItemHolder);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.POST_TAG, post);
                    ((BaseActivity) mActivity).goToActivity(RepostVideoActivity.class, false, bundle);
                }
            });
            postItemHolder.tvRepostsNumber.setText("" + post.getRepostsNumber());

            /*if (post.isShared())
                postItemHolder.ibShare.setImageResource(R.drawable.share_hover);
            else
                postItemHolder.ibShare.setImageResource(R.drawable.share_normal);*/
            postItemHolder.llShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayShareDialog(post);
                }
            });
            //postItemHolder.tvSharesNumber.setText("" + post.getSharesNumber());
        }

        return convertView;
    }

    private String convertDateToText(String dateString) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            Date uploadDate = simpleDateFormat.parse(dateString);

            long timeDifference = mCurrentDate.getTime() - uploadDate.getTime();
            long days = TimeUnit.MILLISECONDS.toDays(timeDifference);
            if (days >= 365) {
                return days / 365 + " y";
            } else if (days > 30) {
                return days / 31 + " mon";
            } else if (days > 0)
                return days + " d";
            else {
                long hours = TimeUnit.MILLISECONDS.toHours(timeDifference);
                if (hours >= 1)
                    return hours + " h";
                else {
                    long minutes = TimeUnit.MILLISECONDS.toMinutes(timeDifference);
                    if (minutes > 0)
                        return minutes + " min";
                    else {
                        long seconds = TimeUnit.MILLISECONDS.toSeconds(timeDifference);
                        return seconds + " sec";
                    }
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setItems(ArrayList<Post> items) {
        this.mItems = items;
        notifyDataSetChanged();
    }

    private void displayShareDialog(Post post) {
        SharePostDialog dialog = new SharePostDialog(mActivity, post);
        dialog.show();
    }

    /*private void displayRepostDialog(Context context, final Post post, final PostItemHolder postItemHolder) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context)
                .setTitle(R.string.repost_video_title)
                .setMessage(R.string.repost_video_message)
                .setPositiveButton(R.string.repost, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        RepostVideoRequest repostVideoRequest = new RepostVideoRequest(post.getId());
                        new RepostVideoModel(mActivity, post, postItemHolder, repostVideoRequest, mOnModelResponseListener);
                        post.setIsReposted(true);
                        postItemHolder.ibRepost.setImageResource(R.drawable.repost_hover);
                        post.setRepostsNumber(post.getRepostsNumber() + 1);
                        postItemHolder.tvRepostsNumber.setText("" + (post.getRepostsNumber()));

                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        Dialog dialog = alertDialog.create();
        dialog.show();
    }*/

    private Friend getFriend(Post post) {
        Friend friend = new Friend(post.getUser());
        return friend;
    }

}
