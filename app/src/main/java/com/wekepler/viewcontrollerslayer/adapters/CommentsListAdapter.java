package com.wekepler.viewcontrollerslayer.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wekepler.R;
import com.wekepler.SharedPreferencesUtil;
import com.wekepler.modelslayer.DeleteCommentModel;
import com.wekepler.modelslayer.DeleteVideoModel;
import com.wekepler.serviceslayer.apis.DeleteCommentApiService;
import com.wekepler.viewcontrollerslayer.BaseActivity;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.EditCommentActivity;
import com.wekepler.viewcontrollerslayer.entities.Comment;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Mohammad Sayed on 8/22/2015.
 */
public class CommentsListAdapter extends BaseAdapter {

    private Activity mActivity;
    private ArrayList<Comment> mItems;
    private boolean isVideoDetails;
    private String mUserName;
    private Date mCurrentDate;
    private OnModelResponseListener mOnModelResponseListener;
    private DisplayImageOptions mUserProfileOptions = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(R.drawable.default_profile_picture)
            .showImageOnFail(R.drawable.default_profile_picture)
            .showImageOnLoading(R.drawable.default_profile_picture)
            .build();
    private final int REQUEST_CODE_EDIT_COMMENT = 11;

    public CommentsListAdapter(Activity activity, ArrayList<Comment> items, boolean isVideoDetails, OnModelResponseListener onModelResponseListener) {
        this.mActivity = activity;
        this.mItems = items;
        this.isVideoDetails = isVideoDetails;
        this.mOnModelResponseListener = onModelResponseListener;
        this.mCurrentDate = Calendar.getInstance().getTime();
        mUserName = SharedPreferencesUtil.getInstance(mActivity).getToken().getUserName();

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final CommentItemHolder commentItemHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.comment_cell, parent, false);
            commentItemHolder = new CommentItemHolder();
            commentItemHolder.ivProfilePicture = (ImageView) convertView.findViewById(R.id.iv_profile_picture);
            commentItemHolder.tvScreenName = (TextView) convertView.findViewById(R.id.tv_screenName);
            commentItemHolder.tvUserName = (TextView) convertView.findViewById(R.id.tv_user_name);
            commentItemHolder.tvPostSince = (TextView) convertView.findViewById(R.id.tv_post_since);
            commentItemHolder.tvBody = (TextView) convertView.findViewById(R.id.tv_body);
            commentItemHolder.ibOptions = (ImageButton) convertView.findViewById(R.id.ib_options);
            commentItemHolder.ibLike = (ImageButton) convertView.findViewById(R.id.ib_like);
            commentItemHolder.tvLikesNumber = (TextView) convertView.findViewById(R.id.tv_likes_number);

            convertView.setTag(commentItemHolder);

        } else {
            commentItemHolder = (CommentItemHolder) convertView.getTag();
        }

        if (!isVideoDetails && position == 0)
            convertView.setBackgroundResource(R.drawable.rounded_top_two_corners_white);
        else
            convertView.setBackgroundResource(R.color.white);

        final Comment comment = (Comment) getItem(position);
        User user = comment.getUser();
        ImageLoader.getInstance().displayImage(user.getProfilePictureUrl(), commentItemHolder.ivProfilePicture, mUserProfileOptions);
        commentItemHolder.tvScreenName.setText(user.getScreenName());
        commentItemHolder.tvUserName.setText(user.getUserName());
        commentItemHolder.tvPostSince.setText(convertDateToText(comment.getPostSince()));
        commentItemHolder.tvBody.setText(comment.getBody());

        commentItemHolder.ibOptions.setOnClickListener(new View.OnClickListener() {
                                                           @Override
                                                           public void onClick(View v) {

                                                               PopupMenu popupMenu = new PopupMenu(mActivity, v);
                                                               if (mUserName.equals(comment.getUser().getUserName())) {
                                                                   popupMenu.getMenuInflater().inflate(R.menu.menu_user_comment_options, popupMenu.getMenu());
                                                                   popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                                                       @Override
                                                                       public boolean onMenuItemClick(MenuItem item) {
                                                                           String menuItemTitle = item.getTitle().toString();
                                                                           if (menuItemTitle.equalsIgnoreCase(mActivity.getString(R.string.delete))) {
                                                                               new DeleteCommentModel(mActivity, comment.getId(), mOnModelResponseListener);
                                                                               return true;
                                                                           } else if (menuItemTitle.equalsIgnoreCase(mActivity.getString(R.string.edit))) {
                                                                               Bundle bundle = new Bundle();
                                                                               bundle.putSerializable(Constants.COMMENT_TAG, comment);
                                                                               ((BaseActivity) mActivity).goToActivityForResult(EditCommentActivity.class, REQUEST_CODE_EDIT_COMMENT, bundle);
                                                                               return true;
                                                                           }
                                                                           return false;
                                                                       }
                                                                   });
                                                                   popupMenu.show();

                                                               } else {
                                                                   popupMenu.getMenuInflater().inflate(R.menu.menu_other_users_comment_options, popupMenu.getMenu());
                                                                   popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                                                       @Override
                                                                       public boolean onMenuItemClick(MenuItem item) {
                                                                           String menuItemTitle = item.getTitle().toString();
                                                                           if (menuItemTitle.equalsIgnoreCase(mActivity.getString(R.string.delete))) {
                                                                               new DeleteCommentModel(mActivity, comment.getId(), mOnModelResponseListener);
                                                                               return true;
                                                                           } else if (menuItemTitle.equalsIgnoreCase(mActivity.getString(R.string.block))) {

                                                                               return true;
                                                                           }
                                                                           return false;
                                                                       }
                                                                   });
                                                                   popupMenu.show();
                                                               }
                                                           }
                                                       }

        );

        if (comment.isCanLike())

        {
            if (comment.isLiked()) {
                commentItemHolder.ibLike.setImageResource(R.drawable.like_hover);
            } else {
                commentItemHolder.ibLike.setImageResource(R.drawable.like_normal);
            }

            commentItemHolder.ibLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (comment.isLiked()) {
                        commentItemHolder.ibLike.setImageResource(R.drawable.like_normal);
                        comment.setLikesNumber((comment.getLikesNumber() - 1));
                        commentItemHolder.tvLikesNumber.setText("" + comment.getLikesNumber());
                        comment.setIsLiked(false);

                    } else {
                        commentItemHolder.ibLike.setImageResource(R.drawable.like_hover);
                        comment.setLikesNumber((comment.getLikesNumber() + 1));
                        commentItemHolder.tvLikesNumber.setText("" + comment.getLikesNumber());
                        comment.setIsLiked(true);
                    }
                }
            });
            commentItemHolder.tvLikesNumber.setText("" + comment.getLikesNumber());
        } else

        {
            commentItemHolder.ibLike.setImageResource(R.drawable.like_normal);
            commentItemHolder.tvLikesNumber.setVisibility(View.INVISIBLE);
        }


        return convertView;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setItems(ArrayList<Comment> items) {
        this.mItems = items;
    }

    public void addItem(Comment comment) {
        mItems.add(comment);
        notifyDataSetChanged();
    }

    private String convertDateToText(String dateString) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            Date uploadDate = simpleDateFormat.parse(dateString);

            long timeDifference = mCurrentDate.getTime() - uploadDate.getTime();
            long days = TimeUnit.MILLISECONDS.toDays(timeDifference);
            if (days >= 365) {
                return days / 365 + " y";
            } else if (days > 30) {
                return days / 31 + " mon";
            } else if (days > 0)
                return days + " d";
            else {
                long hours = TimeUnit.MILLISECONDS.toHours(timeDifference);
                if (hours >= 1)
                    return hours + " h";
                else {
                    long minutes = TimeUnit.MILLISECONDS.toMinutes(timeDifference);
                    if (minutes > 0)
                        return minutes + " min";
                    else {
                        long seconds = TimeUnit.MILLISECONDS.toSeconds(timeDifference);
                        return seconds + " sec";
                    }
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
