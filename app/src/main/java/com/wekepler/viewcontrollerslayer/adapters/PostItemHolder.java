package com.wekepler.viewcontrollerslayer.adapters;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

/**
 * Created by Mohammad Sayed on 8/22/2015.
 */
public class PostItemHolder {
    public ImageView ivProfilePicture;
    public TextView tvTitle;
    public TextView tvUserName;
    public ImageButton ibFollow;
    public TextView tvPostSince;
    public ImageButton ibPlay;
    public ImageView ivVideoThumbnail;
    public TextView tvVideoCaption;
    public ImageButton ibOptions;
    public TextView tvViewersNumber;
    public LinearLayout llLike;
    public ImageButton ibLike;
    public TextView tvLikesNumber;
    public LinearLayout llComment;
    public ImageButton ibComment;
    public TextView tvCommentsNumber;
    public LinearLayout llRepost;
    public ImageButton ibRepost;
    public TextView tvRepostsNumber;
    public LinearLayout llShare;
    public ImageButton ibShare;
    //public TextView tvSharesNumber;

}
