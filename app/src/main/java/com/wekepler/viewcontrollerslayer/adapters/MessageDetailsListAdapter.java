package com.wekepler.viewcontrollerslayer.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wekepler.R;
import com.wekepler.viewcontrollerslayer.entities.Message;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Mohammad Sayed on 8/22/2015.
 */
public class MessageDetailsListAdapter extends BaseAdapter {

    private Activity mActivity;
    private ArrayList<Message> mItems;
    private Date mCurrentDate;
    private DisplayImageOptions mUserProfileOptions = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(R.drawable.default_profile_picture)
            .showImageOnFail(R.drawable.default_profile_picture)
            .showImageOnLoading(R.drawable.default_profile_picture)
            .build();

    public MessageDetailsListAdapter(Activity activity, ArrayList<Message> items) {
        this.mActivity = activity;
        this.mItems = items;
        mCurrentDate = new Date();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final MessageItemHolder messageItemHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.message_detailes_cell, parent, false);
            messageItemHolder = new MessageItemHolder();
            messageItemHolder.llMessageLayout = (LinearLayout) convertView.findViewById(R.id.ll_message);
            messageItemHolder.ivProfilePicture = (ImageView) convertView.findViewById(R.id.iv_profile_picture);
            messageItemHolder.tvTitle = (TextView) convertView.findViewById(R.id.tv_screenName);
            messageItemHolder.tvUserName = (TextView) convertView.findViewById(R.id.tv_user_name);
            messageItemHolder.tvDate = (TextView) convertView.findViewById(R.id.tv_post_since);
            messageItemHolder.tvMessageBody = (TextView) convertView.findViewById(R.id.tv_body);
            convertView.setTag(messageItemHolder);

        } else {
            messageItemHolder = (MessageItemHolder) convertView.getTag();
        }


        final Message item = (Message) getItem(position);
        //ImageLoader.getInstance().displayImage("", messageItemHolder.ivProfilePicture, mUserProfileOptions);
        //messageItemHolder.tvTitle.setText(item.getTitle());
        //messageItemHolder.tvUserName.setText(item.getUserName());
        messageItemHolder.tvDate.setText(convertTextToDate(item.getSentDate()));
        messageItemHolder.tvMessageBody.setText(item.getMessage());

        return convertView;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setItems(ArrayList<Message> items) {
        this.mItems = items;
        notifyDataSetChanged();
    }

    public void addItem(Message message) {
        this.mItems.add(message);
        notifyDataSetChanged();
    }

    public String convertTextToDate(String dateString) {

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ", Locale.UK);
            Date uploadDate = simpleDateFormat.parse(dateString);

            long timeDifference = mCurrentDate.getTime() - uploadDate.getTime();
            long days = TimeUnit.MILLISECONDS.toDays(timeDifference);
            if (days >= 365) {
                return days / 365 + " y";
            } else if (days > 30) {
                return days / 31 + " mon";
            } else if (days > 0)
                return days + " d";
            else {
                long hours = TimeUnit.MILLISECONDS.toHours(timeDifference);
                if (hours >= 1)
                    return hours + " h";
                else {
                    long minutes = TimeUnit.MILLISECONDS.toMinutes(timeDifference);
                    if (minutes > 0)
                        return minutes + " min";
                    else {
                        long seconds = TimeUnit.MILLISECONDS.toSeconds(timeDifference);
                        return seconds + " sec";
                    }
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

}
