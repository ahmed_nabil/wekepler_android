package com.wekepler.viewcontrollerslayer.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.wekepler.R;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.FriendProfileActivity;
import com.wekepler.viewcontrollerslayer.SearchFriendsActivity;
import com.wekepler.viewcontrollerslayer.entities.Friend;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Mohammad Sayed on 8/22/2015.
 */
public class SearchFriendsListAdapter extends BaseAdapter {

    private Activity mActivity;
    private ArrayList<Friend> mItems;
    private ArrayList<Friend> mPopulationList;

    public SearchFriendsListAdapter(Activity activity, ArrayList<Friend> items) {
        this.mActivity = activity;
        this.mItems = items;
        mPopulationList = new ArrayList<>();
        mPopulationList.addAll(items);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final SearchFriendItemHolder searchFriendItemHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.friend_cell, parent, false);
            searchFriendItemHolder = new SearchFriendItemHolder();
            searchFriendItemHolder.llFriendLayout = (LinearLayout) convertView.findViewById(R.id.ll_friend);
            searchFriendItemHolder.ivProfilePicture = (ImageView) convertView.findViewById(R.id.iv_profile_picture);
            searchFriendItemHolder.tvTitle = (TextView) convertView.findViewById(R.id.tv_screenName);
            searchFriendItemHolder.tvUserName = (TextView) convertView.findViewById(R.id.tv_user_name);
            searchFriendItemHolder.ibFollow = (ImageButton) convertView.findViewById(R.id.ib_follow);
            convertView.setTag(searchFriendItemHolder);

        } else {
            searchFriendItemHolder = (SearchFriendItemHolder) convertView.getTag();
        }

        final Friend item = (Friend) getItem(position);
        ImageLoader.getInstance().displayImage(item.getProfilePictureUrl(), searchFriendItemHolder.ivProfilePicture);
        searchFriendItemHolder.tvTitle.setText(item.getScreenName());
        searchFriendItemHolder.tvUserName.setText(item.getUserName());

        if (item.isFollowed())
            searchFriendItemHolder.ibFollow.setImageResource(R.drawable.big_follow_hover);
        else
            searchFriendItemHolder.ibFollow.setImageResource(R.drawable.big_follow_normal);


        searchFriendItemHolder.llFriendLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SearchFriendsActivity) mActivity).goToActivity(FriendProfileActivity.class, false, Constants.FRIEND_TAG, item);
            }
        });

        searchFriendItemHolder.ibFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.isFollowed()) {
                    searchFriendItemHolder.ibFollow.setImageResource(R.drawable.big_follow_normal);
                    item.setIsFollowed(false);
                } else {
                    searchFriendItemHolder.ibFollow.setImageResource(R.drawable.big_follow_hover);
                    item.setIsFollowed(true);
                }
            }
        });


        return convertView;
    }

    @Override
    public int getCount() {
        return mPopulationList.size();
    }

    @Override
    public Object getItem(int position) {
        return mPopulationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void filter(String string) {
        string = string.toLowerCase(Locale.getDefault());
        mPopulationList.clear();
        if (string.trim().equals(""))
            mPopulationList.addAll(mItems);
        else {
            for (Friend friend : mItems) {
                if (friend.getScreenName().trim().toLowerCase(Locale.getDefault()).contains(string)
                        || friend.getUserName().trim().toLowerCase(Locale.getDefault()).contains(string))
                    mPopulationList.add(friend);
            }
        }
        notifyDataSetChanged();
    }
}
