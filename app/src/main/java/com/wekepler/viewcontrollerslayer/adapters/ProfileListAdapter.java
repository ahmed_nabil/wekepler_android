package com.wekepler.viewcontrollerslayer.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wekepler.R;
import com.wekepler.viewcontrollerslayer.BaseActivity;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.RepostVideoActivity;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.Post;
import com.wekepler.viewcontrollerslayer.entities.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Mohammad Sayed on 8/22/2015.
 */
public class ProfileListAdapter extends PostsListAdapter {

    public ProfileListAdapter(Activity activity, ArrayList<Post> items, OnModelResponseListener onModelResponseListener) {
        super(activity, items, onModelResponseListener);
    }

    @Override
    protected View getConvertView(LayoutInflater inflater, ViewGroup viewGroup) {
        return inflater.inflate(R.layout.profile_post_cell, viewGroup, false);
    }
}
