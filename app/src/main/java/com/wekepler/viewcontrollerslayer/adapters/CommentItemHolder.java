package com.wekepler.viewcontrollerslayer.adapters;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Mohammad Sayed on 8/22/2015.
 */
public class CommentItemHolder {
    public ImageView ivProfilePicture;
    public TextView tvScreenName;
    public TextView tvUserName;
    public ImageButton ibOptions;
    public TextView tvPostSince;
    public TextView tvBody;
    public ImageButton ibLike;
    public TextView tvLikesNumber;
}
