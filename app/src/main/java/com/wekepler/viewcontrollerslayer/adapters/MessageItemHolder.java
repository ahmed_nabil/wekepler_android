package com.wekepler.viewcontrollerslayer.adapters;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Mohammad Sayed on 8/22/2015.
 */
public class MessageItemHolder {

    public LinearLayout llMessageLayout;
    public ImageView ivProfilePicture;
    public TextView tvTitle;
    public TextView tvUserName;
    public TextView tvDate;
    public TextView tvMessageBody;
}
