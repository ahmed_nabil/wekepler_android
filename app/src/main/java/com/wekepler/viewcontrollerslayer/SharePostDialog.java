package com.wekepler.viewcontrollerslayer;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.plus.PlusShare;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;
import com.wekepler.R;
import com.wekepler.viewcontrollerslayer.entities.Post;

/**
 * Created by Mohammad Sayed on 10/2/2015.
 */
public class SharePostDialog extends Dialog implements View.OnClickListener {


    private ImageButton mIbFacebook;
    private ImageButton mIbTwitter;
    private ImageButton mIbGoogle;
    private ImageButton mIbWhatsapp;
    private ImageButton mIbEmail;
    private ImageButton mIbCopyLink;

    private Post mPost;
    private CallbackManager mCallbackManager;
    private Activity mActivity;
    private ShareDialog mShareDialog;

    public SharePostDialog(Activity activity, Post post) {
        super(activity);
        this.mActivity = activity;
        this.mPost = post;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.share_post_dialog);
        initializeViews();
        mCallbackManager = CallbackManager.Factory.create();
        mShareDialog = new ShareDialog(mActivity);
        mShareDialog.registerCallback(mCallbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                SharePostDialog.this.dismiss();
                toast(mActivity.getString(R.string.share_successful));
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
                toast(mActivity.getString(R.string.share_error));
            }
        });
    }

    private void initializeViews() {
        mIbFacebook = (ImageButton) findViewById(R.id.ib_share_facebook);
        mIbTwitter = (ImageButton) findViewById(R.id.ib_share_twitter);
        mIbGoogle = (ImageButton) findViewById(R.id.ib_share_google);
        mIbWhatsapp = (ImageButton) findViewById(R.id.ib_share_whatsapp);
        mIbEmail = (ImageButton) findViewById(R.id.ib_share_email);
        mIbCopyLink = (ImageButton) findViewById(R.id.ib_share_copy_link);

        mIbFacebook.setOnClickListener(this);
        mIbTwitter.setOnClickListener(this);
        mIbGoogle.setOnClickListener(this);
        mIbWhatsapp.setOnClickListener(this);
        mIbEmail.setOnClickListener(this);
        mIbCopyLink.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mIbFacebook) {
            ShareLinkContent content = new ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse(mPost.getVideoUrl()))
                    .setContentTitle(mPost.getVideoCaption())
                    .setImageUrl(Uri.parse(mPost.getVideoThumbnailUrl()))
                    .setContentDescription(mPost.getVideoCaption())
                    .build();
            if (ShareDialog.canShow(ShareLinkContent.class)) {
                mShareDialog.show(content);
            }
        } else if (v == mIbTwitter) {
            TweetComposer.Builder builder = new TweetComposer.Builder(mActivity)
                    .text(mActivity.getString(R.string.check_video) + mPost.getVideoUrl())
                    .image(Uri.parse(mPost.getVideoThumbnailUrl()));
            builder.show();
        } else if (v == mIbGoogle) {
            Intent shareIntent = new PlusShare.Builder(mActivity)
                    .setType("text/plain")
                    .setText(mActivity.getString(R.string.check_video))
                    .setContentUrl(Uri.parse(mPost.getVideoUrl()))
                    .getIntent();

            mActivity.startActivityForResult(shareIntent, 0);

        } else if (v == mIbWhatsapp) {
            /**
             * Show share dialog BOTH image and text
             */
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            //Target whatsapp:
            shareIntent.setPackage("com.whatsapp");
            //Add text and then Image URI
            shareIntent.putExtra(Intent.EXTRA_TEXT, mActivity.getString(R.string.check_video) + mPost.getVideoUrl());
            shareIntent.setType("text/plain");
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            try {
                mActivity.startActivity(shareIntent);
            } catch (android.content.ActivityNotFoundException ex) {
                toast(mActivity.getString(R.string.whatsapp_not_installed));
            }
        } else if (v == mIbEmail) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            //intent.putExtra(Intent.EXTRA_EMAIL, "emailaddress@emailaddress.com");
            intent.putExtra(Intent.EXTRA_SUBJECT, mActivity.getString(R.string.share_email_subject));
            intent.putExtra(Intent.EXTRA_TEXT, mActivity.getString(R.string.check_video) + mPost.getVideoUrl());
            mActivity.startActivity(Intent.createChooser(intent, "Send Email"));
        } else if (v == mIbCopyLink) {
            int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                android.text.ClipboardManager clipboard = (android.text.ClipboardManager) mActivity
                        .getSystemService(Context.CLIPBOARD_SERVICE);
                clipboard.setText(mPost.getVideoUrl());
            } else {
                ClipboardManager clipboard = (ClipboardManager) mActivity
                        .getSystemService(Context.CLIPBOARD_SERVICE);
                android.content.ClipData clip = ClipData
                        .newPlainText("Video Url", mPost.getVideoUrl());
                clipboard.setPrimaryClip(clip);
            }
        }
    }

    private void toast(String message) {
        Toast.makeText(mActivity, message, Toast.LENGTH_LONG).show();
    }
}
