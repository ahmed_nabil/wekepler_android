package com.wekepler.viewcontrollerslayer;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.wekepler.R;
import com.wekepler.modelslayer.EditCommentModel;
import com.wekepler.viewcontrollerslayer.entities.Comment;
import com.wekepler.viewcontrollerslayer.entities.parameters.EditCommentParameters;

public class EditCommentActivity extends BaseActivity implements View.OnClickListener {

    private EditText mEtComment;
    private TextView mTvSave;
    private ImageView mIvBack;
    private Comment mComment;
    private ProgressBar mPbLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_comment);
        initializeViews();
    }

    @Override
    public void initializeViews() {
        mEtComment = (EditText) findViewById(R.id.et_comment);
        mTvSave = (TextView) findViewById(R.id.tv_right_icon_action_bar);
        mIvBack = (ImageView) findViewById(R.id.iv_left_icon_action_bar);
        mPbLoading = (ProgressBar) findViewById(R.id.pb_loading);
        getComment();
        mTvSave.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
    }

    private void getComment() {
        if (getIntent() != null && getIntent().hasExtra(Constants.BUNDLE_TAG)) {
            mComment = (Comment) getIntent().getBundleExtra(Constants.BUNDLE_TAG).getSerializable(Constants.COMMENT_TAG);
        }
    }

    @Override
    public void onSuccess(String operationTag, Object... objects) {
        mPbLoading.setVisibility(View.INVISIBLE);
        if (operationTag.equals(Constants.EDIT_COMMENT_TAG)) {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onError(String operationTag, String error, Object... objects) {
        mPbLoading.setVisibility(View.INVISIBLE);
        toastLong(error);
    }

    @Override
    public void onClick(View v) {
        if (v == mTvSave) {
            String comment = getTextFromEditText(mEtComment, true);
            if (comment.equals("")) {
                toastLong(R.string.empty_comment);
                return;
            }
            mPbLoading.setVisibility(View.VISIBLE);
            EditCommentParameters editCommentParameters = new EditCommentParameters(comment);
            new EditCommentModel(this, mComment.getId(), editCommentParameters, this);
        } else if (v == mIvBack) {
            onBackPressed();
        }
    }
}
