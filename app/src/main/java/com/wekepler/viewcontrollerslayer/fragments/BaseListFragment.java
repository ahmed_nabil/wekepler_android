package com.wekepler.viewcontrollerslayer.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;

import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;

/**
 * Created by Mohammad Sayed on 9/12/2015.
 */
public abstract class BaseListFragment extends ListFragment implements OnModelResponseListener {
}
