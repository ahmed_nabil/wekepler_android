package com.wekepler.viewcontrollerslayer.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wekepler.R;
import com.wekepler.viewcontrollerslayer.MainActivity;
import com.wekepler.viewcontrollerslayer.adapters.MessageListAdapter;
import com.wekepler.viewcontrollerslayer.entities.Message;

import java.util.ArrayList;

/**
 * Created by Mohammad Sayed on 8/20/2015.
 */
public class MessagesTab extends BaseListFragment {

    private MainActivity mMainActivity;
    private ArrayList<Message> messagesList;
    private MessageListAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mMainActivity = (MainActivity) getActivity();
        View view = inflater.inflate(R.layout.fragment_messages, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getMessagesList();
        adapter = new MessageListAdapter(getActivity(), messagesList);
        getListView().setAdapter(adapter);
    }

    private ArrayList<Message> getMessagesList() {
        messagesList = new ArrayList<>();
        Message message = new Message();
        message.setProfilePictureUrl("drawable://" + R.drawable.profile_picture);
        message.setTitle("Creative Channel");
        message.setUserName("esawy_rah");
        message.setDate("May 24");
        message.setMessage("Lorem ipsum dolor sit amet, adipisicing elit, sed do eiusmod tempor");
        messagesList.add(message);

        message = new Message();
        message.setProfilePictureUrl("drawable://" + R.drawable.profile_picture);
        message.setTitle("Creative Channel");
        message.setUserName("esawy_rah");
        message.setDate("May 24");
        message.setMessage("Lorem ipsum dolor sit amet, adipisicing elit, sed do eiusmod tempor");
        messagesList.add(message);

        message = new Message();
        message.setProfilePictureUrl("drawable://" + R.drawable.profile_picture);
        message.setTitle("Creative Channel");
        message.setUserName("esawy_rah");
        message.setDate("May 24");
        message.setMessage("Lorem ipsum dolor sit amet, adipisicing elit, sed do eiusmod tempor");
        messagesList.add(message);

        message = new Message();
        message.setProfilePictureUrl("drawable://" + R.drawable.profile_picture);
        message.setTitle("Creative Channel");
        message.setUserName("esawy_rah");
        message.setDate("May 24");
        message.setMessage("Lorem ipsum dolor sit amet, adipisicing elit, sed do eiusmod tempor");
        messagesList.add(message);


        message = new Message();
        message.setProfilePictureUrl("drawable://" + R.drawable.profile_picture);
        message.setTitle("Creative Channel");
        message.setUserName("esawy_rah");
        message.setDate("May 24");
        message.setMessage("Lorem ipsum dolor sit amet, adipisicing elit, sed do eiusmod tempor");
        messagesList.add(message);


        message = new Message();
        message.setProfilePictureUrl("drawable://" + R.drawable.profile_picture);
        message.setTitle("Creative Channel");
        message.setUserName("esawy_rah");
        message.setDate("May 24");
        message.setMessage("Lorem ipsum dolor sit amet, adipisicing elit, sed do eiusmod tempor");
        messagesList.add(message);


        message = new Message();
        message.setProfilePictureUrl("drawable://" + R.drawable.profile_picture);
        message.setTitle("Creative Channel");
        message.setUserName("esawy_rah");
        message.setDate("May 24");
        message.setMessage("Lorem ipsum dolor sit amet, adipisicing elit, sed do eiusmod tempor");
        messagesList.add(message);


        message = new Message();
        message.setProfilePictureUrl("drawable://" + R.drawable.profile_picture);
        message.setTitle("Creative Channel");
        message.setUserName("esawy_rah");
        message.setDate("May 24");
        message.setMessage("Lorem ipsum dolor sit amet, adipisicing elit, sed do eiusmod tempor");
        messagesList.add(message);

        return messagesList;

    }

    @Override
    public void onSuccess(String operationTag, Object... objects) {

    }

    @Override
    public void onError(String operationTag, String error, Object... objects) {

    }
}
