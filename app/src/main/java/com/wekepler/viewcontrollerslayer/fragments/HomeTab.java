package com.wekepler.viewcontrollerslayer.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.costum.android.widget.LoadMoreListView;
import com.costum.android.widget.PullAndLoadListView;
import com.wekepler.R;
import com.wekepler.modelslayer.FeedsOfCurrentUserModel;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.MainActivity;
import com.wekepler.viewcontrollerslayer.adapters.PostItemHolder;
import com.wekepler.viewcontrollerslayer.adapters.PostsListAdapter;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.Post;
import com.wekepler.viewcontrollerslayer.entities.User;
import com.wekepler.viewcontrollerslayer.entities.parameters.VideosParameters;

import java.util.ArrayList;

/**
 * Created by Mohammad Sayed on 8/20/2015.
 */
public class HomeTab extends BaseListFragment {


    private ProgressBar mPbLoading;
    private ArrayList<Post> mPostsList;
    private PostsListAdapter mAdapter;
    private final int NUMBER_OF_VIDEOS_PER_CALL = 25;

    private enum LoadingType {
        REFRESH, FIRST, LOAD_MORE
    }

    private LoadingType mLoadingType;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeViews(view);
        getPostsList();
    }

    protected void initializeViews(View view) {
        ((MainActivity) getActivity()).setBaseListFragment(this);

        mPostsList = new ArrayList<>();
        mPbLoading = (ProgressBar) view.findViewById(R.id.pb_loading);
        mAdapter = new PostsListAdapter(getActivity(), mPostsList, this);
        getListView().setAdapter(mAdapter);
        ((PullAndLoadListView) getListView())
                .setOnLoadMoreListener(new PullAndLoadListView.OnLoadMoreListener() {
                    public void onLoadMore() {
                        // Do the work to load more items at the end of list here
                        mLoadingType = LoadingType.LOAD_MORE;
                        VideosParameters videosParameters =
                                new VideosParameters(NUMBER_OF_VIDEOS_PER_CALL, mPostsList.get(mPostsList.size() - 1).getId());
                        new FeedsOfCurrentUserModel(getActivity(), videosParameters, HomeTab.this);
                    }
                });

        ((PullAndLoadListView) getListView())
                .setOnRefreshListener(new PullAndLoadListView.OnRefreshListener() {
                    public void onRefresh() {
                        // Do work to refresh the list here.
                        mLoadingType = LoadingType.REFRESH;
                        VideosParameters videosParameters =
                                new VideosParameters(NUMBER_OF_VIDEOS_PER_CALL, 0);
                        new FeedsOfCurrentUserModel(getActivity(), videosParameters, HomeTab.this);
                    }
                });
    }

    private void getPostsList() {
        //Actual Code:---------------------
        mPbLoading.setVisibility(View.VISIBLE);
        mLoadingType = LoadingType.FIRST;
        VideosParameters videosParameters =
                new VideosParameters(NUMBER_OF_VIDEOS_PER_CALL, 0);
        new FeedsOfCurrentUserModel(getActivity(), videosParameters, this);
    }

    @Override
    public void onSuccess(String operationTag, Object... objects) {

        if (operationTag.equals(Constants.FEEDS_OF_CURRENT_USER_TAG)) {
            ArrayList<Post> posts = (ArrayList<Post>) objects[0];
            if (mPostsList.isEmpty() || mLoadingType == LoadingType.FIRST)
                mPostsList = posts;
            else {
                if (mLoadingType == LoadingType.REFRESH) {
                    //posts.addAll(mPostsList);
                    ArrayList<Post> temp = new ArrayList<>();
                    for (Post post : posts) {
                        if (post.getId() != mPostsList.get(0).getId())
                            temp.add(post);
                        else
                            break;
                    }
                    temp.addAll(mPostsList);
                    mPostsList = temp;
                    ((PullAndLoadListView) getListView()).onRefreshComplete();
                } else if (mLoadingType == LoadingType.LOAD_MORE) {
                    mPostsList.addAll(posts);
                    ((PullAndLoadListView) getListView()).onLoadMoreComplete();
                }
            }
            mAdapter.setItems(mPostsList);
            mPbLoading.setVisibility(View.INVISIBLE);
            if (mPostsList.isEmpty())
                Toast.makeText(getActivity(), "no videos available", Toast.LENGTH_LONG).show();
        } else if (operationTag.equals(Constants.LIKE_VIDEO_TAG)) {
            Toast.makeText(getActivity(), "video is liked", Toast.LENGTH_LONG).show();
        } else if (operationTag.equals(Constants.FOLLOW_USER_TAG)) {
            Post post = (Post) objects[0];
            String status = post.getUser().isFollowed() ? "Followed" : "Un-Followed";
            Toast.makeText(getActivity(), post.getUser().getUserName() + " is " + status, Toast.LENGTH_LONG).show();
        } else if (operationTag.equals(Constants.FAVORITE_VIDEO_TAG)) {
            Toast.makeText(getActivity(), "Video added to favorites", Toast.LENGTH_LONG).show();
        } else if (operationTag.equals(Constants.REPORT_VIDEO_TAG)) {
            Toast.makeText(getActivity(), "Video is reported", Toast.LENGTH_LONG).show();
        } else if (operationTag.equals(Constants.BLOCK_VIDEO_TAG)) {
            Toast.makeText(getActivity(), "Video is blocked", Toast.LENGTH_LONG).show();
            mPostsList.clear();
            getPostsList();
        } else if (operationTag.equals(Constants.DELETE_VIDEO_TAG)) {
            Toast.makeText(getActivity(), "Video is deleted", Toast.LENGTH_LONG).show();
            mPostsList.clear();
            getPostsList();
        }
    }

    @Override
    public void onError(String operationTag, String error, Object... objects) {
        mPbLoading.setVisibility(View.INVISIBLE);
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
        if (operationTag.equals(Constants.LIKE_VIDEO_TAG)) {
            Post post = (Post) objects[0];
            PostItemHolder postItemHolder = (PostItemHolder) objects[1];
            if (post.isLiked()) {
                post.setIsLiked(false);
                postItemHolder.ibLike.setImageResource(R.drawable.like_normal);
                post.setLikesNumber(post.getLikesNumber() - 1);
                postItemHolder.tvLikesNumber.setText("" + post.getLikesNumber());
            } else {
                post.setIsLiked(true);
                postItemHolder.ibLike.setImageResource(R.drawable.like_hover);
                post.setLikesNumber(post.getLikesNumber() + 1);
                postItemHolder.tvLikesNumber.setText("" + post.getLikesNumber());
            }
        } else if (operationTag.equals(Constants.REPOST_VIDEO_TAG)) {
            Post post = (Post) objects[0];
            PostItemHolder postItemHolder = (PostItemHolder) objects[1];
            post.setIsReposted(false);
            postItemHolder.ibRepost.setImageResource(R.drawable.repost_normal);
            post.setRepostsNumber(post.getRepostsNumber() - 1);
            postItemHolder.tvRepostsNumber.setText("" + post.getRepostsNumber());
        } else if (operationTag.equals(Constants.FOLLOW_USER_TAG)) {
            Post post = (Post) objects[0];
            User user = post.getUser();
            PostItemHolder postItemHolder = (PostItemHolder) objects[1];
            if (user.isFollowed()) {
                user.setFollowed(false);
                postItemHolder.ibFollow.setImageResource(R.drawable.follow_icon_normal);
            } else {
                user.setFollowed(true);
                postItemHolder.ibFollow.setImageResource(R.drawable.follow_icon_hover);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.VIEW_VIDEO_DETAILS_REQUEST_CODE)
            getPostsList();
    }
}
