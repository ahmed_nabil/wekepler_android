package com.wekepler.viewcontrollerslayer.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.costum.android.widget.PullAndLoadListView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wekepler.R;
import com.wekepler.modelslayer.FeedsOfCurrentUserModel;
import com.wekepler.modelslayer.GetCurrentUserProfileModel;
import com.wekepler.modelslayer.VideosOfCurrentUserModel;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.MainActivity;
import com.wekepler.viewcontrollerslayer.SettingsActivity;
import com.wekepler.viewcontrollerslayer.adapters.PostItemHolder;
import com.wekepler.viewcontrollerslayer.adapters.PostsListAdapter;
import com.wekepler.viewcontrollerslayer.adapters.ProfileListAdapter;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.Post;
import com.wekepler.viewcontrollerslayer.entities.User;
import com.wekepler.viewcontrollerslayer.entities.parameters.VideosParameters;

import java.util.ArrayList;

/**
 * Created by Mohammad Sayed on 8/20/2015.
 */
public class ProfileTab extends BaseListFragment implements OnModelResponseListener, View.OnClickListener {

    private final int NUMBER_OF_VIDEOS_PER_CALL = 25;
    private MainActivity mMainActivity;
    private User mUser;
    private ArrayList<Post> mVideosList;
    private ArrayList<Post> favoriteList;
    private ProfileListAdapter adapter;

    private ProgressBar mPbLoading;
    private ListView mListView;
    private ImageView mIvProfilePicture;
    private TextView mTvScreenName;
    private TextView mTvUserName;
    private ImageButton mIbSettings;
    private TextView mTvStatus;
    private TextView mTvLocation;
    private TextView mTvFollowersNumber;
    private TextView mTvVideosNumber;
    private TextView mTvFollowingNumber;
    private RadioGroup mRadioGroup;
    private RadioButton mRbVideos;
    private RadioButton mRbFavorite;

    private enum LoadingType {
        REFRESH, FIRST, LOAD_MORE
    }

    private LoadingType mLoadingType;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mMainActivity = (MainActivity) getActivity();
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeViews(view);
    }

    protected void initializeViews(View view) {
        mPbLoading = (ProgressBar) view.findViewById(R.id.pb_loading);
        mListView = getListView();
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View header = inflater.inflate(R.layout.profile_list_header, mListView, false);
        mListView.addHeaderView(header);
        /*LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) header.getLayoutParams();
        params.setMargins(-10, 0, -10, -10);*/
        mIvProfilePicture = (ImageView) header.findViewById(R.id.iv_profile_picture);
        mTvScreenName = (TextView) header.findViewById(R.id.tv_screenName);
        mTvUserName = (TextView) header.findViewById(R.id.tv_user_name);
        mIbSettings = (ImageButton) header.findViewById(R.id.ib_settings);
        mTvStatus = (TextView) header.findViewById(R.id.tv_status);
        mTvLocation = (TextView) header.findViewById(R.id.tv_location);
        mTvFollowersNumber = (TextView) header.findViewById(R.id.tv_followers_number);
        mTvVideosNumber = (TextView) header.findViewById(R.id.tv_videos_number);
        mTvFollowingNumber = (TextView) header.findViewById(R.id.tv_following_number);

        mRadioGroup = (RadioGroup) header.findViewById(R.id.rg_tabs_header);
        mRbVideos = (RadioButton) header.findViewById(R.id.rb_video);
        mRbFavorite = (RadioButton) header.findViewById(R.id.rb_favorite);
        mRbVideos.setTextColor(ContextCompat.getColor(mMainActivity, R.color.orange));
        mIbSettings.setOnClickListener(this);

        mLoadingType = LoadingType.FIRST;
        getProfile();
        getVideosList();

        adapter = new ProfileListAdapter(getActivity(), mVideosList, this);
        mListView.setAdapter(adapter);
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_video) {
                    mRbVideos.setTextColor(ContextCompat.getColor(mMainActivity, R.color.orange));
                    mRbFavorite.setTextColor(ContextCompat.getColor(mMainActivity, R.color.dark_gray));
                    adapter.setItems(getVideosList());
                    adapter.notifyDataSetChanged();
                } else if (checkedId == R.id.rb_favorite) {
                    mRbVideos.setTextColor(ContextCompat.getColor(mMainActivity, R.color.dark_gray));
                    mRbFavorite.setTextColor(ContextCompat.getColor(mMainActivity, R.color.orange));
                    adapter.setItems(getFavoriteList());
                    adapter.notifyDataSetChanged();
                }
            }
        });

        ((PullAndLoadListView) mListView)
                .setOnLoadMoreListener(new PullAndLoadListView.OnLoadMoreListener() {
                    public void onLoadMore() {
                        // Do the work to load more items at the end of list here
                        if (mRbVideos.isChecked()) {
                            mLoadingType = LoadingType.LOAD_MORE;
                            VideosParameters videosParameters =
                                    new VideosParameters(NUMBER_OF_VIDEOS_PER_CALL, mVideosList.get(mVideosList.size() - 1).getId());
                            new VideosOfCurrentUserModel(getActivity(), videosParameters, ProfileTab.this);
                        } else {

                        }
                    }
                });

        ((PullAndLoadListView) mListView)
                .setOnRefreshListener(new PullAndLoadListView.OnRefreshListener() {
                    public void onRefresh() {
                        mLoadingType = LoadingType.REFRESH;
                        getProfile();
                        if (mRbVideos.isChecked()) {
                            mVideosList.clear();
                            getVideosList();
                        } else {

                        }
                    }
                });
    }

    private void getProfile() {
        new GetCurrentUserProfileModel(mMainActivity, this);
    }


    private ArrayList<Post> getVideosList() {
        //Actual Code:---------------------
        if (mVideosList == null)
            mVideosList = new ArrayList<>();
        mPbLoading.setVisibility(View.VISIBLE);
        VideosParameters videosParameters =
                new VideosParameters(NUMBER_OF_VIDEOS_PER_CALL, 0);
        new VideosOfCurrentUserModel(getActivity(), videosParameters, this);
        return mVideosList;
    }

    private ArrayList<Post> getFavoriteList() {
        if (favoriteList == null) {
            favoriteList = new ArrayList<>();
            User user = new User();
            user.setProfilePictureUrl("drawable://" + R.drawable.default_profile_picture);
            user.setScreenName("Default Channel");
            user.setUserName("default_name");
            Post post = post = new Post();
            post.setUser(user);
            post.setPostSince("1 d");
            post.setVideoCaption("Lorem ipsum dolor sit amet, adipisicing elit, sed do eiusmod tempor ...");
            post.setViewersNumber(587587);
            post.setIsLiked(false);
            post.setLikesNumber(1000);
            post.setIsCommented(true);
            post.setCommentsNumber(999);
            post.setIsReposted(true);
            post.setRepostsNumber(14789);


            //post.setIsShared(false);
            //post.setSharesNumber(500);
            favoriteList.add(post);

            user = new User();
            user.setProfilePictureUrl("drawable://" + R.drawable.profile_picture);
            user.setScreenName("Creative Channel");
            user.setUserName("esawy_rah");
            post = new Post();
            post.setUser(user);
            post.setPostSince("17 h");
            post.setVideoCaption("Lorem ipsum dolor sit amet, adipisicing elit, sed do eiusmod tempor ...");
            post.setViewersNumber(17356);
            post.setIsLiked(true);
            post.setLikesNumber(45);
            post.setIsCommented(false);
            post.setCommentsNumber(45);
            post.setIsReposted(false);
            post.setRepostsNumber(80);
            //post.setIsShared(true);
            //post.setSharesNumber(121);
            favoriteList.add(post);

            user = new User();
            user.setProfilePictureUrl("drawable://" + R.drawable.default_profile_picture);
            user.setScreenName("Default Channel");
            user.setUserName("default_name");
            post = new Post();
            post.setUser(user);
            post.setPostSince("1 d");
            post.setVideoCaption("Lorem ipsum dolor sit amet, adipisicing elit, sed do eiusmod tempor ...");
            post.setViewersNumber(587587);
            post.setIsLiked(false);
            post.setLikesNumber(1000);
            post.setIsCommented(true);
            post.setCommentsNumber(999);
            post.setIsReposted(true);
            post.setRepostsNumber(14789);
            //post.setIsShared(false);
            //post.setSharesNumber(500);
            favoriteList.add(post);
        }
        return favoriteList;
    }

    /*private void setListViewHeightBasedOnChildren(final ListView listView) {
        listView.post(new Runnable() {
            @Override
            public void run() {
                ListAdapter listAdapter = listView.getAdapter();
                if (listAdapter == null)
                    return;

                int totalHeight = 0;
                for (int i = 0; i < listAdapter.getCount(); i++) {
                    View listItem = listAdapter.getView(i, null, listView);
                    listItem.measure(0, 0);
                    totalHeight += listItem.getMeasuredHeight();
                }

                ViewGroup.LayoutParams params = listView.getLayoutParams();
                params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount()))// - 1))
                        + listView.getListPaddingTop();
                listView.setLayoutParams(params);
            }
        });
    }*/

    @Override
    public void onSuccess(String operationTag, Object... objects) {

        if (operationTag.equals(Constants.GET_CURRENT_USER_PROFILE_TAG)) {
            Toast.makeText(getActivity(), "profile of user retrieved successfully", Toast.LENGTH_LONG).show();
            mUser = (User) objects[0];
            ImageLoader.getInstance().displayImage(mUser.getProfilePictureUrl(), mIvProfilePicture);
            mTvScreenName.setText(mUser.getScreenName());
            mTvUserName.setText(mUser.getUserName());
            mTvStatus.setText(mUser.getDescription());
            mTvLocation.setText(mUser.getLocation());
            mTvFollowersNumber.setText("" + mUser.getFollowersNumber());
            mTvVideosNumber.setText("" + mUser.getVideosNumber());
            mTvFollowingNumber.setText("" + mUser.getFollowingNumber());

        } else if (operationTag.equals(Constants.VIDEOS_OF_CURRENT_USER_TAG)) {
            ArrayList<Post> posts = (ArrayList<Post>) objects[0];
            if (mVideosList == null || mVideosList.isEmpty())
                mVideosList = posts;
            else {
                mVideosList.addAll(posts);
            }
            adapter.setItems(mVideosList);
            //setListViewHeightBasedOnChildren(mListView);
            mPbLoading.setVisibility(View.INVISIBLE);

            if (mLoadingType == LoadingType.REFRESH) {
                ((PullAndLoadListView) getListView()).onRefreshComplete();
            } else if (mLoadingType == LoadingType.LOAD_MORE) {
                ((PullAndLoadListView) getListView()).onLoadMoreComplete();
            }


        } else if (operationTag.equals(Constants.LIKE_VIDEO_TAG)) {
            Toast.makeText(getActivity(), "video is liked", Toast.LENGTH_LONG).show();
        } else if (operationTag.equals(Constants.REPOST_VIDEO_TAG)) {
            Toast.makeText(getActivity(), "video is reposted", Toast.LENGTH_LONG).show();
        } else if (operationTag.equals(Constants.FOLLOW_USER_TAG)) {
            Post post = (Post) objects[0];
            String status = post.getUser().isFollowed() ? "Followed" : "Un-Followed";
            Toast.makeText(getActivity(), post.getUser().getUserName() + " is " + status, Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onError(String operationTag, String error, Object... objects) {
        mPbLoading.setVisibility(View.INVISIBLE);
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
        if (operationTag.equals(Constants.LIKE_VIDEO_TAG)) {
            Post post = (Post) objects[0];
            PostItemHolder postItemHolder = (PostItemHolder) objects[1];
            if (post.isLiked()) {
                post.setIsLiked(false);
                postItemHolder.ibLike.setImageResource(R.drawable.like_normal);
                post.setLikesNumber(post.getLikesNumber() - 1);
                postItemHolder.tvLikesNumber.setText("" + post.getLikesNumber());
            } else {
                post.setIsLiked(true);
                postItemHolder.ibLike.setImageResource(R.drawable.like_hover);
                post.setLikesNumber(post.getLikesNumber() + 1);
                postItemHolder.tvLikesNumber.setText("" + post.getLikesNumber());
            }
        } else if (operationTag.equals(Constants.REPOST_VIDEO_TAG)) {
            Post post = (Post) objects[0];
            PostItemHolder postItemHolder = (PostItemHolder) objects[1];
            post.setIsReposted(false);
            postItemHolder.ibRepost.setImageResource(R.drawable.repost_normal);
            post.setRepostsNumber(post.getRepostsNumber() - 1);
            postItemHolder.tvRepostsNumber.setText("" + post.getRepostsNumber());
        } else if (operationTag.equals(Constants.FOLLOW_USER_TAG)) {
            Post post = (Post) objects[0];
            User user = post.getUser();
            PostItemHolder postItemHolder = (PostItemHolder) objects[1];
            if (user.isFollowed()) {
                user.setFollowed(false);
                postItemHolder.ibFollow.setImageResource(R.drawable.follow_icon_normal);
            } else {
                user.setFollowed(true);
                postItemHolder.ibFollow.setImageResource(R.drawable.follow_icon_hover);
            }
        } else if (operationTag.equals(Constants.GET_CURRENT_USER_PROFILE_TAG)) {
            Toast.makeText(getActivity(), "error in retrieving user profile", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == mIbSettings) {
            Intent intent = new Intent(mMainActivity, SettingsActivity.class);
            intent.putExtra(Constants.USER_TAG, mUser);
            mMainActivity.startActivityForResult(intent, Constants.EDIT_SETTINGS_REQUEST_CODE);
        }
    }
}
