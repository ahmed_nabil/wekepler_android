package com.wekepler.viewcontrollerslayer;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.wekepler.R;
import com.wekepler.modelslayer.FollowUserModel;
import com.wekepler.modelslayer.FriendVideosModel;
import com.wekepler.modelslayer.GetUserProfileModel;
import com.wekepler.viewcontrollerslayer.adapters.PostItemHolder;
import com.wekepler.viewcontrollerslayer.adapters.PostsListAdapter;
import com.wekepler.viewcontrollerslayer.entities.Friend;
import com.wekepler.viewcontrollerslayer.entities.Post;
import com.wekepler.viewcontrollerslayer.entities.User;
import com.wekepler.viewcontrollerslayer.entities.parameters.VideosParameters;

import java.util.ArrayList;

public class FriendProfileActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack;
    private ImageView mIvSettings;
    private TextView mTvActionBarTitle;

    private ProgressBar mPbLoading;

    private Friend mFriend;
    private ArrayList<Post> mPostsList;
    private PostsListAdapter mAdapter;

    private ListView mListView;
    private ImageView mIvProfilePicture;
    private TextView mTvScreenName;
    private TextView mTvUserName;
    private TextView mTvStatus;
    private TextView mTvLocation;
    private ImageButton mIbMessage;
    private ImageButton mIbFollow;
    private TextView mTvFollowersNumber;
    private TextView mTvVideosNumber;
    private TextView mTvFollowingNumber;

    private final int NUMBER_OF_VIDEOS_PER_CALL = 25;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_profile);
        initializeViews();
    }

    @Override
    public void initializeViews() {

        mIvBack = (ImageView) findViewById(R.id.iv_back_action_bar);
        mIvSettings = (ImageView) findViewById(R.id.iv_settings_action_bar);
        mTvActionBarTitle = (TextView) findViewById(R.id.tv_center_title);

        mPbLoading = (ProgressBar) findViewById(R.id.pb_loading);

        mIvProfilePicture = (ImageView) findViewById(R.id.iv_profile_picture);
        mTvScreenName = (TextView) findViewById(R.id.tv_screenName);
        mTvUserName = (TextView) findViewById(R.id.tv_user_name);
        mTvStatus = (TextView) findViewById(R.id.tv_status);
        mTvLocation = (TextView) findViewById(R.id.tv_location);
        mIbFollow = (ImageButton) findViewById(R.id.ib_follow);
        mIbMessage = (ImageButton) findViewById(R.id.ib_message);
        mTvFollowersNumber = (TextView) findViewById(R.id.tv_followers_number);
        mTvVideosNumber = (TextView) findViewById(R.id.tv_videos_number);
        mTvFollowingNumber = (TextView) findViewById(R.id.tv_following_number);
        getProfile();

        mIvBack.setOnClickListener(this);
        mIbFollow.setOnClickListener(this);
        mIbMessage.setOnClickListener(this);

        mListView = (ListView) findViewById(R.id.lv_profile_list);


        getPostsList();
        mAdapter = new PostsListAdapter(this, mPostsList, this);
        mListView.setAdapter(mAdapter);
        setListViewHeightBasedOnChildren(mListView);
        //getListView().addHeaderView(mHeaderView);
    }


    private void getProfile() {
        mFriend = (Friend) getIntent().getSerializableExtra(Constants.FRIEND_TAG);
        new GetUserProfileModel(this, mFriend.getUserId(), this);
    }

    private ArrayList<Post> getPostsList() {
        //Actual Code:---------------------
        if (mPostsList == null)
            mPostsList = new ArrayList<>();

        if (mPostsList != null && mPostsList.isEmpty()) {
            mPbLoading.setVisibility(View.VISIBLE);
            VideosParameters videosParameters = new VideosParameters(NUMBER_OF_VIDEOS_PER_CALL, 0);
            new FriendVideosModel(this, mFriend.getUserId(), videosParameters, this);
        }
        return mPostsList;

    }

    private void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount()))// - 1))
                + listView.getListPaddingTop();
        listView.setLayoutParams(params);
    }

    @Override
    public void onClick(View v) {
        if (v == mIvBack) {
            onBackPressed();
        } else if (v == mIbFollow) {

            new FollowUserModel(this, mFriend, null, null, this);
            if (mFriend.isFollowed()) {
                mFriend.setFollowed(false);
                mIbFollow.setImageResource(R.drawable.friend_profile_follow_normal);
            } else {
                mFriend.setFollowed(true);
                mIbFollow.setImageResource(R.drawable.friend_profile_follow_hover);
            }


            if (mFriend.isFollowed()) {
                mIbFollow.setImageResource(R.drawable.friend_profile_follow_normal);
                mFriend.setIsFollowed(false);

            } else {
                mIbFollow.setImageResource(R.drawable.friend_profile_follow_hover);
                mFriend.setIsFollowed(true);
            }
        } else if (v == mIbMessage) {
            if (mFriend != null) {
                ArrayList<Integer> receiverIds = new ArrayList<>();
                ArrayList<String> receiverScreenNames = new ArrayList<>();
                receiverIds.add(mFriend.getUserId());
                receiverScreenNames.add(mFriend.getScreenName());
                Bundle bundle = new Bundle();
                bundle.putIntegerArrayList(Constants.RECEIVER_ID_TAG, receiverIds);
                bundle.putStringArrayList(Constants.RECEIVER_SCREEN_NAME_TAG, receiverScreenNames);
                goToActivity(MessageDetailsActivity.class, false, bundle);
            }
        }
    }

    @Override
    public void onSuccess(String operationTag, Object... objects) {

        if (operationTag.equals(Constants.GET_USER_PROFILE_TAG)) {
            mFriend = (Friend) objects[0];
            mTvActionBarTitle.setText(mFriend.getScreenName());
            ImageLoader.getInstance().displayImage(mFriend.getProfilePictureUrl(), mIvProfilePicture);
            mTvScreenName.setText(mFriend.getScreenName());
            mTvUserName.setText(mFriend.getUserName());
            mTvStatus.setText(mFriend.getDescription());
            mTvLocation.setText(mFriend.getLocation());
            mTvFollowersNumber.setText("" + mFriend.getFollowersNumber());
            mTvVideosNumber.setText("" + mFriend.getVideosNumber());
            mTvFollowingNumber.setText("" + mFriend.getFollowingNumber());
            if (mFriend.isFollowed()) {
                mIbFollow.setImageResource(R.drawable.friend_profile_follow_hover);
            } else {
                mIbFollow.setImageResource(R.drawable.friend_profile_follow_normal);
            }

        } else if (operationTag.equals(Constants.VIDEOS_OF_FRIEND_TAG)) {
            ArrayList<Post> posts = (ArrayList<Post>) objects[0];
            if (mPostsList.isEmpty())
                mPostsList = posts;
            else {
                mPostsList.addAll(posts);
            }
            mAdapter.setItems(mPostsList);
            if (mPostsList.isEmpty())
                toastLong("This user has no video");
            setListViewHeightBasedOnChildren(mListView);
            mPbLoading.setVisibility(View.INVISIBLE);
        } else if (operationTag.equals(Constants.LIKE_VIDEO_TAG)) {
            Toast.makeText(this, "video is liked", Toast.LENGTH_LONG).show();
        } else if (operationTag.equals(Constants.REPOST_VIDEO_TAG)) {
            Toast.makeText(this, "video is reposted", Toast.LENGTH_LONG).show();
        } else if (operationTag.equals(Constants.FOLLOW_USER_TAG)) {
            if (objects != null) {
                Post post = (Post) objects[0];
                String status = post.getUser().isFollowed() ? "Followed" : "Un-Followed";
                Toast.makeText(this, post.getUser().getUserName() + " is " + status, Toast.LENGTH_LONG).show();
            } else {
                String status = mFriend.isFollowed() ? "Followed" : "Un-Followed";
                Toast.makeText(this, mFriend.getUserName() + " is " + status, Toast.LENGTH_LONG).show();
            }
        } else if (operationTag.equals(Constants.GET_USER_PROFILE_TAG)) {
            toastLong("profile of user retrieved successfully");
        }
    }

    @Override
    public void onError(String operationTag, String error, Object... objects) {
        mPbLoading.setVisibility(View.INVISIBLE);
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        if (operationTag.equals(Constants.LIKE_VIDEO_TAG)) {
            Post post = (Post) objects[0];
            PostItemHolder postItemHolder = (PostItemHolder) objects[1];
            if (post.isLiked()) {
                post.setIsLiked(false);
                postItemHolder.ibLike.setImageResource(R.drawable.like_normal);
                postItemHolder.tvLikesNumber.setText((post.getLikesNumber() - 1) + "");
            } else {
                post.setIsLiked(true);
                postItemHolder.ibLike.setImageResource(R.drawable.like_hover);
                postItemHolder.tvLikesNumber.setText((post.getLikesNumber() + 1) + "");
            }
        } else if (operationTag.equals(Constants.REPOST_VIDEO_TAG)) {
            Post post = (Post) objects[0];
            PostItemHolder postItemHolder = (PostItemHolder) objects[1];
            post.setIsReposted(false);
            postItemHolder.ibRepost.setImageResource(R.drawable.repost_normal);
            postItemHolder.tvRepostsNumber.setText((post.getRepostsNumber() - 1) + "");
        } else if (operationTag.equals(Constants.FOLLOW_USER_TAG)) {
            if (objects != null) {
                Post post = (Post) objects[0];
                User user = post.getUser();
                PostItemHolder postItemHolder = (PostItemHolder) objects[1];
                if (user.isFollowed()) {
                    user.setFollowed(false);
                    postItemHolder.ibFollow.setImageResource(R.drawable.follow_icon_normal);
                } else {
                    user.setFollowed(true);
                    postItemHolder.ibFollow.setImageResource(R.drawable.follow_icon_hover);
                }
            } else {
                if (mFriend.isFollowed()) {
                    mFriend.setFollowed(false);
                    mIbFollow.setImageResource(R.drawable.friend_profile_follow_normal);
                } else {
                    mFriend.setFollowed(true);
                    mIbFollow.setImageResource(R.drawable.friend_profile_follow_hover);
                }
            }
        } else if (operationTag.equals(Constants.GET_USER_PROFILE_TAG)) {
            toastLong("error in retrieving user profile");
        }
    }

}
