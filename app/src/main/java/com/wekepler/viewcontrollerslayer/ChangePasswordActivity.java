package com.wekepler.viewcontrollerslayer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.wekepler.R;
import com.wekepler.modelslayer.ChangePasswordModel;
import com.wekepler.viewcontrollerslayer.entities.requests.ChangePasswordRequest;

public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack;
    private Button mBtnSave;
    private EditText mEtOldPassword;
    private EditText mEtNewPassword;
    private EditText mEtConfirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        initializeViews();
    }

    @Override
    public void initializeViews() {
        mIvBack = (ImageView) findViewById(R.id.iv_left_icon_action_bar);
        mBtnSave = (Button) findViewById(R.id.btn_right_icon_action_bar);
        mEtOldPassword = (EditText) findViewById(R.id.et_old_password);
        mEtNewPassword = (EditText) findViewById(R.id.et_new_password);
        mEtConfirmPassword = (EditText) findViewById(R.id.et_confirm_password);

        mIvBack.setOnClickListener(this);
        mBtnSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mIvBack)
            onBackPressed();
        else if (v == mBtnSave) {
            String oldPassword = getTextFromEditText(mEtOldPassword, false);
            String newPassword = getTextFromEditText(mEtNewPassword, false);
            String confirmPassword = getTextFromEditText(mEtConfirmPassword, false);

            if (oldPassword.equals(""))
                toastLong(R.string.empty_old_password);
            else if (newPassword.equals(""))
                toastLong(R.string.empty_new_password);
            else if (confirmPassword.equals(""))
                toastLong(R.string.empty_confirm_password);
            else if (!newPassword.equals(confirmPassword))
                toastLong(R.string.confirm_password_not_match);
            else {
                ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();
                changePasswordRequest.setOldPassword(oldPassword);
                changePasswordRequest.setNewPassword(newPassword);
                changePasswordRequest.setConfirmPassword(confirmPassword);
                new ChangePasswordModel(this, changePasswordRequest, this);
            }
        }
    }

    @Override
    public void onSuccess(String operationTag, Object... objects) {
        if (operationTag.equals(Constants.CHANGE_PASSWORD_TAG)) {
            onBackPressed();
        }
    }

    @Override
    public void onError(String operationTag, String error, Object... objects) {
        toastLong(error);
    }

}
