package com.wekepler.viewcontrollerslayer.entities.Responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mohammad Sayed on 9/21/2015.
 */
public class UserInfoResponse {

    @SerializedName("UserName")
    private String userName;

    @SerializedName("HasRegistered")
    private boolean hasRegistered;

    @SerializedName("LoginProvider")
    private String loginProvider;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isHasRegistered() {
        return hasRegistered;
    }

    public void setHasRegistered(boolean hasRegistered) {
        this.hasRegistered = hasRegistered;
    }

    public String getLoginProvider() {
        return loginProvider;
    }

    public void setLoginProvider(String loginProvider) {
        this.loginProvider = loginProvider;
    }
}
