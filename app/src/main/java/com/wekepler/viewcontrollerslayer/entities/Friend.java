package com.wekepler.viewcontrollerslayer.entities;

import java.io.Serializable;

/**
 * Created by Mohammad Sayed on 8/23/2015.
 */
public class Friend extends User implements Serializable {

    public Friend() {
    }

    public Friend(User user) {
        super(user);
    }

    private boolean isFollowed;

    public boolean isFollowed() {
        return isFollowed;
    }

    public void setIsFollowed(boolean isFollowed) {
        this.isFollowed = isFollowed;
    }
}
