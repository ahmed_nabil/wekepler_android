package com.wekepler.viewcontrollerslayer.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mohammad Sayed on 8/22/2015.
 */
public class Message {

    @SerializedName("MessageID")
    private int id;

    @SerializedName("SenderID")
    private int senderId;

    @SerializedName("ReceiverID")
    private int receiverId;

    @SerializedName("MessageText")
    private String message;

    @SerializedName("SentDate")
    private String sentDate;

    @SerializedName("ViewDate")
    private String viewDate;


    @SerializedName("IsViewed")
    private boolean isViewed;

    @SerializedName("Sender")
    private User sender;

    @SerializedName("Receiver")
    private User receiver;


    private String profilePictureUrl;
    private String title;
    private String userName;
    private String date;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    public int getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(int receiverId) {
        this.receiverId = receiverId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSentDate() {
        return sentDate;
    }

    public void setSentDate(String sentDate) {
        this.sentDate = sentDate;
    }

    public String getViewDate() {
        return viewDate;
    }

    public void setViewDate(String viewDate) {
        this.viewDate = viewDate;
    }

    public boolean isViewed() {
        return isViewed;
    }

    public void setIsViewed(boolean isViewed) {
        this.isViewed = isViewed;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public String getProfilePictureUrl() {
        return profilePictureUrl;
    }

    public void setProfilePictureUrl(String profilePictureUrl) {
        this.profilePictureUrl = profilePictureUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
