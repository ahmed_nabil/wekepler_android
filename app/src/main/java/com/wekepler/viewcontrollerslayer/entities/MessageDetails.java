package com.wekepler.viewcontrollerslayer.entities;

import java.util.ArrayList;

/**
 * Created by Mohammad Sayed on 8/22/2015.
 */
public class MessageDetails {

    private String id;
    private ArrayList<Message> messagesList;
    private Friend friend;
    private User user;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Message> getMessagesList() {
        return messagesList;
    }

    public void setMessagesList(ArrayList<Message> messagesList) {
        this.messagesList = messagesList;
    }

    public Friend getFriend() {
        return friend;
    }

    public void setFriend(Friend friend) {
        this.friend = friend;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
