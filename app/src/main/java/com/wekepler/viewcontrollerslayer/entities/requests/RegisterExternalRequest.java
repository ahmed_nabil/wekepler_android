package com.wekepler.viewcontrollerslayer.entities.requests;

import com.google.gson.annotations.SerializedName;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.User;

/**
 * Created by Mohammad Sayed on 9/5/2015.
 */
public class RegisterExternalRequest {

    @SerializedName("username")
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
