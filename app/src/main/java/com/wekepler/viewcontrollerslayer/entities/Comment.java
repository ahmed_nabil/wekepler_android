package com.wekepler.viewcontrollerslayer.entities;

import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Mohammad Sayed on 8/24/2015.
 */
public class Comment implements Serializable {

    @SerializedName("CommentID")
    private int id;

    @SerializedName("VideoID")
    private int postId;

    @SerializedName("CommentText")
    private String body;

    @SerializedName("ParentCommentID")
    private int parentCommentId;

    @SerializedName("CommentDate")
    private String postSince;

    @SerializedName("LastModifiedDate")
    private String lastModifiedDate;

    @SerializedName("ChildComments")
    private ArrayList<Comment> childComments;


    @SerializedName("LikesCount")
    private int likesNumber;

    @SerializedName("User")
    private User user;

    @SerializedName("Liked")
    private boolean isLiked;

    private boolean canLike;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getParentCommentId() {
        return parentCommentId;
    }

    public void setParentCommentId(int parentCommentId) {
        this.parentCommentId = parentCommentId;
    }

    public String getPostSince() {
        return postSince;
    }

    public void setPostSince(String postSince) {
        this.postSince = postSince;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public ArrayList<Comment> getChildComments() {
        return childComments;
    }

    public void setChildComments(ArrayList<Comment> childComments) {
        this.childComments = childComments;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setIsLiked(boolean isLiked) {
        this.isLiked = isLiked;
    }

    public boolean isCanLike() {
        return canLike;
    }

    public void setCanLike(boolean canLike) {
        this.canLike = canLike;
    }

    public int getLikesNumber() {
        return likesNumber;
    }

    public void setLikesNumber(int likesNumber) {
        this.likesNumber = likesNumber;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
