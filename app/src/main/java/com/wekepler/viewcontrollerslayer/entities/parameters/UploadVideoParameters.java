package com.wekepler.viewcontrollerslayer.entities.parameters;

/**
 * Created by Mohammad Sayed on 9/12/2015.
 */
public class UploadVideoParameters extends BaseParameters {

    public static final String TITLE = "Title";
    public static final String COMMENT_TEXT = "CommentText";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    private String title;
    private String commentText;
    private double latitude;
    private double longitude;

    public UploadVideoParameters(String title, String commentText, double latitude, double longitude) {
        parametersMap.put(TITLE, title);
        parametersMap.put(COMMENT_TEXT, commentText);
        parametersMap.put(LATITUDE, String.valueOf(latitude));
        parametersMap.put(LONGITUDE, String.valueOf(longitude));
        this.title = title;
        this.commentText = commentText;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

}
