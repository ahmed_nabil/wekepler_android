package com.wekepler.viewcontrollerslayer.entities.Responses;

import com.google.gson.annotations.SerializedName;
import com.wekepler.viewcontrollerslayer.entities.Post;

import java.util.ArrayList;

/**
 * Created by Mohammad Sayed on 9/12/2015.
 */
public class PostsArray {

    @SerializedName("")
    ArrayList<Post> posts;

    public void setPosts(ArrayList<Post> posts) {
        this.posts = posts;
    }

    public ArrayList<Post> getPosts() {
        return posts;
    }
}
