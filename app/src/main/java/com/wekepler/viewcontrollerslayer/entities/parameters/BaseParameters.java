package com.wekepler.viewcontrollerslayer.entities.parameters;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammad Sayed on 9/12/2015.
 */
public class BaseParameters {

    protected Map parametersMap;

    public BaseParameters() {
        parametersMap = new HashMap<>();
    }

    public void setParametersMap(Map parametersMap) {
        this.parametersMap = parametersMap;
    }

    public Map getParametersMap() {
        return parametersMap;
    }
}
