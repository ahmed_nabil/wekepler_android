package com.wekepler.viewcontrollerslayer.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Mohammad Sayed on 8/23/2015.
 */
public class User implements Serializable {

    public User() {
    }

    @SerializedName("UserID")
    private int userId;

    @SerializedName("ProfileImage")
    private String profilePictureUrl;

    @SerializedName("UserName")
    private String userName;

    @SerializedName("FirstName")
    private String firstName;

    @SerializedName("LastName")
    private String lastName;

    @SerializedName("Email")
    private String email;

    @SerializedName("ScreenName")
    private String screenName;

    @SerializedName("Description")
    private String description;

    @SerializedName("FollowersCount")
    private int followersNumber;

    public User(User user) {
        this.userId = user.getUserId();
        this.userName = user.getUserName();
        this.profilePictureUrl = user.getProfilePictureUrl();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.screenName = user.getScreenName();
        this.email = user.getEmail();
        this.description = user.getDescription();
        this.followersNumber = user.getFollowersNumber();
        this.followingNumber = user.getFollowingNumber();
        this.videosNumber = user.getVideosNumber();
        this.gender = user.getGender();
    }

    @SerializedName("FollowingCount")
    private int followingNumber;

    @SerializedName("VideosCount")
    private int videosNumber;

    @SerializedName("Gender")
    private String gender;


    @SerializedName("Followed")
    private boolean followed;


    protected String password;
    protected String location;

    public String getProfilePictureUrl() {
        return profilePictureUrl;
    }

    public void setProfilePictureUrl(String profilePictureUrl) {
        this.profilePictureUrl = profilePictureUrl;
    }


    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getScreenName() {
        return firstName + " " + lastName;
        //return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getFollowersNumber() {
        return followersNumber;
    }

    public void setFollowersNumber(int followersNumber) {
        this.followersNumber = followersNumber;
    }

    public int getVideosNumber() {
        return videosNumber;
    }

    public void setVideosNumber(int videosNumber) {
        this.videosNumber = videosNumber;
    }

    public int getFollowingNumber() {
        return followingNumber;
    }

    public void setFollowingNumber(int followingNumber) {
        this.followingNumber = followingNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isFollowed() {
        return followed;
    }

    public void setFollowed(boolean followed) {
        this.followed = followed;
    }
}
