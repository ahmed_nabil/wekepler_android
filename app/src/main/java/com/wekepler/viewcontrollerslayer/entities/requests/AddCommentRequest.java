package com.wekepler.viewcontrollerslayer.entities.requests;

import com.google.gson.annotations.SerializedName;
import com.wekepler.viewcontrollerslayer.entities.User;

/**
 * Created by Mohammad Sayed on 9/5/2015.
 */
public class AddCommentRequest {

    @SerializedName("VideoID")
    private int videoId;

    @SerializedName("CommentText")
    private String commentText;

    @SerializedName("ParentCommentID")
    private int parentCommentId;


    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public int getParentCommentId() {
        return parentCommentId;
    }

    public void setParentCommentId(int parentCommentId) {
        this.parentCommentId = parentCommentId;
    }
}
