package com.wekepler.viewcontrollerslayer.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Mohammad Sayed on 8/22/2015.
 */
public class Post implements Serializable {


    @SerializedName("User")
    private User user;

    @SerializedName("VideoID")
    private int id;

    @SerializedName("Link")
    private String videoUrl;

    @SerializedName("CommentText")
    private String videoCaption;

    @SerializedName("ViewsCount")
    private int viewersNumber;

    @SerializedName("LikesCount")
    private int likesNumber;

    @SerializedName("CommentsCount")
    private int commentsNumber;

    /*@SerializedName("SharesCount")
    private int sharesNumber;*/


    @SerializedName("RepostCount")
    private int repostsNumber;

    @SerializedName("latitude")
    private double latitude;

    @SerializedName("longitude")
    private double longitude;

    @SerializedName("Comments")
    private ArrayList<Comment> comments;


    @SerializedName("Likes")
    private ArrayList<Like> likes;

    @SerializedName("Reposted")
    private boolean isReposted;

    @SerializedName("Liked")
    private boolean isLiked;

    @SerializedName("Commented")
    private boolean isCommented;

    @SerializedName("ThumbnailLink")
    private String videoThumbnailUrl;

    @SerializedName("UploadDate")
    private String postSince;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoCaption() {
        return videoCaption;
    }

    public void setVideoCaption(String videoCaption) {
        this.videoCaption = videoCaption;
    }

    public int getViewersNumber() {
        return viewersNumber;
    }

    public void setViewersNumber(int viewersNumber) {
        this.viewersNumber = viewersNumber;
    }

    public int getLikesNumber() {
        return likesNumber;
    }

    public void setLikesNumber(int likesNumber) {
        this.likesNumber = likesNumber;
    }

    public int getCommentsNumber() {
        return commentsNumber;
    }

    public void setCommentsNumber(int commentsNumber) {
        this.commentsNumber = commentsNumber;
    }

    /*public int getSharesNumber() {
        return sharesNumber;
    }

    public void setSharesNumber(int sharesNumber) {
        this.sharesNumber = sharesNumber;
    }*/

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public ArrayList<Like> getLikes() {
        return likes;
    }

    public void setLikes(ArrayList<Like> likes) {
        this.likes = likes;
    }

    public String getPostSince() {
        return postSince;
    }

    public void setPostSince(String postSince) {
        this.postSince = postSince;
    }

    public String getVideoThumbnailUrl() {
        return videoThumbnailUrl;
    }

    public void setVideoThumbnailUrl(String videoThumbnailUrl) {
        this.videoThumbnailUrl = videoThumbnailUrl;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setIsLiked(boolean isLiked) {
        this.isLiked = isLiked;
    }

    public boolean isCommented() {
        return isCommented;
    }

    public void setIsCommented(boolean isCommented) {
        this.isCommented = isCommented;
    }

    public boolean isReposted() {
        return isReposted;
    }

    public void setIsReposted(boolean isReposted) {
        this.isReposted = isReposted;
    }

    public int getRepostsNumber() {
        return repostsNumber;
    }

    public void setRepostsNumber(int repostsNumber) {
        this.repostsNumber = repostsNumber;
    }

    /*public boolean isShared() {
        return isShared;
    }

    public void setIsShared(boolean isShared) {
        this.isShared = isShared;
    }*/
}
