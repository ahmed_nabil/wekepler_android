package com.wekepler.viewcontrollerslayer.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Mohammad Sayed on 8/24/2015.
 */
public class Like implements Serializable {

    @SerializedName("LikeID")
    private int id;

    @SerializedName("VideoID")
    private int postId;

    @SerializedName("UserID")
    private int userId;

    @SerializedName("LikeDate")
    private String postSince;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getPostSince() {
        return postSince;
    }

    public void setPostSince(String postSince) {
        this.postSince = postSince;
    }
}
