package com.wekepler.viewcontrollerslayer.entities.parameters;

/**
 * Created by Mohammad Sayed on 9/12/2015.
 */
public class VideosParameters extends BaseParameters {
    public VideosParameters(int numberOfVideos, int maxVideoId) {
        parametersMap.put("PageSize", numberOfVideos);
        parametersMap.put("MaxmumID", maxVideoId);
    }
}
