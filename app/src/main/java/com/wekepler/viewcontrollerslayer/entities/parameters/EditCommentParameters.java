package com.wekepler.viewcontrollerslayer.entities.parameters;

/**
 * Created by Mohammad Sayed on 9/12/2015.
 */
public class EditCommentParameters extends BaseParameters {
    public EditCommentParameters(String comment) {
        parametersMap.put("CommentText", comment);
    }
}
