package com.wekepler.viewcontrollerslayer.entities.requests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mohammad Sayed on 9/5/2015.
 */
public class SendMessageRequest {

    @SerializedName("ReceiverID")
    private String receiverId;

    @SerializedName("MessageText")
    private String messageText;

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }
}
