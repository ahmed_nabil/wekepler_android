package com.wekepler.viewcontrollerslayer.entities.requests;

import com.google.gson.annotations.SerializedName;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.User;

/**
 * Created by Mohammad Sayed on 9/5/2015.
 */
public class LoginRequest extends AuthenticationRequest {

    @SerializedName("grant_type")
    private String grantType;

    public LoginRequest(User user) {
        super(user);
        this.grantType = Constants.GRANT_TYPE_PASSOWRD;
    }

    public String getGrantType() {
        return grantType;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }
}
