package com.wekepler.viewcontrollerslayer.entities.requests;

import com.google.gson.annotations.SerializedName;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.User;

/**
 * Created by Mohammad Sayed on 9/5/2015.
 */
public class AuthenticationRequest {

    @SerializedName("username")
    private String userName;

    @SerializedName("password")
    private String password;

    public AuthenticationRequest(User user) {
        this.userName = user.getUserName();
        this.password = user.getPassword();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
