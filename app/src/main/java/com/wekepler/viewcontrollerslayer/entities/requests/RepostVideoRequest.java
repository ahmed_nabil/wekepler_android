package com.wekepler.viewcontrollerslayer.entities.requests;

import com.google.gson.annotations.SerializedName;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.User;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Mohammad Sayed on 9/5/2015.
 */
public class RepostVideoRequest {

    public RepostVideoRequest(int videoId, String commentText) {
        this.videoId = videoId;
        this.commentText = commentText;
    }

    @SerializedName("VideoID")
    private int videoId;

    @SerializedName("CommentText")
    private String commentText;

    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }
}
