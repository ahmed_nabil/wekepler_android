package com.wekepler.viewcontrollerslayer.entities.requests;

import com.google.gson.annotations.SerializedName;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.User;

/**
 * Created by Mohammad Sayed on 9/5/2015.
 */
public class RegistrationRequest extends AuthenticationRequest {

    @SerializedName("email")
    private String email;

    @SerializedName("confirmpassword")
    private String confirmPassword;

    @SerializedName("FirstName")
    private String firstName;

    @SerializedName("LastName")
    private String lastName;


    public RegistrationRequest(User user) {
        super(user);
        this.email = user.getEmail();
        this.confirmPassword = user.getPassword();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
