package com.wekepler.viewcontrollerslayer.entities.parameters;

/**
 * Created by Mohammad Sayed on 9/12/2015.
 */
public class RepostVideoParameters extends BaseParameters {
    public RepostVideoParameters(int repostId, int userId, int videoId, String date) {
        parametersMap.put("RepostID", repostId);
        parametersMap.put("UserID", userId);
        parametersMap.put("VideoID", videoId);
        parametersMap.put("RepostDate", date);
    }
}
