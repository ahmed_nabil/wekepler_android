package com.wekepler.viewcontrollerslayer.entities;

/**
 * Created by Mohammad Sayed on 8/29/2015.
 */
public interface OnModelResponseListener {
    public void onSuccess(String operationTag, Object... objects);

    public void onError(String operationTag, String error, Object... objects);
}
