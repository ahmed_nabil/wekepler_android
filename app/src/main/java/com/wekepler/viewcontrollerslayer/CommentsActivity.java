package com.wekepler.viewcontrollerslayer;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.wekepler.R;
import com.wekepler.modelslayer.AddCommentToVideoModel;
import com.wekepler.modelslayer.GetCommentsModel;
import com.wekepler.viewcontrollerslayer.adapters.CommentsListAdapter;
import com.wekepler.viewcontrollerslayer.entities.Comment;
import com.wekepler.viewcontrollerslayer.entities.requests.AddCommentRequest;

import java.util.ArrayList;

public class CommentsActivity extends BaseActivity implements View.OnClickListener {

    private String mCommentId;
    private ArrayList<Comment> mCommentsList;
    private ListView mListView;
    private CommentsListAdapter mAdapter;
    private ImageView mIvBack;
    private EditText mEtComment;
    private Button mBtnSend;
    private ProgressBar mPbLoading;
    private int mVideoId;
    private final int REQUEST_CODE_EDIT_COMMENT = 11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        initializeViews();

    }

    @Override
    public void initializeViews() {
        mCommentId = getIntent().getStringExtra(Constants.COMMENT_ID_TAG);
        mIvBack = (ImageView) findViewById(R.id.iv_left_icon_action_bar);
        mListView = (ListView) findViewById(R.id.lv_comments);
        mEtComment = (EditText) findViewById(R.id.et_comment);
        mBtnSend = (Button) findViewById(R.id.btn_send);
        mPbLoading = (ProgressBar) findViewById(R.id.pb_loading);
        mIvBack.setOnClickListener(this);
        mBtnSend.setOnClickListener(this);

        getCommentsList();
        mAdapter = new CommentsListAdapter(this, mCommentsList, false, this);
        mListView.setAdapter(mAdapter);
    }

    private ArrayList<Comment> getCommentsList() {
        mCommentsList = new ArrayList<>();
        Bundle bundle = getIntent().getBundleExtra(Constants.BUNDLE_TAG);
        mVideoId = bundle.getInt(Constants.VIDEO_ID_TAG);
        mPbLoading.setVisibility(View.VISIBLE);
        new GetCommentsModel(this, mVideoId, this);
        return mCommentsList;
    }

    @Override
    public void onClick(View v) {
        if (v == mIvBack) {
            finish();
        } else if (v == mBtnSend) {
            String comment = getTextFromEditText(mEtComment, true);
            if (comment.equals("")) {
                toastLong(R.string.no_comments_to_post);
            } else {
                AddCommentRequest addCommentRequest = new AddCommentRequest();
                addCommentRequest.setCommentText(comment);
                addCommentRequest.setVideoId(mVideoId);
                addCommentRequest.setParentCommentId(1);
                new AddCommentToVideoModel(this, addCommentRequest, this);
            }
        }
    }

    @Override
    public void onSuccess(String operationTag, Object... objects) {
        mPbLoading.setVisibility(View.INVISIBLE);
        if (operationTag.equals(Constants.GET_COMMENTS_TAG)) {
            mCommentsList = (ArrayList<Comment>) objects[0];
            mAdapter.setItems(mCommentsList);
            mAdapter.notifyDataSetChanged();
        } else if (operationTag.equals(Constants.ADD_COMMENT_VIDEO_TAG)) {
            mEtComment.setText("");
            getCommentsList();
        } else if (operationTag.equals(Constants.DELETE_COMMENT_TAG)) {
            getCommentsList();
        }

    }

    @Override
    public void onError(String operationTag, String error, Object... objects) {
        mPbLoading.setVisibility(View.INVISIBLE);
        toastLong(error);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_EDIT_COMMENT) {
            if (resultCode == RESULT_OK) {
                getCommentsList();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
