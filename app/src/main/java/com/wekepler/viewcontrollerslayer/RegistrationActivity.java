package com.wekepler.viewcontrollerslayer;

import android.support.v4.app.NavUtils;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.wekepler.R;
import com.wekepler.modelslayer.RegistrationModel;
import com.wekepler.viewcontrollerslayer.entities.User;
import com.wekepler.viewcontrollerslayer.entities.requests.RegistrationRequest;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistrationActivity extends BaseActivity {

    private EditText mEtUsername;
    private EditText mEtFirstName;
    private EditText mEtLastName;
    private EditText mEtEmail;
    private EditText mEtPassword;
    private CheckBox mCbTerms;
    private ProgressBar mPbLoading;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        initializeViews();
    }

    @Override
    public void initializeViews() {
        customizeActionBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mEtUsername = (EditText) findViewById(R.id.et_username_registration);
        mEtFirstName = (EditText) findViewById(R.id.et_first_name_registration);
        mEtLastName = (EditText) findViewById(R.id.et_last_name_registration);
        mEtEmail = (EditText) findViewById(R.id.et_email_registration);
        mEtPassword = (EditText) findViewById(R.id.et_password_registration);
        mCbTerms = (CheckBox) findViewById(R.id.cb_terms_and_policies_registration);
        mPbLoading = (ProgressBar) findViewById(R.id.pb_loading);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_registration, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_sign_up) {
            mPbLoading.setVisibility(View.VISIBLE);
            if (checkValidation()) {
                RegistrationRequest registrationRequest = createRegistrationRequest();
                new RegistrationModel(this, registrationRequest, this);
                return true;
            } else
                mPbLoading.setVisibility(View.INVISIBLE);

        } else if (id == android.R.id.home) {
            setResult(RESULT_CANCELED);
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private boolean checkValidation() {
        String email = getTextFromEditText(mEtEmail, true);
        if (getTextFromEditText(mEtUsername, true).equals("")) {
            toastLong(R.string.empty_username);
            return false;
        } else if (getTextFromEditText(mEtFirstName, true).equals("")) {
            toastLong(R.string.empty_first_name);
            return false;
        } else if (getTextFromEditText(mEtLastName, true).equals("")) {
            toastLong(R.string.empty_last_name);
            return false;
        } else if (email.equals("")) {
            toastLong(R.string.empty_email);
            return false;
        } else if (!validateEmailRegex(email)) {
            toastLong(R.string.invalid_email);
            return false;
        } else if (getTextFromEditText(mEtPassword, false).equals("")) {
            toastLong(R.string.empty_password);
            return false;
        } else if (!mCbTerms.isChecked()) {
            toastLong(R.string.must_agree_on_terms);
            return false;
        }
        return true;
    }

    private boolean validateEmailRegex(String email) {
        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    private RegistrationRequest createRegistrationRequest() {
        User user = new User();
        user.setUserName(getTextFromEditText(mEtUsername, true));
        user.setFirstName(getTextFromEditText(mEtFirstName, true));
        user.setLastName(getTextFromEditText(mEtLastName, true));
        user.setEmail(getTextFromEditText(mEtEmail, true));
        user.setPassword(getTextFromEditText(mEtPassword, false));
        return new RegistrationRequest(user);
    }


    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void onSuccess(String operationTag, Object... objects) {
        if (operationTag.equals(Constants.REGISTRATION_TAG)) {
            mPbLoading.setVisibility(View.INVISIBLE);
            setResult(RESULT_OK);
            finish();
        }
    }


    @Override
    public void onError(String operationTag, String error, Object... objects) {
        mPbLoading.setVisibility(View.INVISIBLE);
        toastLong(error);
    }
}