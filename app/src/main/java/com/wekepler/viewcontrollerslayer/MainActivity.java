package com.wekepler.viewcontrollerslayer;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wekepler.R;
import com.wekepler.SharedPreferencesUtil;
import com.wekepler.viewcontrollerslayer.adapters.ViewPagerAdapter;
import com.wekepler.viewcontrollerslayer.entities.User;
import com.wekepler.viewcontrollerslayer.fragments.BaseFragment;
import com.wekepler.viewcontrollerslayer.fragments.BaseListFragment;
import com.wekepler.widgets.SlidingTabLayout;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends BaseActivity {


    private ViewPagerAdapter adapter;
    private ViewPager pager;
    private ArrayList<String> mTitles = new ArrayList<>();
    private SlidingTabLayout tabs;
    private File mVideoFile;
    private BaseListFragment mBaseListFragment;
    private BaseFragment mBaseFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeViews();
    }

    @Override
    public void initializeViews() {
        //initialize ActionBar
        customizeActionBar();

        //initialize IDs


        //initialize listener

        //-------- initialize adapters-----------------------------

        mTitles.add(getString(R.string.home));
        mTitles.add(getString(R.string.profile));
        mTitles.add(getString(R.string.messages));

        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter = new ViewPagerAdapter(this, getSupportFragmentManager(), mTitles);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(mTitles.size());

        /*// Give the PagerSlidingTabStrip the ViewPager
        PagerSlidingTabStrip tabsStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        // Attach the view pager to the tab strip
        tabsStrip.setViewPager(pager);*/


        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width
        tabs.setImageCustomTabView(R.layout.tab_layout, R.id.iv_icon);
        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabs_selector_color);
            }
        });
        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);

        //----------------------------------------------------------
    }

    /**
     * @param right          true if right icon false if left icon
     * @param iconResourceId resource id of image to be replaced
     * @param listener       what to do when clicked
     */
    public void changeToolbarIconProperties(boolean right, int iconResourceId, View.OnClickListener listener) {
        ImageView iconImageView = right ? (ImageView) findViewById(R.id.ib_right_icon_action_bar)
                : (ImageView) findViewById(R.id.iv_left_icon_action_bar);
        iconImageView.setImageResource(iconResourceId);
        iconImageView.setOnClickListener(listener);
        if (iconImageView.getVisibility() != View.VISIBLE)
            showToolbarIcon(right);
    }

    public void changeToolbarLogo(int iconResourceId) {
        ImageView iconImageView = (ImageView) findViewById(R.id.iv_logo_action_bar);
        iconImageView.setImageResource(iconResourceId);
    }

    /**
     * @param right true if right icon false if left icon
     */
    public void hideToolbarIcon(boolean right) {
        ImageView iconImageView = right ? (ImageView) findViewById(R.id.ib_right_icon_action_bar)
                : (ImageView) findViewById(R.id.iv_left_icon_action_bar);
        iconImageView.setVisibility(View.INVISIBLE);
    }

    /**
     * @param right true if right icon false if left icon
     */
    public void showToolbarIcon(boolean right) {
        ImageView iconImageView = right ? (ImageView) findViewById(R.id.ib_right_icon_action_bar)
                : (ImageView) findViewById(R.id.iv_left_icon_action_bar);
        iconImageView.setVisibility(View.VISIBLE);
    }

    public void displayToolbarCenterTitle(String title) {
        ImageView logoImageView = (ImageView) findViewById(R.id.iv_logo_action_bar);
        logoImageView.setVisibility(View.INVISIBLE);
        TextView titleTextView = (TextView) findViewById(R.id.tv_center_title);
        titleTextView.setVisibility(View.VISIBLE);
        titleTextView.setText(title);
    }

    public void displayToolbarWekeplerLogo() {
        TextView titleTextView = (TextView) findViewById(R.id.tv_center_title);
        titleTextView.setVisibility(View.INVISIBLE);
        ImageView logoImageView = (ImageView) findViewById(R.id.iv_logo_action_bar);
        logoImageView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSuccess(String operationTag, Object... objects) {

    }

    @Override
    public void onError(String operationTag, String error, Object... objects) {
        toastLong(error);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.VIDEO_CAPTURE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String videoPath;
                if (data == null) {
                    if (mVideoFile != null)
                        videoPath = mVideoFile.getAbsolutePath();
                    else return;
                } else
                    videoPath = data.getData().getPath();
                goToActivity(PostVideoActivity.class, false, Constants.VIDEO_PATH_TAG, videoPath);
            }
        } else if (requestCode == Constants.VIDEO_PICK_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Get (file) URI of the vid from the return Intent's data
                Uri videoUri = data.getData();
                String videoPath = getPath(videoUri);
                goToActivity(PostVideoActivity.class, false, Constants.VIDEO_PATH_TAG, videoPath);
            }
        } else if (requestCode == Constants.EDIT_SETTINGS_REQUEST_CODE) {
            if (resultCode == RESULT_FIRST_USER) {
                SharedPreferencesUtil.getInstance(this).clearDataForLogout();
                goToActivity(LoginActivity.class, true);
            }
        } else if (requestCode == Constants.VIEW_VIDEO_DETAILS_REQUEST_CODE) {
            if (mBaseListFragment != null)
                mBaseListFragment.onActivityResult(requestCode, resultCode, data);
            else if (mBaseFragment != null)
                mBaseFragment.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void setVideoFile(File videoFile) {
        this.mVideoFile = videoFile;
    }

    public String getAbsolutePath(Uri uri) {
        String[] projection = {MediaStore.Video.Media._ID,
                MediaStore.Video.Media.DATA,
                MediaStore.Video.Media.DISPLAY_NAME,
                MediaStore.Video.Media.SIZE};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(projection[0]);
        String path = cursor.getString(columnIndex); // returns null
        cursor.close();
        return path;
    }

    private String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA, MediaStore.Video.Media.SIZE, MediaStore.Video.Media.DURATION};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA));
        cursor.close();
        return path;
    }

    public void setBaseListFragment(BaseListFragment baseListFragment) {
        this.mBaseListFragment = baseListFragment;
    }
}
