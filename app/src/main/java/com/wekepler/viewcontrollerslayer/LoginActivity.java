package com.wekepler.viewcontrollerslayer;

import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.wekepler.R;
import com.wekepler.modelslayer.LoginModel;
import com.wekepler.modelslayer.RegisterExternalModel;
import com.wekepler.modelslayer.UserInfoModel;
import com.wekepler.SharedPreferencesUtil;
import com.wekepler.serviceslayer.entities.responses.Token;
import com.wekepler.viewcontrollerslayer.entities.Responses.UserInfoResponse;
import com.wekepler.viewcontrollerslayer.entities.User;
import com.wekepler.viewcontrollerslayer.entities.requests.LoginRequest;
import com.wekepler.viewcontrollerslayer.entities.requests.RegisterExternalRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginActivity extends BaseActivity implements View.OnClickListener, FacebookCallback<LoginResult>,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GetGoogleTokenAsyncTask.OnTokenRetrievedListener {

    private final int REGISTRATION_CODE = 10;
    private EditText mEtUsername;
    private EditText mEtPassword;
    private TextView mTvForgotPassword;
    private ImageButton mIbtnLogin;
    private TextView mTvSignUp;
    private ProgressBar mPbLoading;

    private int mLoginType = -1;

    //----------------------- Facebook components -------------------------------
    private ImageButton mIbtnFacebookLogin;
    private LoginButton mIbtnHiddenFacebookLogin;
    private CallbackManager mCallbackManager;
    private final String FACEBOOK_LOGIN_PERMISSION = "user_friends";

    //----------------------- Google Plus components -------------------------------
    private ImageButton mIbtnGooglePlusLogin;
    /* Request code used to invoke sign in user interactions. */
    private static final int GOOGLE_SIGN_IN_REQUEST_CODE = 0;
    /* Client used to interact with Google APIs. */
    private GoogleApiClient mGoogleApiClient;
    /* Is there a ConnectionResult resolution in progress? */
    private boolean mIsResolving = false;
    /* Should we automatically resolve ConnectionResults when possible? */
    private boolean mShouldResolve = false;

    //----------------------- Twitter components -------------------------------
    private ImageButton mIbtnTwitterLogin;
    private TwitterLoginButton mIbtnHiddenTwitterLogin;
    private Callback<TwitterSession> mTwitterSessionCallback = new Callback<TwitterSession>() {
        @Override
        public void success(Result<TwitterSession> result) {
            TwitterSession twitterSession = result.data;
            SharedPreferencesUtil.getInstance(LoginActivity.this).putInt(Constants.LOGIN_TYPE_TAG, Constants.LOGIN_TYPE_TWITTER_CODE);
            SharedPreferencesUtil.getInstance(LoginActivity.this).putString(Constants.TOKEN_STRING_TAG, twitterSession.getAuthToken().token);
            new UserInfoModel(LoginActivity.this, LoginActivity.this);
        }

        @Override
        public void failure(TwitterException e) {
            mPbLoading.setVisibility(View.INVISIBLE);
            toastLong(e.getMessage());
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //getKeyHash();
        initializeViews();

        Token token = SharedPreferencesUtil.getInstance(this).getToken();
        if (token != null) {
            goToActivity(MainActivity.class, true);
        } else {

            // Build GoogleApiClient with access to basic profile
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(Plus.API)
                    .addScope(new Scope(Scopes.PROFILE))
                    .build();
        }
    }

    @Override
    public void initializeViews() {

        //initialize IDs
        mEtUsername = (EditText) findViewById(R.id.et_username_login);
        mEtPassword = (EditText) findViewById(R.id.et_password_login);
        mTvForgotPassword = (TextView) findViewById(R.id.tv_forgot_password_login);
        mIbtnLogin = (ImageButton) findViewById(R.id.ibtn_login_login);
        mIbtnFacebookLogin = (ImageButton) findViewById(R.id.ibtn_facebook_login);
        mIbtnHiddenFacebookLogin = (LoginButton) findViewById(R.id.ibtn_hidden_facebook_login);
        mIbtnGooglePlusLogin = (ImageButton) findViewById(R.id.ibtn_google_plus_login);
        mIbtnTwitterLogin = (ImageButton) findViewById(R.id.ibtn_twitter_login);
        mIbtnHiddenTwitterLogin = (TwitterLoginButton) findViewById(R.id.ibtn_hidden_twitter_login);
        mTvSignUp = (TextView) findViewById(R.id.tv_signup_login);
        underLineText(mTvSignUp);
        mPbLoading = (ProgressBar) findViewById(R.id.pb_loading);

        //initialize listener
        mTvForgotPassword.setOnClickListener(this);
        mIbtnLogin.setOnClickListener(this);
        mIbtnFacebookLogin.setOnClickListener(this);
        mIbtnGooglePlusLogin.setOnClickListener(this);
        mIbtnTwitterLogin.setOnClickListener(this);
        mTvSignUp.setOnClickListener(this);

        mCallbackManager = new CallbackManager.Factory().create();
        mIbtnHiddenFacebookLogin.setReadPermissions(FACEBOOK_LOGIN_PERMISSION);
        mIbtnHiddenFacebookLogin.registerCallback(mCallbackManager, this);

        mIbtnHiddenTwitterLogin.setCallback(mTwitterSessionCallback);
    }

    @Override
    public void onClick(View v) {
        if (v == mIbtnLogin) {
            //goToActivity(MainActivity.class, true);
            mPbLoading.setVisibility(View.VISIBLE);
            if (checkValidation()) {
                mLoginType = Constants.LOGIN_TYPE_ACCOUNT_CODE;
                LoginRequest loginRequest = createLoginRequest();
                new LoginModel(this, loginRequest, this);
            } else {
                mPbLoading.setVisibility(View.INVISIBLE);
            }
        } else if (v == mIbtnFacebookLogin) {
            mPbLoading.setVisibility(View.VISIBLE);
            mLoginType = Constants.LOGIN_TYPE_FACEBOOK_CODE;
            mIbtnHiddenFacebookLogin.performClick();
            //goToActivity(MainActivity.class, true);
        } else if (v == mIbtnGooglePlusLogin) {
            mPbLoading.setVisibility(View.VISIBLE);
            mLoginType = Constants.LOGIN_TYPE_GOOGLE_PLUS_CODE;
            // User clicked the sign-in button, so begin the sign-in process and automatically
            // attempt to resolve any errors that occur.
            mShouldResolve = true;
            mGoogleApiClient.connect();
            // Show a message to the user that we are signing in.
            //goToActivity(MainActivity.class, true);
        } else if (v == mIbtnTwitterLogin) {
            mPbLoading.setVisibility(View.VISIBLE);
            mLoginType = Constants.LOGIN_TYPE_TWITTER_CODE;
            mIbtnHiddenTwitterLogin.performClick();
            //goToActivity(MainActivity.class, true);
        } else if (v == mTvSignUp) {
            goToActivityForResult(RegistrationActivity.class, REGISTRATION_CODE);
        }
    }

    private boolean checkValidation() {

        //String email = getTextFromEditText(mEtUsername, true);
        /* else if (validateEmailRegex(email)) {
            toastLong(R.string.invalid_email);
            return false;
        }*/
        if (getTextFromEditText(mEtUsername, true).equals("")) {
            toastLong(R.string.empty_username);
            return false;
        } else if (getTextFromEditText(mEtPassword, false).equals("")) {
            toastLong(R.string.empty_password);
            return false;
        }
        return true;
    }

    /*private boolean validateEmailRegex(String email) {
        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }*/

    private LoginRequest createLoginRequest() {
        User user = new User();
        user.setUserName(getTextFromEditText(mEtUsername, true));
        user.setPassword(getTextFromEditText(mEtPassword, false));
        return new LoginRequest(user);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REGISTRATION_CODE) {
            if (resultCode == RESULT_OK) {
                toastLong(R.string.registered_successfully);
                //goToActivity(MainActivity.class, true);
            }
        } else if (requestCode == GOOGLE_SIGN_IN_REQUEST_CODE) {
            // If the error resolution was not successful we should not resolve further.
            if (resultCode != RESULT_OK) {
                mShouldResolve = false;
            }
            mIsResolving = false;
            mGoogleApiClient.connect();
        }

        super.onActivityResult(requestCode, resultCode, data);

        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        mIbtnHiddenTwitterLogin.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSuccess(String operationTag, Object... objects) {
        mPbLoading.setVisibility(View.INVISIBLE);
        if (operationTag.equals(Constants.LOGIN_TAG)) {
            goToActivity(MainActivity.class, true);
            SharedPreferencesUtil.getInstance(this).putInt(Constants.LOGIN_TYPE_TAG, Constants.LOGIN_TYPE_ACCOUNT_CODE);
            toastLong("Logged in successfully");
        } else if (operationTag.equals(Constants.USER_INFO_TAG)) {
            UserInfoResponse userInfoResponse = (UserInfoResponse) objects[0];
            if (userInfoResponse.isHasRegistered()) {
                mPbLoading.setVisibility(View.INVISIBLE);
                goToActivity(MainActivity.class, true);
                toastLong("Logged in successfully");
            } else {
                if (mLoginType == Constants.LOGIN_TYPE_FACEBOOK_CODE) {
                    getFacebookUserNameThenRegisterExternal();
                } else if (mLoginType == Constants.LOGIN_TYPE_GOOGLE_PLUS_CODE) {
                    String accountName = Plus.AccountApi.getAccountName(mGoogleApiClient);
                    registerExternal(accountName);
                } else if (mLoginType == Constants.LOGIN_TYPE_TWITTER_CODE) {
                    String username = Twitter.getSessionManager().getActiveSession().getUserName();
                    registerExternal(username);
                }
            }
        } else if (operationTag.equals(Constants.REGISTER_EXTERNAL_TAG)) {
            mPbLoading.setVisibility(View.INVISIBLE);
            goToActivity(MainActivity.class, true);
        }
    }

    @Override
    public void onError(String operationTag, String error, Object... objects) {
        mPbLoading.setVisibility(View.INVISIBLE);
        toastLong(error);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        String token = loginResult.getAccessToken().getToken();
        SharedPreferencesUtil.getInstance(this).putInt(Constants.LOGIN_TYPE_TAG, Constants.LOGIN_TYPE_FACEBOOK_CODE);
        SharedPreferencesUtil.getInstance(this).putString(Constants.TOKEN_STRING_TAG, token);
        new UserInfoModel(this, this);
    }

    @Override
    public void onCancel() {
        //Do nothing
        toastLong("cancelled");
        mPbLoading.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onError(FacebookException error) {
        toastLong(error.getMessage());
        mPbLoading.setVisibility(View.INVISIBLE);

    }

    private void getKeyHash() {
        PackageInfo info = null;
        try {
            info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    private void getFacebookUserNameThenRegisterExternal() {
        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        try {
                            String username = object.getString("name");
                            registerExternal(username);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            toastLong("failed to login");
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void registerExternal(String username) {
        RegisterExternalRequest registerExternalRequest = new RegisterExternalRequest();
        registerExternalRequest.setUserName(username);
        new RegisterExternalModel(LoginActivity.this, registerExternalRequest, LoginActivity.this);
    }

    /*@Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }*/


    @Override
    public void onConnected(Bundle bundle) {
        // onConnected indicates that an account was selected on the device, that the selected
        // account has granted any requested permissions to our app and that we were able to
        // establish a service connection to Google Play services.
        Log.d("LoginActivity", "onConnected:" + bundle);
        mShouldResolve = false;
        new GetGoogleTokenAsyncTask(this, mGoogleApiClient, this).execute();
        // Show the signed-in UI
        //showSignedInUI();

    }

    @Override
    public void onConnectionSuspended(int i) {
        mPbLoading.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // Could not connect to Google Play Services.  The user needs to select an account,
        // grant permissions or resolve an error in order to sign in. Refer to the javadoc for
        // ConnectionResult to see possible error codes.
        mPbLoading.setVisibility(View.INVISIBLE);
        Log.d("LoginActivity", "onConnectionFailed:" + connectionResult);

        if (!mIsResolving && mShouldResolve) {
            if (connectionResult.hasResolution()) {
                try {
                    connectionResult.startResolutionForResult(this, GOOGLE_SIGN_IN_REQUEST_CODE);
                    mIsResolving = true;
                } catch (IntentSender.SendIntentException e) {
                    Log.e("LoginActivity", "Could not resolve ConnectionResult.", e);
                    mIsResolving = false;
                    mGoogleApiClient.connect();
                }
            } else {
                // Could not resolve the connection result, show the user an
                // error dialog.
                toastLong(connectionResult.toString());
                //showErrorDialog(connectionResult);
            }
        } else {
            // Show the signed-out UI
            //showSignedOutUI();
        }
    }

    private void signoutGoogle() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onTokenRetrieved(String token) {
        SharedPreferencesUtil.getInstance(this).putInt(Constants.LOGIN_TYPE_TAG, Constants.LOGIN_TYPE_GOOGLE_PLUS_CODE);
        SharedPreferencesUtil.getInstance(this).putString(Constants.TOKEN_STRING_TAG, token);
        new UserInfoModel(this, this);
    }

    @Override
    public void onTokenRetrievedError(String error) {
        mPbLoading.setVisibility(View.INVISIBLE);
        toastLong(error);
    }
}