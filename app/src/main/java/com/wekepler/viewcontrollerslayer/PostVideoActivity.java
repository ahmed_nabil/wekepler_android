package com.wekepler.viewcontrollerslayer;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wekepler.R;
import com.wekepler.modelslayer.UploadVideoModel;
import com.wekepler.modelslayer.UploadVideoThumbnailModel;
import com.wekepler.viewcontrollerslayer.entities.Post;
import com.wekepler.viewcontrollerslayer.entities.parameters.UploadVideoParameters;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class PostVideoActivity extends BaseActivity implements MediaPlayer.OnPreparedListener, View.OnClickListener {

    private ImageView mIvVideoThumbnail;
    private String mVideoPath;
    private ImageView mIvBack;
    private TextView mTvPostVideo;
    private ImageButton mIbtnPlay;
    private EditText mEtVideoCaption;
    private ProgressBar mPbUploading;
    private UploadVideoModel mUploadVideoModel;
    private UploadVideoThumbnailModel mUploadVideoThumbnailModel;
    private boolean uploadVideo = true;
    private Bitmap mThumbnailBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_video);
        initializeViews();
    }

    @Override
    public void initializeViews() {
        mVideoPath = getIntent().getStringExtra(Constants.VIDEO_PATH_TAG);
        mIvBack = (ImageView) findViewById(R.id.iv_left_icon_action_bar);
        mTvPostVideo = (TextView) findViewById(R.id.ib_right_icon_action_bar);
        mIbtnPlay = (ImageButton) findViewById(R.id.ib_play);
        mEtVideoCaption = (EditText) findViewById(R.id.et_video_caption);
        mIvVideoThumbnail = (ImageView) findViewById(R.id.iv_video_thumbnail);
        mPbUploading = (ProgressBar) findViewById(R.id.pb_uploading);
        //mPbLoading = (ProgressBar) findViewById(R.id.pb_loading);

        //ImageLoader.getInstance().displayImage(mVideoPath, mIvVideoThumbnail);
        mThumbnailBitmap = getVideoThumbnail();
        mIvVideoThumbnail.setImageBitmap(mThumbnailBitmap);

        mIbtnPlay.setOnClickListener(this);
        mIvBack.setOnClickListener(this);
        mTvPostVideo.setOnClickListener(this);

    }

    @Override
    public void onSuccess(String operationTag, Object... objects) {
        if (operationTag.equals(Constants.UPLOAD_VIDEO_TAG)) {
            Post post = (Post) objects[0];
            mPbUploading.setProgress(0);
            File imageFile = getFile(mThumbnailBitmap);
            if (imageFile != null)
                mUploadVideoThumbnailModel = new UploadVideoThumbnailModel(this, post.getId(), "wekepler_image_video_" + post.getId(), imageFile, this, mPbUploading);
            else
                toastLong("Thumbnail file not exists");
        } else if (operationTag.equals(Constants.UPLOAD_VIDEO_THUMBNAIL_TAG)) {
            mPbUploading.setVisibility(View.INVISIBLE);
            toastLong("uploaded successfully");
            finish();
        }
    }

    private File getFile(Bitmap mThumbnailBitmap) {
        try {
            File f = new File(getCacheDir(), "Image" + System.currentTimeMillis());
            f.createNewFile();

            //Convert bitmap to byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            mThumbnailBitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapData = bos.toByteArray();

            //write the bytes in file
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapData);
            fos.flush();
            fos.close();
            return f;
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
    }

    @Override
    public void onClick(View v) {
        if (v == mIvBack)
            onBackPressed();
        else if (v == mTvPostVideo) {

            if (mVideoPath != null) {
                mPbUploading.setVisibility(View.VISIBLE);
                String title = "wekepler_uploads_" + System.currentTimeMillis();
                String commentText = getTextFromEditText(mEtVideoCaption, true);
                UploadVideoParameters uploadVideoParameters = new UploadVideoParameters(title, commentText, 0.0, 0.0);
                File file = new File(mVideoPath);
                if (file.exists()) {
                    mUploadVideoModel = new UploadVideoModel(this, uploadVideoParameters, file, this, mPbUploading);
                } else
                    toastLong("file not found");
            } else
                toastLong("no path found");

        } else if (v == mIbtnPlay) {
            Intent intent = new Intent(this, VideoViewerActivity.class);
            intent.putExtra(Constants.VIDEO_PATH_TAG, mVideoPath);
            startActivity(intent);
        }
    }

    @Override
    public void onError(String operationTag, String error, Object... objects) {
        mPbUploading.setVisibility(View.INVISIBLE);
        toastLong(error);
    }

    private Bitmap getVideoThumbnail() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(mVideoPath,
                MediaStore.Video.Thumbnails.MINI_KIND);
        return thumb;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (uploadVideo && mUploadVideoModel != null) {
            mUploadVideoModel.registerUploadReceiver();
        } else if (!uploadVideo && mUploadVideoThumbnailModel != null)
            mUploadVideoThumbnailModel.registerUploadReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (uploadVideo && mUploadVideoModel != null) {
            mUploadVideoModel.unregisterUploadReceiver();
        } else if (!uploadVideo && mUploadVideoThumbnailModel != null)
            mUploadVideoThumbnailModel.unregisterUploadReceiver();
    }
}
