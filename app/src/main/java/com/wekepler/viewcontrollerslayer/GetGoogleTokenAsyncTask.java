package com.wekepler.viewcontrollerslayer;

import android.accounts.Account;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;

import java.io.IOException;

/**
 * Created by Mohammad Sayed on 9/21/2015.
 */
public class GetGoogleTokenAsyncTask extends AsyncTask<Void, Void, String> {

    private GoogleApiClient mGoogleApiClient;
    private Context mContext;
    private final String TAG = "GetGoogleTokenAsyncTask";
    private final String SERVER_CLIENT_ID = "70018831827-5femef6oil68tuj1vu6lh2cu9ip401ne.apps.googleusercontent.com";
    private OnTokenRetrievedListener mOnTokenRetrievedListener;

    public interface OnTokenRetrievedListener {
        public void onTokenRetrieved(String token);

        public void onTokenRetrievedError(String error);
    }


    public GetGoogleTokenAsyncTask(Context context, GoogleApiClient googleApiClient, OnTokenRetrievedListener listener) {
        this.mContext = context;
        this.mGoogleApiClient = googleApiClient;
        this.mOnTokenRetrievedListener = listener;
    }

    @Override
    protected String doInBackground(Void... params) {
        String accountName = Plus.AccountApi.getAccountName(mGoogleApiClient);
        Account account = new Account(accountName, GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);
        String scopes = "audience:server:client_id:" + SERVER_CLIENT_ID; // Not the app's client ID.
        try {
            return GoogleAuthUtil.getToken(mContext.getApplicationContext(), account, scopes);
        } catch (IOException e) {
            Log.e(TAG, "Error retrieving ID token.", e);
            return null;
        } catch (GoogleAuthException e) {
            Log.e(TAG, "Error retrieving ID token.", e);
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        Log.i(TAG, "ID token: " + result);
        if (result != null) {
            // Successfully retrieved ID Token
            mOnTokenRetrievedListener.onTokenRetrieved(result);
        } else {
            // There was some error getting the ID Token
            mOnTokenRetrievedListener.onTokenRetrievedError("There was some error getting the ID Token");
        }
    }

}
