package com.wekepler.viewcontrollerslayer;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.wekepler.R;
import com.wekepler.viewcontrollerslayer.entities.User;

public class SettingsActivity extends BaseActivity implements View.OnClickListener {

    private User mUser;
    private ImageView mIvBack;
    private Button mBtnSave;
    private ImageView mIvProfileCover;
    private ImageView mIvProfilePicture;
    private ImageButton mIbEditProfileCover;
    private ImageButton mIbEditProfilePicture;
    private EditText mEtScreenName;
    private EditText mEtUserName;
    private EditText mEtStatus;
    private EditText mEtLocation;
    private Button mBtnChangePassword;
    private Button mBtnPrivacy;
    private Button mBtnAboutWekepler;
    private Button mBtnLogout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initializeViews();
    }

    @Override
    public void initializeViews() {
        mUser = (User) getIntent().getSerializableExtra(Constants.USER_TAG);
        mIvBack = (ImageView) findViewById(R.id.iv_left_icon_action_bar);
        mBtnSave = (Button) findViewById(R.id.btn_right_icon_action_bar);
        mIvProfileCover = (ImageView) findViewById(R.id.iv_profile_cover);
        mIvProfilePicture = (ImageView) findViewById(R.id.iv_profile_picture);
        mIbEditProfileCover = (ImageButton) findViewById(R.id.ib_edit_profile_cover);
        mIbEditProfilePicture = (ImageButton) findViewById(R.id.ib_edit_profile_picture);
        mEtScreenName = (EditText) findViewById(R.id.tv_screenName);
        mEtUserName = (EditText) findViewById(R.id.tv_user_name);
        mEtStatus = (EditText) findViewById(R.id.tv_status);
        mEtLocation = (EditText) findViewById(R.id.tv_location);
        mBtnChangePassword = (Button) findViewById(R.id.btn_change_password);
        mBtnPrivacy = (Button) findViewById(R.id.btn_privacy);
        mBtnAboutWekepler = (Button) findViewById(R.id.btn_about_wekepler);
        mBtnLogout = (Button) findViewById(R.id.btn_logout);

        ImageLoader.getInstance().displayImage(mUser.getProfilePictureUrl(), mIbEditProfilePicture);
        mEtScreenName.setText(mUser.getScreenName());
        mEtUserName.setText(mUser.getUserName());
        mEtStatus.setText(mUser.getDescription());
        mEtLocation.setText(mUser.getLocation());

        mIvBack.setOnClickListener(this);
        mBtnSave.setOnClickListener(this);
        mIbEditProfileCover.setOnClickListener(this);
        mIbEditProfilePicture.setOnClickListener(this);
        mEtScreenName.setOnClickListener(this);
        mEtUserName.setOnClickListener(this);
        mEtStatus.setOnClickListener(this);
        mEtLocation.setOnClickListener(this);
        mBtnChangePassword.setOnClickListener(this);
        mBtnPrivacy.setOnClickListener(this);
        mBtnAboutWekepler.setOnClickListener(this);
        mBtnLogout.setOnClickListener(this);
    }

    @Override
    public void onSuccess(String operationTag, Object... objects) {

    }

    @Override
    public void onError(String operationTag, String error, Object... objects) {
        toastLong(error);
    }

    @Override
    public void onClick(View v) {
        if (v == mIvBack) {
            onBackPressed();
        } else if (v == mBtnSave) {

        } else if (v == mIbEditProfileCover) {

        } else if (v == mIbEditProfilePicture) {

        } else if (v == mEtScreenName) {
            if (!mEtScreenName.isEnabled()) {
                mEtScreenName.setEnabled(true);
            } else {
                mEtScreenName.setEnabled(false);
            }

        } else if (v == mEtUserName) {
            if (!mEtUserName.isEnabled()) {
                mEtUserName.setEnabled(true);
            } else {
                mEtUserName.setEnabled(false);
            }

        } else if (v == mEtStatus) {
            if (!mEtStatus.isEnabled()) {
                mEtStatus.setEnabled(true);
            } else {
                mEtStatus.setEnabled(false);
            }

        } else if (v == mEtLocation) {
            if (!mEtLocation.isEnabled()) {
                mEtLocation.setEnabled(true);
            } else {
                mEtLocation.setEnabled(false);
            }

        } else if (v == mBtnChangePassword) {
            goToActivity(ChangePasswordActivity.class, false);
        } else if (v == mBtnPrivacy) {

        } else if (v == mBtnAboutWekepler) {
        } else if (v == mBtnLogout) {
            setResult(RESULT_FIRST_USER);
            finish();
        }
    }
}
