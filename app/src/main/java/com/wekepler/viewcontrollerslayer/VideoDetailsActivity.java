package com.wekepler.viewcontrollerslayer;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.wekepler.R;
import com.wekepler.SharedPreferencesUtil;
import com.wekepler.modelslayer.AddCommentToVideoModel;
import com.wekepler.modelslayer.BlockVideoModel;
import com.wekepler.modelslayer.DeleteVideoModel;
import com.wekepler.modelslayer.FavoriteVideoModel;
import com.wekepler.modelslayer.FollowUserModel;
import com.wekepler.modelslayer.GetCommentsModel;
import com.wekepler.modelslayer.LikeVideoModel;
import com.wekepler.modelslayer.ReportVideoModel;
import com.wekepler.viewcontrollerslayer.adapters.CommentsListAdapter;
import com.wekepler.viewcontrollerslayer.adapters.PostItemHolder;
import com.wekepler.viewcontrollerslayer.entities.Comment;
import com.wekepler.viewcontrollerslayer.entities.Friend;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.Post;
import com.wekepler.viewcontrollerslayer.entities.User;
import com.wekepler.viewcontrollerslayer.entities.requests.AddCommentRequest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class VideoDetailsActivity extends BaseActivity implements View.OnClickListener, OnModelResponseListener {


    //Post members
    private Post mPost;
    private String mUserName;
    private ImageView ivProfilePicture;
    private TextView tvTitle;
    private TextView tvUserName;
    private ImageButton ibFollow;
    private TextView tvPostSince;
    private ImageView ivVideoThumbnail;
    private ImageButton ibPlay;
    private TextView tvVideoCaption;
    private ImageButton ibOptions;
    private TextView tvViewersNumber;
    private LinearLayout llLike;
    private ImageButton ibLike;
    private TextView tvLikesNumber;
    //private LinearLayout llComment;
    private ImageButton ibComment;
    private TextView tvCommentsNumber;
    private LinearLayout llRepost;
    private ImageButton ibRepost;
    private TextView tvRepostsNumber;
    private LinearLayout llShare;
    private ImageButton ibShare;
    private DisplayImageOptions mUserProfileOptions = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(R.drawable.default_profile_picture)
            .showImageOnFail(R.drawable.default_profile_picture)
            .showImageOnLoading(R.drawable.default_profile_picture)
            .build();

    private DisplayImageOptions mVideoThumbnailsOptions = new DisplayImageOptions.Builder()
            .showImageForEmptyUri(R.drawable.video_background)
            .showImageOnFail(R.drawable.video_background)
            .showImageOnLoading(R.drawable.video_background)
            .build();

    //comment list members
    private String mCommentId;
    private ArrayList<Comment> mCommentsList;
    private ListView mListView;
    private CommentsListAdapter mAdapter;
    private ImageView mIvBack;
    private EditText mEtComment;
    private Button mBtnSend;
    private ProgressBar mPbLoading;
    private final int REQUEST_CODE_EDIT_COMMENT = 11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_details);
        initializeViews();
    }

    @Override
    public void initializeViews() {

        mUserName = SharedPreferencesUtil.getInstance(this).getToken().getUserName();

        mCommentId = getIntent().getStringExtra(Constants.COMMENT_ID_TAG);
        mIvBack = (ImageView) findViewById(R.id.iv_left_icon_action_bar);
        mListView = (ListView) findViewById(R.id.lv_comments);

        //initialize posts elements
        View header = getLayoutInflater().inflate(R.layout.post_cell, mListView, false);
        mListView.addHeaderView(header);

        ivProfilePicture = (ImageView) header.findViewById(R.id.iv_profile_picture);
        tvTitle = (TextView) header.findViewById(R.id.tv_screenName);
        tvUserName = (TextView) header.findViewById(R.id.tv_user_name);
        ibFollow = (ImageButton) header.findViewById(R.id.ib_follow);                     //this
        tvPostSince = (TextView) header.findViewById(R.id.tv_post_since);
        ivVideoThumbnail = (ImageView) header.findViewById(R.id.iv_video_thumbnail);
        ibPlay = (ImageButton) header.findViewById(R.id.ib_play);
        tvVideoCaption = (TextView) header.findViewById(R.id.tv_video_caption);
        ibOptions = (ImageButton) header.findViewById(R.id.ib_options);
        tvViewersNumber = (TextView) header.findViewById(R.id.tv_viewers_number);
        llLike = (LinearLayout) header.findViewById(R.id.ll_like);
        ibLike = (ImageButton) header.findViewById(R.id.ib_like);
        tvLikesNumber = (TextView) header.findViewById(R.id.tv_likes_number);
        //llComment = (LinearLayout) header.findViewById(R.id.ll_comment);
        ibComment = (ImageButton) header.findViewById(R.id.ib_comment);
        tvCommentsNumber = (TextView) header.findViewById(R.id.tv_comments_number);
        llRepost = (LinearLayout) header.findViewById(R.id.ll_repost);
        ibRepost = (ImageButton) header.findViewById(R.id.ib_repost);
        tvRepostsNumber = (TextView) header.findViewById(R.id.tv_reposts_number);
        llShare = (LinearLayout) header.findViewById(R.id.ll_share);
        ibShare = (ImageButton) header.findViewById(R.id.ib_share);

        if (getIntent().hasExtra(Constants.BUNDLE_TAG))
            setPostData();


        //initialize comments list members
        mEtComment = (EditText) findViewById(R.id.et_comment);
        mBtnSend = (Button) findViewById(R.id.btn_send);
        mPbLoading = (ProgressBar) findViewById(R.id.pb_loading);
        mIvBack.setOnClickListener(this);
        mBtnSend.setOnClickListener(this);
        getCommentsList();
        mAdapter = new CommentsListAdapter(this, mCommentsList, false, this);
        mListView.setAdapter(mAdapter);
    }

    private void setPostData() {

        mPost = (Post) getIntent().getBundleExtra(Constants.BUNDLE_TAG).getSerializable(Constants.POST_TAG);
        final User user = mPost.getUser();

        ImageLoader.getInstance().displayImage(user.getProfilePictureUrl(), ivProfilePicture, mUserProfileOptions);

        tvTitle.setText(user.getScreenName());
        tvUserName.setText(user.getUserName());

        if (!mUserName.equals(user.getUserName())) {

            ivProfilePicture.setOnClickListener(this);

            ibFollow.setVisibility(View.VISIBLE);
            if (user.isFollowed())
                ibFollow.setImageResource(R.drawable.follow_icon_hover);
            else
                ibFollow.setImageResource(R.drawable.follow_icon_normal);
            ibFollow.setOnClickListener(this);

            /*ibOptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popupMenu = new PopupMenu(VideoDetailsActivity.this, v);
                    popupMenu.getMenuInflater().inflate(R.menu.menu_other_users_video_settings, popupMenu.getMenu());
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            String menuItemTitle = item.getTitle().toString();
                            if (menuItemTitle.equalsIgnoreCase(getString(R.string.favorite))) {
                                new FavoriteVideoModel(VideoDetailsActivity.this, mPost, VideoDetailsActivity.this);
                                return true;
                            } else if (menuItemTitle.equalsIgnoreCase(getString(R.string.report))) {
                                new ReportVideoModel(VideoDetailsActivity.this, mPost, VideoDetailsActivity.this);
                                return true;
                            } else if (menuItemTitle.equalsIgnoreCase(getString(R.string.block))) {
                                new BlockVideoModel(VideoDetailsActivity.this, mPost, VideoDetailsActivity.this);
                                return true;
                            }
                            return false;
                        }
                    });
                    popupMenu.show();
                }
            });*/
        } else {
            ibFollow.setVisibility(View.INVISIBLE);
            /*ibOptions.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popupMenu = new PopupMenu(VideoDetailsActivity.this, v);
                    popupMenu.getMenuInflater().inflate(R.menu.menu_user_video_settings, popupMenu.getMenu());
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            String menuItemTitle = item.getTitle().toString();
                            if (menuItemTitle.equalsIgnoreCase(getString(R.string.favorite))) {
                                new FavoriteVideoModel(VideoDetailsActivity.this, mPost, VideoDetailsActivity.this);
                                return true;
                            } else if (menuItemTitle.equalsIgnoreCase(getString(R.string.delete))) {
                                new DeleteVideoModel(VideoDetailsActivity.this, mPost.getId(), VideoDetailsActivity.this);
                                return true;
                            }
                            return false;
                        }
                    });
                    popupMenu.show();
                }
            });*/
        }

        ibOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(VideoDetailsActivity.this, v);
                popupMenu.getMenuInflater().inflate(R.menu.menu_other_users_video_settings, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        String menuItemTitle = item.getTitle().toString();
                        if (menuItemTitle.equalsIgnoreCase(getString(R.string.favorite))) {
                            new FavoriteVideoModel(VideoDetailsActivity.this, mPost, VideoDetailsActivity.this);
                            return true;
                        } else if (menuItemTitle.equalsIgnoreCase(getString(R.string.report))) {
                            new ReportVideoModel(VideoDetailsActivity.this, mPost, VideoDetailsActivity.this);
                            return true;
                        } else if (menuItemTitle.equalsIgnoreCase(getString(R.string.block))) {
                            new BlockVideoModel(VideoDetailsActivity.this, mPost, VideoDetailsActivity.this);
                            return true;
                        } else if (menuItemTitle.equalsIgnoreCase(getString(R.string.delete))) {
                            new DeleteVideoModel(VideoDetailsActivity.this, mPost.getId(), VideoDetailsActivity.this);
                            return true;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });


        tvPostSince.setText(convertDateToText(mPost.getPostSince()));
        ImageLoader.getInstance().displayImage(mPost.getVideoThumbnailUrl(), ivVideoThumbnail, mVideoThumbnailsOptions);
        ibPlay.setOnClickListener(this);

        tvVideoCaption.setText(mPost.getVideoCaption());

        tvViewersNumber.setText("" + mPost.getViewersNumber());

        if (mPost.isLiked())
            ibLike.setImageResource(R.drawable.like_hover);
        else
            ibLike.setImageResource(R.drawable.like_normal);

        llLike.setOnClickListener(this);
        tvLikesNumber.setText("" + mPost.getLikesNumber());


        if (mPost.isCommented())
            ibComment.setImageResource(R.drawable.comments_hover);
        else
            ibComment.setImageResource(R.drawable.comments_normal);

        /*llComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.VIDEO_ID_TAG, mPost.getId());
                goToActivity(CommentsActivity.class, false, bundle);
            }
        });*/
        tvCommentsNumber.setText("" + mPost.getCommentsNumber());

        if (mPost.isReposted()) {
            ibRepost.setImageResource(R.drawable.repost_hover);
        } else {
            ibRepost.setImageResource(R.drawable.repost_normal);
        }

        llRepost.setOnClickListener(this);

        tvRepostsNumber.setText("" + mPost.getRepostsNumber());

        llShare.setOnClickListener(this);
    }

    private Friend getFriend(Post post) {
        Friend friend = new Friend(post.getUser());
        return friend;
    }


    private ArrayList<Comment> getCommentsList() {
        mCommentsList = new ArrayList<>();
        mPbLoading.setVisibility(View.VISIBLE);
        new GetCommentsModel(this, mPost.getId(), this);
        return mCommentsList;
    }

    @Override
    public void onClick(View v) {
        if (v == mIvBack) {
            finish();
        } else if (v == mBtnSend) {
            String comment = getTextFromEditText(mEtComment, true);
            if (comment.equals("")) {
                toastLong(R.string.no_comments_to_post);
            } else {
                AddCommentRequest addCommentRequest = new AddCommentRequest();
                addCommentRequest.setCommentText(comment);
                addCommentRequest.setVideoId(mPost.getId());
                addCommentRequest.setParentCommentId(1);
                new AddCommentToVideoModel(this, addCommentRequest, this);
            }
        } else if (v == ivProfilePicture) {
            Friend friend = getFriend(mPost);
            goToActivity(FriendProfileActivity.class, false, Constants.FRIEND_TAG, friend);
        } else if (v == ibFollow) {
            User user = mPost.getUser();
            new FollowUserModel(this, user, mPost, null, this);
            if (user.isFollowed()) {
                user.setFollowed(false);
                ibFollow.setImageResource(R.drawable.follow_icon_normal);
            } else {
                user.setFollowed(true);
                ibFollow.setImageResource(R.drawable.follow_icon_hover);
            }
        } else if (v == ibPlay) {
            Intent intent = new Intent(this, VideoViewerActivity.class);
            intent.putExtra(Constants.VIDEO_PATH_TAG, mPost.getVideoUrl());
            startActivity(intent);
        } else if (v == llLike) {
            new LikeVideoModel(this, mPost, null, this);
            if (mPost.isLiked()) {
                mPost.setIsLiked(false);
                ibLike.setImageResource(R.drawable.like_normal);
                mPost.setLikesNumber(mPost.getLikesNumber() - 1);
                tvLikesNumber.setText("" + mPost.getLikesNumber());

            } else {
                mPost.setIsLiked(true);
                ibLike.setImageResource(R.drawable.like_hover);
                mPost.setLikesNumber(mPost.getLikesNumber() + 1);
                tvLikesNumber.setText("" + mPost.getLikesNumber());
            }
        } else if (v == llRepost) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.POST_TAG, mPost);
            goToActivity(RepostVideoActivity.class, false, bundle);
        } else if (v == llShare) {
            displayShareDialog(mPost);
        }

    }

    private void displayShareDialog(Post post) {
        SharePostDialog dialog = new SharePostDialog(this, post);
        dialog.show();
    }


    @Override
    public void onSuccess(String operationTag, Object... objects) {
        mPbLoading.setVisibility(View.INVISIBLE);
        if (operationTag.equals(Constants.GET_COMMENTS_TAG)) {
            mCommentsList = (ArrayList<Comment>) objects[0];
            mAdapter.setItems(mCommentsList);
            mAdapter.notifyDataSetChanged();
        } else if (operationTag.equals(Constants.FOLLOW_USER_TAG)) {
            String status = mPost.getUser().isFollowed() ? "Followed" : "Un-Followed";
            toastLong(mPost.getUser().getUserName() + " is " + status);
        } else if (operationTag.equals(Constants.ADD_COMMENT_VIDEO_TAG)) {
            mEtComment.setText("");
            getCommentsList();
        } else if (operationTag.equals(Constants.DELETE_COMMENT_TAG)) {
            getCommentsList();
        } else if (operationTag.equals(Constants.FAVORITE_VIDEO_TAG)) {
            toastLong("Video added to favorites");
        } else if (operationTag.equals(Constants.REPORT_VIDEO_TAG)) {
            toastLong("Video is reported");
        } else if (operationTag.equals(Constants.BLOCK_VIDEO_TAG)) {
            toastLong("Video is blocked");
        } else if (operationTag.equals(Constants.DELETE_VIDEO_TAG)) {
            toastLong("Video is deleted");
            onBackPressed();
        } else if (operationTag.equals(Constants.LIKE_VIDEO_TAG)) {
            toastLong("video is liked");
        }
    }

    @Override
    public void onError(String operationTag, String error, Object... objects) {
        mPbLoading.setVisibility(View.INVISIBLE);
        toastLong(error);

        if (operationTag.equals(Constants.FOLLOW_USER_TAG)) {
            User user = mPost.getUser();
            if (user.isFollowed()) {
                user.setFollowed(false);
                ibFollow.setImageResource(R.drawable.follow_icon_normal);
            } else {
                user.setFollowed(true);
                ibFollow.setImageResource(R.drawable.follow_icon_hover);
            }
        } else if (operationTag.equals(Constants.LIKE_VIDEO_TAG)) {
            if (mPost.isLiked()) {
                mPost.setIsLiked(false);
                ibLike.setImageResource(R.drawable.like_normal);
                mPost.setLikesNumber(mPost.getLikesNumber() - 1);
                tvLikesNumber.setText("" + mPost.getLikesNumber());
            } else {
                mPost.setIsLiked(true);
                ibLike.setImageResource(R.drawable.like_hover);
                mPost.setLikesNumber(mPost.getLikesNumber() + 1);
                tvLikesNumber.setText("" + mPost.getLikesNumber());
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_EDIT_COMMENT) {
            if (resultCode == RESULT_OK) {
                getCommentsList();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private String convertDateToText(String dateString) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            Date uploadDate = simpleDateFormat.parse(dateString);

            long timeDifference = new Date().getTime() - uploadDate.getTime();
            long days = TimeUnit.MILLISECONDS.toDays(timeDifference);
            if (days >= 365) {
                return days / 365 + " y";
            } else if (days > 30) {
                return days / 31 + " mon";
            } else if (days > 0)
                return days + " d";
            else {
                long hours = TimeUnit.MILLISECONDS.toHours(timeDifference);
                if (hours >= 1)
                    return hours + " h";
                else {
                    long minutes = TimeUnit.MILLISECONDS.toMinutes(timeDifference);
                    if (minutes > 0)
                        return minutes + " min";
                    else {
                        long seconds = TimeUnit.MILLISECONDS.toSeconds(timeDifference);
                        return seconds + " sec";
                    }
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
