package com.wekepler.viewcontrollerslayer;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.wekepler.R;

public class VideoViewerActivity extends AppCompatActivity implements MediaPlayer.OnPreparedListener {

    private VideoView mVideoView;
    private MediaController mMediaController;
    private ProgressBar mPbLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_viewer);
        initializeViews();
    }

    private void initializeViews() {

        mVideoView = (VideoView) findViewById(R.id.vv_player);
        mPbLoading = (ProgressBar) findViewById(R.id.pb_loading);

        mMediaController = new MediaController(this);
        mMediaController.setAnchorView(mVideoView);
        mVideoView.setMediaController(mMediaController);

        String videoPath = getIntent().getStringExtra(Constants.VIDEO_PATH_TAG);
        Uri videoUri = Uri.parse(videoPath);

        mVideoView.setVideoURI(videoUri);
        mVideoView.setOnPreparedListener(this);
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mVideoView.start();
        mPbLoading.setVisibility(View.INVISIBLE);
    }
}
