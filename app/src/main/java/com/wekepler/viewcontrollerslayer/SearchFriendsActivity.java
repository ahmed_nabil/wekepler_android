package com.wekepler.viewcontrollerslayer;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.wekepler.R;
import com.wekepler.viewcontrollerslayer.adapters.SearchFriendsListAdapter;
import com.wekepler.viewcontrollerslayer.entities.Friend;

import java.util.ArrayList;

public class SearchFriendsActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack;
    private ListView mListView;
    private View mListHeader;
    private ArrayList<Friend> mFriendsList;
    private SearchFriendsListAdapter mAdapter;
    private EditText mEtSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_friends);
        initializeViews();
    }

    @Override
    public void initializeViews() {
        mIvBack = (ImageView) findViewById(R.id.iv_back_action_bar);
        mListView = (ListView) findViewById(R.id.lv_search);
        mEtSearch = (EditText) findViewById(R.id.et_search_action_bar);


        mIvBack.setOnClickListener(this);

        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAdapter.filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mListHeader = getLayoutInflater().inflate(R.layout.suggestion_header, mListView, false);
        mAdapter = new SearchFriendsListAdapter(this, getFriends());
        mListView.setAdapter(mAdapter);
        mListView.addHeaderView(mListHeader);
        mListView.setTextFilterEnabled(true);
    }

    private ArrayList<Friend> getFriends() {
        mFriendsList = new ArrayList<>();

        Friend friend = new Friend();
        friend.setScreenName("Creative Channel");
        friend.setUserName("esawy_rah");
        friend.setIsFollowed(false);
        friend.setProfilePictureUrl("drawable://" + R.drawable.profile_picture);
        mFriendsList.add(friend);

        friend = new Friend();
        friend.setScreenName("Photoshop");
        friend.setUserName("Smaoi");
        friend.setIsFollowed(false);
        friend.setProfilePictureUrl("drawable://" + R.drawable.man_profile_picture);
        mFriendsList.add(friend);

        friend = new Friend();
        friend.setScreenName("Beauty Cha");
        friend.setUserName("Tooota");
        friend.setIsFollowed(true);
        friend.setProfilePictureUrl("drawable://" + R.drawable.baby_profile_picture);
        mFriendsList.add(friend);

        friend = new Friend();
        friend.setScreenName("Sports");
        friend.setUserName("U_sma_12y");
        friend.setIsFollowed(true);
        friend.setProfilePictureUrl("drawable://" + R.drawable.football_profile_picture);
        mFriendsList.add(friend);

        return mFriendsList;
    }


    @Override
    public void onClick(View v) {
        if (v == mIvBack) {
            onBackPressed();
        }
    }

    @Override
    public void onSuccess(String operationTag, Object... objects) {

    }

    @Override
    public void onError(String operationTag, String error, Object... objects) {
        toastLong(error);
    }

}
