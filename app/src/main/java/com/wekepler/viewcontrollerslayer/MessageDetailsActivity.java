package com.wekepler.viewcontrollerslayer;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.wekepler.R;
import com.wekepler.modelslayer.GetSentMessagesModel;
import com.wekepler.modelslayer.SendMessageModel;
import com.wekepler.viewcontrollerslayer.adapters.MessageDetailsListAdapter;
import com.wekepler.viewcontrollerslayer.entities.Message;
import com.wekepler.viewcontrollerslayer.entities.requests.SendMessageRequest;

import java.util.ArrayList;
import java.util.Arrays;

public class MessageDetailsActivity extends BaseActivity implements View.OnClickListener {

    private ImageView mIvBack;
    private ImageView mIvSettings;
    private TextView mTvActionBarTitle;
    private EditText mEtMessage;
    private Button mBtnSend;

    //private MessageDetails mMessageDetails;
    private MessageDetailsListAdapter mAdapter;
    private ListView mListView;
    private ArrayList<Integer> mReceiversIds;
    private ArrayList<String> mReceiversScreenNames;
    private ArrayList<Message> mMessagesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_details);
        initializeViews();
    }

    @Override
    public void initializeViews() {
        mIvBack = (ImageView) findViewById(R.id.iv_back_action_bar);
        mIvSettings = (ImageView) findViewById(R.id.iv_settings_action_bar);
        mTvActionBarTitle = (TextView) findViewById(R.id.tv_center_title);
        mListView = (ListView) findViewById(R.id.lv_messages);
        mEtMessage = (EditText) findViewById(R.id.et_message);
        mBtnSend = (Button) findViewById(R.id.btn_send);

        mIvBack.setOnClickListener(this);
        mBtnSend.setOnClickListener(this);

        if (getIntent() != null && getIntent().hasExtra(Constants.BUNDLE_TAG)) {
            mReceiversIds = getIntent().getBundleExtra(Constants.BUNDLE_TAG).getIntegerArrayList(Constants.RECEIVER_ID_TAG);
            mReceiversScreenNames = getIntent().getBundleExtra(Constants.BUNDLE_TAG).getStringArrayList(Constants.RECEIVER_SCREEN_NAME_TAG);
            getMessageDetails();
        }
        if (mReceiversScreenNames != null && !mReceiversScreenNames.isEmpty()) {
            mTvActionBarTitle.setText(mReceiversScreenNames.get(0));
        }
        mMessagesList = new ArrayList<>();
        mAdapter = new MessageDetailsListAdapter(this, mMessagesList);
        mListView.setAdapter(mAdapter);
    }

    public void getMessageDetails() {
        new GetSentMessagesModel(this, this);
    }

    @Override
    public void onClick(View v) {
        if (v == mIvBack) {
            onBackPressed();
        } else if (v == mBtnSend) {
            String message = getTextFromEditText(mEtMessage, true);
            if (message.equals("")) {
                toastLong("Please insert a message");
                return;
            }

            if (mReceiversIds != null && !mReceiversIds.isEmpty()) {
                SendMessageRequest sendMessageRequest = new SendMessageRequest();
                sendMessageRequest.setReceiverId("" + mReceiversIds.get(0));
                sendMessageRequest.setMessageText(message);
                new SendMessageModel(this, sendMessageRequest, this);
            }
        }
    }

    @Override
    public void onSuccess(String operationTag, Object... objects) {
        if (operationTag.equals(Constants.GET_SENT_MESSAGES_TAG)) {
            mMessagesList = (ArrayList<Message>) objects[0];
            mAdapter.setItems(mMessagesList);
            toastLong("successfully_retrieved");
        } else if (operationTag.equals(Constants.SEND_MESSAGE_TAG)) {
            toastLong("Successfully_sent");
            Message message = (Message) objects[0];
            mAdapter.addItem(message);

        }
    }

    @Override
    public void onError(String operationTag, String error, Object... objects) {
        toastLong(error);
    }

}
