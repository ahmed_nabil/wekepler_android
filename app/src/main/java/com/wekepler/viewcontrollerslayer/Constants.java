package com.wekepler.viewcontrollerslayer;

/**
 * Created by Mohammad Sayed on 8/24/2015.
 */
public class Constants {
    public static final String COMMENT_ID_TAG = "comment_id";
    public static final String FRIEND_TAG = "friend";
    public static final String RECEIVER_ID_TAG = "friend_id";
    public static final String RECEIVER_SCREEN_NAME_TAG = "friend_screen_name";
    public static final String POST_TAG = "post";
    public static final String VIDEO_PATH_TAG = "video_path";
    public static final int VIDEO_CAPTURE_REQUEST_CODE = 5;
    public static final int VIDEO_PICK_REQUEST_CODE = 6;
    public static final int EDIT_SETTINGS_REQUEST_CODE = 7;
    public static final int VIEW_VIDEO_DETAILS_REQUEST_CODE = 8;
    public static final String GRANT_TYPE_PASSOWRD = "password";
    public static final String LOGIN_TAG = "login";
    public static final String REGISTRATION_TAG = "login";
    public static final String FEEDS_OF_CURRENT_USER_TAG = "feeds_of_current_user";
    public static final String VIDEOS_OF_CURRENT_USER_TAG = "videos_of_current_user";
    public static final String VIDEOS_OF_FRIEND_TAG = "videos_of_friend";
    public static final String LIKE_VIDEO_TAG = "like_video";
    public static final String BLOCK_VIDEO_TAG = "block_video";
    public static final String REPORT_VIDEO_TAG = "report_video";
    public static final String FAVORITE_VIDEO_TAG = "favorite_video";
    public static final String ADD_COMMENT_VIDEO_TAG = "comment_video";
    public static final String EDIT_COMMENT_TAG = "edit_comment";
    public static final String DELETE_COMMENT_TAG = "delete_comment";
    public static final String SEND_MESSAGE_TAG = "send_message";
    public static final String VIDEO_ID_TAG = "video_id";
    public static final String GET_COMMENTS_TAG = "get_comments";
    public static final String GET_SENT_MESSAGES_TAG = "get_sent_messages";
    public static final String GET_USER_PROFILE_TAG = "get_user_profile";
    public static final String GET_CURRENT_USER_PROFILE_TAG = "get_current_user_profile";
    public static final String UPLOAD_VIDEO_TAG = "upload_video";
    public static final String UPLOAD_VIDEO_THUMBNAIL_TAG = "upload_video_thumbnail";
    public static final String CHANGE_PASSWORD_TAG = "upload_video";
    public static final String DELETE_VIDEO_TAG = "delete_video";
    public static final String REPOST_VIDEO_TAG = "repost_video";
    public static final String FOLLOW_USER_TAG = "follow_user";

    public static final String FOLLOW = "Follow";
    public static final String UNFOLLOW = "UnFollow";


    public static final short SERVICE_TYPE_FEED = 0;
    public static final short SERVICE_TYPE_VIDEO = 1;
    public static final String USER_INFO_TAG = "user_info";
    public static final String REGISTER_EXTERNAL_TAG = "register_external";
    public static final String TOKEN_TAG = "token";
    public static final String TOKEN_STRING_TAG = "token_string";
    public static final String BUNDLE_TAG = "bundle";
    public static final String COMMENTS_TAG = "comments";
    public static final String COMMENT_TAG = "comment";
    public static final String URI_TAG = "uri";
    public static final String DATA_RETRIEVED_TAG = "data_retrieved";
    public static final String DATA_STORED_TAG = "data_stored";
    public static final String LOGIN_TYPE_TAG = "login_type";
    public static final int LOGIN_TYPE_ACCOUNT_CODE = 0;
    public static final int LOGIN_TYPE_FACEBOOK_CODE = 1;
    public static final int LOGIN_TYPE_TWITTER_CODE = 2;
    public static final int LOGIN_TYPE_GOOGLE_PLUS_CODE = 3;

    public static final int BODY_CONTENT_JSON_CODE = 0;
    public static final int BODY_CONTENT_URL_ENCODED_CODE = 1;
    public static final String USER_TAG = "user";


}
