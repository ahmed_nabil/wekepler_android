package com.wekepler.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.wekepler.R;

/**
 * Created by Mohammad Sayed on 8/22/2015.
 */
public class AspectRatioImageView extends ImageView {

    private float mAspectRatio;

    public AspectRatioImageView(Context context){
        this(context, null);
    }

    public AspectRatioImageView(Context context, AttributeSet attrs){
        this(context, attrs, 0);
    }

    public AspectRatioImageView(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AspectRatioImageView);
        mAspectRatio = a.getFloat(R.styleable.AspectRatioImageView_aspect_ratio, -1);
        a.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        //super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        /*if(mAspectRatio > 0){
            int width = getMeasuredWidth();
            int height = (int) (width * mAspectRatio);
            setMeasuredDimension(width, height);
        }*/

        /*Drawable drawable = getDrawable();
        if(drawable == null){
            setMeasuredDimension(0,0);
        }else{
            float imageSideRatio = (float) drawable.getIntrinsicWidth() / (float) drawable.getIntrinsicHeight();
            float viewSideRatio = (float) MeasureSpec.getSize(widthMeasureSpec) / (float) MeasureSpec.getSize(heightMeasureSpec);
            if(imageSideRatio >= viewSideRatio){
                int width = MeasureSpec.getSize(widthMeasureSpec);
                int height = (int) (width/imageSideRatio);
                setMeasuredDimension(width,height);
            }
            else{
                int height = MeasureSpec.getSize(heightMeasureSpec);
                int width = (int) (height * imageSideRatio);
                setMeasuredDimension(width,height);
            }
        }*/
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = width * getDrawable().getIntrinsicHeight() / getDrawable().getIntrinsicWidth();
        setMeasuredDimension(width, height);


    }
}
