package com.wekepler;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.decode.ImageDecoder;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.wekepler.widgets.SmartUriDecoder;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Mohammad Sayed on 8/22/2015.
 */
public class WekeplerApplication extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "55iEu4fsGx1FuRZWcmfQgVBqu";
    private static final String TWITTER_SECRET = "w3EGl8bfORUqZG2EWAhVngTG3XkwsbUU4ghCbXwstSjyIY5CFC";


    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));

        ImageDecoder smartUriDecoder = new SmartUriDecoder(getContentResolver(), new BaseImageDecoder(false));

        // UNIVERSAL IMAGE LOADER SETUP
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .displayer(new RoundedBitmapDisplayer(35))
                .showImageForEmptyUri(R.drawable.default_profile_picture)
                .showImageOnFail(R.drawable.default_profile_picture)
                .showImageOnLoading(R.drawable.default_profile_picture)
                .build();


        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                        //.discCacheSize(100 * 1024 * 1024)
                .imageDecoder(smartUriDecoder)
                .build();

        ImageLoader.getInstance().init(config);
        // END - UNIVERSAL IMAGE LOADER SETUP


        //Facebook Initialization
        FacebookSdk.sdkInitialize(this);

    }
}
