package com.wekepler.serviceslayer.apis;

import android.content.Context;

import com.wekepler.R;
import com.wekepler.SharedPreferencesUtil;
import com.wekepler.modelslayer.OnServiceResponseListener;
import com.wekepler.serviceslayer.NetworkManager;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.Friend;

import java.util.Map;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public class GetUserProfileApiService extends BaseApiService {

    private Context mContext;

    public GetUserProfileApiService(Context context, int userId, OnServiceResponseListener onServiceResponseListener) {
        super(context, context.getString(R.string.get_user_profile_uri), onServiceResponseListener);
        setUrl(String.format(getUrl(), "" + userId));
        this.mContext = context;
    }

    @Override
    public void connect(Object... objects) {
        NetworkManager networkManager = NetworkManager.getInstance(mContext.getApplicationContext());
        int loginType = SharedPreferencesUtil.getInstance(mContext)
                .getInt(Constants.LOGIN_TYPE_TAG, Constants.LOGIN_TYPE_ACCOUNT_CODE);
        Map<String, String> headers = getBaseHeaders(loginType);
        if (headers != null) {
            networkManager.sendGetRequest(this, headers, null, Friend.class, this, this);
        }
    }

    @Override
    public void onResponse(Object response) {
        mOnServiceResponseListener.onSuccess(response);
    }
}
