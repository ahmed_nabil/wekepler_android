package com.wekepler.serviceslayer.apis;

import android.content.Context;

import com.wekepler.R;
import com.wekepler.SharedPreferencesUtil;
import com.wekepler.modelslayer.OnServiceResponseListener;
import com.wekepler.serviceslayer.NetworkManager;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.Message;
import com.wekepler.viewcontrollerslayer.entities.requests.AddCommentRequest;
import com.wekepler.viewcontrollerslayer.entities.requests.SendMessageRequest;

import java.util.Map;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public class SendMessageApiService extends BaseApiService {

    private Context mContext;

    public SendMessageApiService(Context context, OnServiceResponseListener onServiceResponseListener) {
        super(context, context.getString(R.string.send_message_uri), onServiceResponseListener);
        this.mContext = context;
    }

    @Override
    public void connect(Object... objects) {
        SendMessageRequest sendMessageRequest = (SendMessageRequest) objects[0];
        NetworkManager networkManager = NetworkManager.getInstance(mContext.getApplicationContext());
        int loginType = SharedPreferencesUtil.getInstance(mContext)
                .getInt(Constants.LOGIN_TYPE_TAG, Constants.LOGIN_TYPE_ACCOUNT_CODE);
        Map<String, String> headers = getBaseHeaders(loginType);
        if (headers != null) {
            networkManager.sendPostRequest(this, headers, Constants.BODY_CONTENT_JSON_CODE, mGson.toJson(sendMessageRequest), Message.class, this, this);
        }
    }

    @Override
    public void onResponse(Object response) {
        mOnServiceResponseListener.onSuccess(response);
    }
}
