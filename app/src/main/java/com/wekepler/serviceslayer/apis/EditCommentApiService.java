package com.wekepler.serviceslayer.apis;

import android.content.Context;

import com.wekepler.R;
import com.wekepler.SharedPreferencesUtil;
import com.wekepler.modelslayer.OnServiceResponseListener;
import com.wekepler.serviceslayer.NetworkManager;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.Comment;
import com.wekepler.viewcontrollerslayer.entities.parameters.EditCommentParameters;
import com.wekepler.viewcontrollerslayer.entities.requests.AddCommentRequest;
import com.wekepler.viewcontrollerslayer.entities.requests.LoginRequest;

import java.util.Map;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public class EditCommentApiService extends BaseApiService {

    private Context mContext;

    public EditCommentApiService(Context context, int commentId, OnServiceResponseListener onServiceResponseListener) {
        super(context, context.getString(R.string.edit_comment_uri), onServiceResponseListener);
        setUrl(String.format(getUrl(), "" + commentId));
        this.mContext = context;
    }

    @Override
    public void connect(Object... objects) {
        EditCommentParameters editCommentParameters = (EditCommentParameters) objects[0];
        NetworkManager networkManager = NetworkManager.getInstance(mContext.getApplicationContext());
        int loginType = SharedPreferencesUtil.getInstance(mContext)
                .getInt(Constants.LOGIN_TYPE_TAG, Constants.LOGIN_TYPE_ACCOUNT_CODE);
        Map<String, String> headers = getBaseHeaders(loginType);
        if (headers != null) {
            networkManager.sendPutRequest(this, headers, Constants.BODY_CONTENT_URL_ENCODED_CODE, editCommentParameters.getParametersMap(), null, this, this);
        }
    }

    @Override
    public void onResponse(Object response) {
        mOnServiceResponseListener.onSuccess(response);
    }
}
