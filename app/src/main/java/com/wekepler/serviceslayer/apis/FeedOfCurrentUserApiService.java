package com.wekepler.serviceslayer.apis;

import android.content.Context;

import com.wekepler.R;
import com.wekepler.SharedPreferencesUtil;
import com.wekepler.modelslayer.OnServiceResponseListener;
import com.wekepler.serviceslayer.NetworkManager;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.Post;
import com.wekepler.viewcontrollerslayer.entities.parameters.VideosParameters;

import java.util.Map;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public class FeedOfCurrentUserApiService extends BaseApiService {

    private Context mContext;

    public FeedOfCurrentUserApiService(Context context, OnServiceResponseListener onServiceResponseListener) {
        super(context, context.getString(R.string.feed_of_current_user_uri), onServiceResponseListener);
        this.mContext = context;
    }

    @Override
    public void connect(Object... objects) {
        VideosParameters videosParameters = (VideosParameters) objects[0];
        NetworkManager networkManager = NetworkManager.getInstance(mContext.getApplicationContext());
        int loginType = SharedPreferencesUtil.getInstance(mContext)
                .getInt(Constants.LOGIN_TYPE_TAG, Constants.LOGIN_TYPE_ACCOUNT_CODE);
        Map<String, String> headers = getBaseHeaders(loginType);
        if (headers != null) {
            networkManager.sendGetRequest(this, headers, videosParameters.getParametersMap(),
                    Post[].class, this, this);
        }
    }

    @Override
    public void onResponse(Object response) {
        mOnServiceResponseListener.onSuccess(response);
    }
}
