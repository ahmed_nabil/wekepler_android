package com.wekepler.serviceslayer.apis;

import android.content.Context;

import com.wekepler.R;
import com.wekepler.modelslayer.OnServiceResponseListener;
import com.wekepler.serviceslayer.NetworkManager;
import com.wekepler.SharedPreferencesUtil;
import com.wekepler.serviceslayer.entities.responses.Token;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.requests.LoginRequest;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public class LoginApiService extends BaseApiService {

    private Context mContext;
    private Token mToken;

    public LoginApiService(Context context, OnServiceResponseListener onServiceResponseListener) {
        super(context, context.getString(R.string.login_uri), onServiceResponseListener);
        this.mContext = context;
    }

    @Override
    public void connect(Object... objects) {
        LoginRequest loginRequest = (LoginRequest) objects[0];
        //sendUser using network manager
        NetworkManager networkManager = NetworkManager.getInstance(mContext.getApplicationContext());
        //networkManager.sendPostRequest(this, null, mGson.toJson(loginRequest), null, this, this);
        networkManager.sendPostRequest(this, null, Constants.BODY_CONTENT_URL_ENCODED_CODE, requestToUrlEncoded(loginRequest), Token.class, this, this);
    }

    private String requestToUrlEncoded(LoginRequest loginRequest) {
        StringBuilder stringBuilder = new StringBuilder("");
        stringBuilder.append("username=" + loginRequest.getUserName());
        stringBuilder.append("&password=" + loginRequest.getPassword());
        stringBuilder.append("&grant_type=" + loginRequest.getGrantType());
        return stringBuilder.toString();
    }

    public void saveToken() {
        SharedPreferencesUtil.getInstance(mContext).putToken(mToken);
        mOnServiceResponseListener.onSuccess(Constants.DATA_STORED_TAG);
    }

    @Override
    public void onResponse(Object response) {
        mToken = (Token) response;
        mOnServiceResponseListener.onSuccess(Constants.DATA_RETRIEVED_TAG);
    }
}
