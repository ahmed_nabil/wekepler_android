package com.wekepler.serviceslayer.apis;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.wekepler.R;
import com.wekepler.modelslayer.OnServiceResponseListener;
import com.wekepler.SharedPreferencesUtil;
import com.wekepler.serviceslayer.entities.responses.Token;
import com.wekepler.viewcontrollerslayer.Constants;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public abstract class BaseApiService implements Response.Listener, Response.ErrorListener {

    private String mUrl;
    protected Gson mGson;
    protected OnServiceResponseListener mOnServiceResponseListener;
    private Context mContext;

    public BaseApiService(Context context, String uri, OnServiceResponseListener onServiceResponseListener) {
        this.mContext = context;
        mUrl = context.getString(R.string.server_url) + uri;
        mGson = new Gson();
        this.mOnServiceResponseListener = onServiceResponseListener;
    }

    public BaseApiService(String url) {
        mUrl = url;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        this.mUrl = url;
    }

    public void setUri(String uri) {
        mUrl = mContext.getString(R.string.server_url) + uri;
    }

    public abstract void connect(Object... objects);

    @Override
    public void onErrorResponse(VolleyError error) {
        if (error != null && error.networkResponse != null && error.networkResponse.data != null)
            mOnServiceResponseListener.onError(error.networkResponse.statusCode, new String(error.networkResponse.data));
        else
            mOnServiceResponseListener.onError(1, mContext.getString(R.string.connection_error));
    }

    protected Map<String, String> getBaseHeaders(int loginType) {
        Map<String, String> map = new HashMap<>();
        String tokenString = null;
        if (loginType == Constants.LOGIN_TYPE_ACCOUNT_CODE) {
            Token token = SharedPreferencesUtil.getInstance(mContext).getToken();
            tokenString = token.getAccessToken();
            map.put(mContext.getString(R.string.authorization), token.getTokenType() + " " + token.getAccessToken());
        } else {
            tokenString = SharedPreferencesUtil.getInstance(mContext).getString(Constants.TOKEN_STRING_TAG, null);
            if (tokenString == null)
                mOnServiceResponseListener.onError(1, mContext.getString(R.string.invalid_token));
            else
                map.put(mContext.getString(R.string.authorization), "Bearer " + tokenString);
        }

        return map;
    }

}
