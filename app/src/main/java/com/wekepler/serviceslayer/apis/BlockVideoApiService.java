package com.wekepler.serviceslayer.apis;

import android.content.Context;

import com.wekepler.R;
import com.wekepler.SharedPreferencesUtil;
import com.wekepler.modelslayer.OnServiceResponseListener;
import com.wekepler.serviceslayer.NetworkManager;
import com.wekepler.viewcontrollerslayer.Constants;

import java.util.Map;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public class BlockVideoApiService extends BaseApiService {

    private Context mContext;

    public BlockVideoApiService(Context context, int videoId, OnServiceResponseListener onServiceResponseListener) {
        super(context, context.getString(R.string.block_video_uri), onServiceResponseListener);
        setUrl(String.format(getUrl(), "" + videoId));
        this.mContext = context;
    }

    @Override
    public void connect(Object... objects) {
        NetworkManager networkManager = NetworkManager.getInstance(mContext.getApplicationContext());
        int loginType = SharedPreferencesUtil.getInstance(mContext)
                .getInt(Constants.LOGIN_TYPE_TAG, Constants.LOGIN_TYPE_ACCOUNT_CODE);
        Map<String, String> headers = getBaseHeaders(loginType);
        if (headers != null) {
            networkManager.sendPostRequest(this, headers, Constants.BODY_CONTENT_JSON_CODE, null, null, this, this);
        }
    }

    @Override
    public void onResponse(Object response) {
        mOnServiceResponseListener.onSuccess(response);
    }
}
