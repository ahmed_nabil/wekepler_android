package com.wekepler.serviceslayer.apis;

import android.content.Context;

import com.wekepler.R;
import com.wekepler.modelslayer.OnServiceResponseListener;
import com.wekepler.serviceslayer.NetworkManager;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.requests.LoginRequest;
import com.wekepler.viewcontrollerslayer.entities.requests.RegistrationRequest;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public class RegistrationApiService extends BaseApiService {

    private Context mContext;

    public RegistrationApiService(Context context, OnServiceResponseListener onServiceResponseListener) {
        super(context, context.getString(R.string.register_uri), onServiceResponseListener);
        this.mContext = context;
    }

    @Override
    public void connect(Object... objects) {
        RegistrationRequest registrationRequest = (RegistrationRequest) objects[0];
        NetworkManager networkManager = NetworkManager.getInstance(mContext.getApplicationContext());
        networkManager.sendPostRequest(this, null, Constants.BODY_CONTENT_JSON_CODE, mGson.toJson(registrationRequest), null, this, this);
    }

    @Override
    public void onResponse(Object response) {
        mOnServiceResponseListener.onSuccess();
    }
}
