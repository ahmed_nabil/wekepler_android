package com.wekepler.serviceslayer.apis;

import android.content.Context;

import com.wekepler.R;
import com.wekepler.SharedPreferencesUtil;
import com.wekepler.modelslayer.OnServiceResponseListener;
import com.wekepler.serviceslayer.NetworkManager;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.parameters.RepostVideoParameters;
import com.wekepler.viewcontrollerslayer.entities.requests.ChangePasswordRequest;
import com.wekepler.viewcontrollerslayer.entities.requests.RepostVideoRequest;

import java.util.Map;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public class RepostVideoApiService extends BaseApiService {

    private Context mContext;
    private RepostVideoRequest mRepostVideoRequest;

    public RepostVideoApiService(Context context, RepostVideoRequest repostVideoRequest, OnServiceResponseListener onServiceResponseListener) {
        super(context, context.getString(R.string.repost_uri), onServiceResponseListener);
        this.mContext = context;
        this.mRepostVideoRequest = repostVideoRequest;
    }

    @Override
    public void connect(Object... objects) {
        NetworkManager networkManager = NetworkManager.getInstance(mContext.getApplicationContext());
        int loginType = SharedPreferencesUtil.getInstance(mContext)
                .getInt(Constants.LOGIN_TYPE_TAG, Constants.LOGIN_TYPE_ACCOUNT_CODE);
        Map<String, String> headers = getBaseHeaders(loginType);
        if (headers != null) {
            networkManager.sendPostRequest(this, headers, Constants.BODY_CONTENT_JSON_CODE, mGson.toJson(mRepostVideoRequest), null, this, this);
        }
    }

    @Override
    public void onResponse(Object response) {
        mOnServiceResponseListener.onSuccess(response);
    }
}
