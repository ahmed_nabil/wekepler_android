package com.wekepler.serviceslayer.apis;

import android.content.Context;

import com.wekepler.R;
import com.wekepler.SharedPreferencesUtil;
import com.wekepler.modelslayer.OnServiceResponseListener;
import com.wekepler.serviceslayer.NetworkManager;
import com.wekepler.serviceslayer.entities.responses.Token;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.requests.ChangePasswordRequest;
import com.wekepler.viewcontrollerslayer.entities.requests.LoginRequest;

import java.util.Map;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public class ChangePasswordApiService extends BaseApiService {

    private Context mContext;

    public ChangePasswordApiService(Context context, OnServiceResponseListener onServiceResponseListener) {
        super(context, context.getString(R.string.change_password_uri), onServiceResponseListener);
        this.mContext = context;
    }

    @Override
    public void connect(Object... objects) {
        ChangePasswordRequest changePasswordRequest = (ChangePasswordRequest) objects[0];
        //sendUser using network manager
        int loginType = SharedPreferencesUtil.getInstance(mContext)
                .getInt(Constants.LOGIN_TYPE_TAG, Constants.LOGIN_TYPE_ACCOUNT_CODE);
        Map<String, String> headers = getBaseHeaders(loginType);

        NetworkManager networkManager = NetworkManager.getInstance(mContext.getApplicationContext());
        if (headers != null) {
            networkManager.sendPostRequest(this, headers, Constants.BODY_CONTENT_JSON_CODE, mGson.toJson(changePasswordRequest), null, this, this);
        }
    }

    @Override
    public void onResponse(Object response) {
        mOnServiceResponseListener.onSuccess(Constants.CHANGE_PASSWORD_TAG);
    }
}
