package com.wekepler.serviceslayer.apis;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.ProgressBar;

import com.alexbbb.uploadservice.AbstractUploadServiceReceiver;
import com.alexbbb.uploadservice.UploadRequest;
import com.alexbbb.uploadservice.UploadService;
import com.wekepler.R;
import com.wekepler.SharedPreferencesUtil;
import com.wekepler.modelslayer.OnServiceResponseListener;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.PostVideoActivity;
import com.wekepler.viewcontrollerslayer.entities.Post;
import com.wekepler.viewcontrollerslayer.entities.parameters.UploadVideoParameters;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public class UploadVideoApiService extends BaseApiService {

    private Context mContext;
    private ProgressBar mProgressBar;

    public UploadVideoApiService(Context context, ProgressBar progressBar, OnServiceResponseListener onServiceResponseListener) {
        super(context, context.getString(R.string.upload_video_uri), onServiceResponseListener);
        this.mContext = context;
        this.mProgressBar = progressBar;
    }

    @Override
    public void connect(Object... objects) {
        UploadVideoParameters videosParameters = (UploadVideoParameters) objects[0];
        File videoFile = (File) objects[1];
        StringBuilder url = new StringBuilder("");
        url.append("?");
        Map<String, String> parameters = videosParameters.getParametersMap();
        for (Map.Entry entry : parameters.entrySet()) {
            url.append(entry.getKey() + "=" + entry.getValue() + "&");
        }
        setUrl(getUrl() + url.toString().replace(" ", "%20"));
        int loginType = SharedPreferencesUtil.getInstance(mContext)
                .getInt(Constants.LOGIN_TYPE_TAG, Constants.LOGIN_TYPE_ACCOUNT_CODE);
        Map<String, String> headers = getBaseHeaders(loginType);
        upload(getUrl(), videoFile, videosParameters, headers.get(mContext.getString(R.string.authorization)));
    }


    @Override
    public void onResponse(Object response) {
        mOnServiceResponseListener.onSuccess();
    }


    public void upload(String url, File videoFile, UploadVideoParameters uploadVideoParameters, String token) {
        final UploadRequest request = new UploadRequest(mContext,
                "custom-upload-id", url);

    /*
     * parameter-name: is the name of the parameter that will contain file's data.
     * Pass "uploaded_file" if you're using the test PHP script
     *
     * custom-file-name.extension: is the file name seen by the server.
     * E.g. value of $_FILES["uploaded_file"]["name"] of the test PHP script
     */
        request.addFileToUpload(videoFile.getAbsolutePath(),
                "uploaded_file",
                "custom-file-name.extension",
                "content-type");

        //You can add your own custom headers
        request.addHeader(mContext.getString(R.string.authorization), token);
        //and parameters
        /*request.addHeader(uploadVideoParameters.COMMENT_TEXT, uploadVideoParameters.getCommentText());
        request.addHeader(uploadVideoParameters.LATITUDE, String.valueOf(uploadVideoParameters.getLatitude()));
        request.addHeader(uploadVideoParameters.LONGITUDE, String.valueOf(uploadVideoParameters.getLongitude()));*/

        //If you want to add a parameter with multiple values, you can do the following:
        /*request.addParameter("array-parameter-name", "value1");
        request.addParameter("array-parameter-name", "value2");
        request.addParameter("array-parameter-name", "valueN");*/

        //or
        /*String[] values = new String[]{"value1", "value2", "valueN"};
        request.addArrayParameter("array-parameter-name", values);*/

        //or
        /*List<String> valuesList = new ArrayList<String>();
        valuesList.add("value1");
        valuesList.add("value2");
        valuesList.add("valueN");
        request.addArrayParameter("array-parameter-name", valuesList);*/

        //configure the notification
        request.setNotificationConfig(android.R.drawable.ic_menu_upload,
                "upload video: " + uploadVideoParameters.getTitle(),
                "upload in progress text",
                "upload completed successfully text",
                "upload error text",
                false);

        // set a custom user agent string for the upload request
        // if you comment the following line, the system default user-agent will be used
        /*request.setCustomUserAgent("UploadServiceDemo/1.0");*/

        // set the intent to perform when the user taps on the upload notification.
        // currently tested only with intents that launches an activity
        // if you comment this line, no action will be performed when the user taps on the notification
        request.setNotificationClickIntent(new Intent(mContext, PostVideoActivity.class));

        // set the maximum number of automatic upload retries on error
        request.setMaxRetries(2);

        try {
            //Start upload service and display the notification
            UploadService.startUpload(request);

        } catch (Exception exc) {
            //You will end up here only if you pass an incomplete UploadRequest
            Log.e("AndroidUploadService", exc.getLocalizedMessage(), exc);
        }
    }

    private static final String TAG = "AndroidUploadService";
    private final AbstractUploadServiceReceiver mUploadReceiver =
            new AbstractUploadServiceReceiver() {

                // you can override this progress method if you want to get
                // the completion progress in percent (0 to 100)
                // or if you need to know exactly how many bytes have been transferred
                // override the method below this one
                @Override
                public void onProgress(String uploadId, int progress) {
                    Log.i(TAG, "The progress of the upload with ID "
                            + uploadId + " is: " + progress);
                    mProgressBar.setProgress(progress);
                }

                @Override
                public void onProgress(final String uploadId, final long uploadedBytes, final long totalBytes) {
                    Log.i(TAG, "Upload with ID "
                            + uploadId + " uploaded bytes: " + uploadedBytes + ", total: " + totalBytes);
                }

                @Override
                public void onError(String uploadId, Exception exception) {
                    Log.e(TAG, "Error in upload with ID: " + uploadId + ". "
                            + exception.getLocalizedMessage(), exception);
                }

                @Override
                public void onCompleted(String uploadId,
                                        int serverResponseCode,
                                        String serverResponseMessage) {
                    Log.i(TAG, "Upload with ID " + uploadId
                            + " has been completed with HTTP " + serverResponseCode
                            + ". Response from server: " + serverResponseMessage);

                    if (serverResponseCode == 200 || serverResponseCode == 201) {
                        mOnServiceResponseListener.onSuccess(mGson.fromJson(serverResponseMessage, Post.class));
                    } else {
                        mOnServiceResponseListener.onError(serverResponseCode, serverResponseMessage);
                    }
                    //If your server responds with a JSON, you can parse it
                    //from serverResponseMessage string using a library
                    //such as org.json (embedded in Android) or google's gson
                }
            };

    public void registerUploadServiceReceiver() {
        mUploadReceiver.register(mContext);
    }

    public void unregisterUploadServiceReceiver() {
        mUploadReceiver.unregister(mContext);
    }

}
