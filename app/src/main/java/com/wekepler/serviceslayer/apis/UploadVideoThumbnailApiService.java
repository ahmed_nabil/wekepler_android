package com.wekepler.serviceslayer.apis;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.ProgressBar;

import com.alexbbb.uploadservice.AbstractUploadServiceReceiver;
import com.alexbbb.uploadservice.UploadRequest;
import com.alexbbb.uploadservice.UploadService;
import com.wekepler.R;
import com.wekepler.SharedPreferencesUtil;
import com.wekepler.modelslayer.OnServiceResponseListener;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.PostVideoActivity;
import com.wekepler.viewcontrollerslayer.entities.Post;

import java.io.File;
import java.util.Map;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public class UploadVideoThumbnailApiService extends BaseApiService {

    private Context mContext;
    private ProgressBar mProgressBar;

    public UploadVideoThumbnailApiService(Context context, int videoId, ProgressBar progressBar, OnServiceResponseListener onServiceResponseListener) {
        super(context, context.getString(R.string.upload_video_thumbnail_uri), onServiceResponseListener);
        setUrl(String.format(getUrl(), "" + videoId));
        this.mContext = context;
        this.mProgressBar = progressBar;
    }

    @Override
    public void connect(Object... objects) {
        String imageTitle = (String) objects[0];
        File videoFile = (File) objects[1];
        StringBuilder url = new StringBuilder(getUrl());
        url.append("?");
        setUrl(url.toString());
        int loginType = SharedPreferencesUtil.getInstance(mContext)
                .getInt(Constants.LOGIN_TYPE_TAG, Constants.LOGIN_TYPE_ACCOUNT_CODE);
        Map<String, String> headers = getBaseHeaders(loginType);

        upload(getUrl(), videoFile, imageTitle, headers.get(mContext.getString(R.string.authorization)));
    }


    @Override
    public void onResponse(Object response) {
        mOnServiceResponseListener.onSuccess();
    }


    public void upload(String url, File videoFile, String imageTitle, String token) {
        final UploadRequest request = new UploadRequest(mContext,
                "custom-upload-id", url);

        request.addFileToUpload(videoFile.getAbsolutePath(),
                "uploaded_file",
                "custom-file-name.extension",
                "content-type");

        request.addHeader(mContext.getString(R.string.authorization), token);

        //configure the notification
        request.setNotificationConfig(android.R.drawable.ic_menu_upload,
                "upload video: " + imageTitle,
                "upload in progress text",
                "upload completed successfully text",
                "upload error text",
                false);

        // set the intent to perform when the user taps on the upload notification.
        // currently tested only with intents that launches an activity
        // if you comment this line, no action will be performed when the user taps on the notification
        request.setNotificationClickIntent(new Intent(mContext, PostVideoActivity.class));

        // set the maximum number of automatic upload retries on error
        request.setMaxRetries(2);

        try {
            //Start upload service and display the notification
            UploadService.startUpload(request);

        } catch (Exception exc) {
            //You will end up here only if you pass an incomplete UploadRequest
            Log.e("AndroidUploadService", exc.getLocalizedMessage(), exc);
        }
    }

    private static final String TAG = "AndroidUploadService";
    private final AbstractUploadServiceReceiver mUploadReceiver =
            new AbstractUploadServiceReceiver() {

                // you can override this progress method if you want to get
                // the completion progress in percent (0 to 100)
                // or if you need to know exactly how many bytes have been transferred
                // override the method below this one
                @Override
                public void onProgress(String uploadId, int progress) {
                    Log.i(TAG, "The progress of the upload with ID "
                            + uploadId + " is: " + progress);
                    mProgressBar.setProgress(progress);
                }

                @Override
                public void onProgress(final String uploadId, final long uploadedBytes, final long totalBytes) {
                    Log.i(TAG, "Upload with ID "
                            + uploadId + " uploaded bytes: " + uploadedBytes + ", total: " + totalBytes);
                }

                @Override
                public void onError(String uploadId, Exception exception) {
                    Log.e(TAG, "Error in upload with ID: " + uploadId + ". "
                            + exception.getLocalizedMessage(), exception);
                }

                @Override
                public void onCompleted(String uploadId,
                                        int serverResponseCode,
                                        String serverResponseMessage) {
                    Log.i(TAG, "Upload with ID " + uploadId
                            + " has been completed with HTTP " + serverResponseCode
                            + ". Response from server: " + serverResponseMessage);

                    if (serverResponseCode == 200 || serverResponseCode == 201) {
                        mOnServiceResponseListener.onSuccess(mGson.fromJson(serverResponseMessage, Post.class));
                    } else {
                        mOnServiceResponseListener.onError(serverResponseCode, serverResponseMessage);
                    }
                    //If your server responds with a JSON, you can parse it
                    //from serverResponseMessage string using a library
                    //such as org.json (embedded in Android) or google's gson
                }
            };

    public void registerUploadServiceReceiver() {
        mUploadReceiver.register(mContext);
    }

    public void unregisterUploadServiceReceiver() {
        mUploadReceiver.unregister(mContext);
    }
}
