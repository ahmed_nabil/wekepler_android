package com.wekepler.serviceslayer;

import android.content.Context;
import android.widget.ProgressBar;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.wekepler.serviceslayer.apis.BaseApiService;
import com.wekepler.serviceslayer.entities.requests.GsonDeleteRequest;
import com.wekepler.serviceslayer.entities.requests.GsonGetRequest;
import com.wekepler.serviceslayer.entities.requests.GsonMultipartPostRequest;
import com.wekepler.serviceslayer.entities.requests.GsonPostRequest;
import com.wekepler.serviceslayer.entities.requests.GsonPutRequest;

import java.io.File;
import java.util.Map;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public class NetworkManager {

    private static NetworkManager mNetworkManager;
    private Context mApplicationContext;
    private RequestQueue mRequestQueue;

    private NetworkManager(Context applicationContext) {
        this.mApplicationContext = applicationContext;
        mRequestQueue = Volley.newRequestQueue(applicationContext);
    }

    public static NetworkManager getInstance(Context appliationContext) {
        if (mNetworkManager == null) {
            synchronized (NetworkManager.class) {
                if (mNetworkManager == null)
                    mNetworkManager = new NetworkManager(appliationContext);
            }
        }
        return mNetworkManager;
    }

    public void sendPostRequest(BaseApiService api, Map<String, String> headersMap, int bodyContentType, String requestBody, Class responseClass, Response.Listener listener, Response.ErrorListener errorListener) {
        GsonPostRequest gsonPostRequest = new GsonPostRequest(api.getUrl(), headersMap, bodyContentType, requestBody, responseClass, listener, errorListener);
        mRequestQueue.add(gsonPostRequest);
    }

    public void sendGetRequest(BaseApiService api, Map<String, String> headersMap, Map<String, String> parameters, Class responseClass, Response.Listener listener, Response.ErrorListener errorListener) {
        if (parameters != null) {
            StringBuilder url = new StringBuilder(api.getUrl());
            url.append("?");
            for (Map.Entry entry : parameters.entrySet()) {
                url.append(entry.getKey() + "=" + entry.getValue() + "&");
            }
            api.setUrl(url.toString());
        }
        GsonGetRequest gsonGetRequest = new GsonGetRequest(api.getUrl(), headersMap, responseClass, listener, errorListener);
        mRequestQueue.add(gsonGetRequest);
    }

    public void sendPutRequest(BaseApiService api, Map<String, String> headersMap, int bodyContentType, Map<String, String> parameters, Class responseClass, Response.Listener listener, Response.ErrorListener errorListener) {
        if (parameters != null) {
            StringBuilder url = new StringBuilder(api.getUrl());
            url.append("?");
            for (Map.Entry entry : parameters.entrySet()) {
                url.append(entry.getKey() + "=" + entry.getValue() + "&");
            }
            String urlString = url.toString();
            urlString = urlString.replace(" ", "%20");
            api.setUrl(urlString);
        }
        GsonPutRequest gsonPutRequest = new GsonPutRequest(api.getUrl(), headersMap, bodyContentType, responseClass, listener, errorListener);
        mRequestQueue.add(gsonPutRequest);
    }

    public void sendDeleteRequest(BaseApiService api, Map<String, String> headersMap, int bodyContentType, Map<String, String> parameters, Class responseClass, Response.Listener listener, Response.ErrorListener errorListener) {
        if (parameters != null) {
            StringBuilder url = new StringBuilder(api.getUrl());
            url.append("?");
            for (Map.Entry entry : parameters.entrySet()) {
                url.append(entry.getKey() + "=" + entry.getValue() + "&");
            }
            String urlString = url.toString();
            urlString = urlString.replace(" ", "%20");
            api.setUrl(urlString);
        }
        GsonDeleteRequest gsonDeleteRequest = new GsonDeleteRequest(api.getUrl(), headersMap, bodyContentType, responseClass, listener, errorListener);
        mRequestQueue.add(gsonDeleteRequest);
    }
}
