package com.wekepler.serviceslayer.entities.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mohammad Sayed on 9/16/2015.
 */
public class Token {

    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("token_type")
    private String tokenType;

    @SerializedName("expires_in")
    private String expires_in;

    @SerializedName("userName")
    private String userName;

    @SerializedName(".issued")
    private String issueDateString;

    @SerializedName(".expires")
    private String expirationDateString;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(String expires_in) {
        this.expires_in = expires_in;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIssueDateString() {
        return issueDateString;
    }

    public void setIssueDateString(String issueDateString) {
        this.issueDateString = issueDateString;
    }

    public String getExpirationDateString() {
        return expirationDateString;
    }

    public void setExpirationDateString(String expirationDateString) {
        this.expirationDateString = expirationDateString;
    }
}
