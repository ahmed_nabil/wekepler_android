package com.wekepler.serviceslayer.entities.requests;

import com.android.volley.Response;

import java.util.Map;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public class GsonGetRequest<T> extends GsonRequest<T> {
    public GsonGetRequest(String url, Map headersMap, Class responseClass, Response.Listener listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, headersMap, -1, null, responseClass, listener, errorListener);
    }
}