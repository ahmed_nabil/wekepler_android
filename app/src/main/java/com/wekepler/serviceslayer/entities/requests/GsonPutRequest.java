package com.wekepler.serviceslayer.entities.requests;

import com.android.volley.Response;

import java.util.Map;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public class GsonPutRequest<T> extends GsonRequest<T> {
    public GsonPutRequest(String url, Map headersMap, int bodyContentType, Class responseClass, Response.Listener listener, Response.ErrorListener errorListener) {
        super(Method.PUT, url, headersMap, bodyContentType, null, responseClass, listener, errorListener);
    }
}