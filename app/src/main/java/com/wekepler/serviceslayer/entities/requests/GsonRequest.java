package com.wekepler.serviceslayer.entities.requests;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.wekepler.viewcontrollerslayer.Constants;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public class GsonRequest<T> extends JsonRequest<T> {

    private final Gson gson = new Gson();
    private final Class<T> mResponseClass;
    private Map<String, String> mRequestHeadersMap;
    private int mBodyContentType;

    public GsonRequest(int method, String url, Map<String, String> requestHeadersMap, int bodyContentType, String requestBody, Class<T> responseClass, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, url, requestBody, listener, errorListener);
        this.mBodyContentType = bodyContentType;
        this.mResponseClass = responseClass;
        this.mRequestHeadersMap = requestHeadersMap;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(mResponseClass == null ? null : gson.fromJson(json, mResponseClass),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    public String getBodyContentType() {
        if (mBodyContentType == Constants.BODY_CONTENT_URL_ENCODED_CODE) {
            String contentType = "application/x-www-form-urlencoded";
            return contentType;
        } else
            return super.getBodyContentType();
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        if (mRequestHeadersMap == null)
            return super.getHeaders();
        //mRequestHeadersMap = new HashMap<>();
        //mRequestHeadersMap.put("Content-Type", "application/x-www-form-urlencoded");
        return mRequestHeadersMap;
    }
}