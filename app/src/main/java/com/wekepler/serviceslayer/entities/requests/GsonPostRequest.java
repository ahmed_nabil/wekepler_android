package com.wekepler.serviceslayer.entities.requests;

import com.android.volley.Response;

import java.util.Map;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public class GsonPostRequest<T> extends GsonRequest<T> {
    public GsonPostRequest(String url, Map<String, String> requestHeadersMap, int bodyContentType, String requestBody,
                           Class responseClass, Response.Listener listener,
                           Response.ErrorListener errorListener) {
        super(Method.POST, url, requestHeadersMap, bodyContentType, requestBody, responseClass, listener, errorListener);
    }
}
