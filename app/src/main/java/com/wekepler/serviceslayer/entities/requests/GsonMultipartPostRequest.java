package com.wekepler.serviceslayer.entities.requests;

import android.widget.ProgressBar;

import com.android.volley.Response;
import com.android.volley.VolleyLog;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public class GsonMultipartPostRequest<T> extends GsonPostRequest<T> {

    private MultipartEntityBuilder mBuilder = MultipartEntityBuilder.create();
    private final File mFile;
    protected Map<String, String> headers;
    private static final String FILE_PART_NAME = "file";
    private ProgressBar mProgressBar;
    private int mMaxNumberOfBytes;
    private int mTransferredBytes = 0;

    public GsonMultipartPostRequest(String url, File file, Map<String, String> requestHeadersMap, int bodyContentType, String requestBody, Class responseClass, Response.Listener listener, Response.ErrorListener errorListener, ProgressBar progressBar) {
        super(url, requestHeadersMap, bodyContentType, requestBody, responseClass, listener, errorListener);
        this.mFile = file;
        buildMultipartEntity();
        this.mProgressBar = progressBar;
        if (mProgressBar != null)
            mProgressBar.setMax(100);
    }

    private void buildMultipartEntity() {
        mBuilder.addBinaryBody(FILE_PART_NAME, mFile, ContentType.create("video/*"), mFile.getName());
        //mBuilder.setLaxMode().setBoundary("xx").setCharset(Charset.forName("UTF-8"));
        mBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        mBuilder.addBinaryBody("upload_file", mFile);
    }

    @Override
    public String getBodyContentType() {
        String contentTypeHeader = mBuilder.build().getContentType().getValue();
        return contentTypeHeader;
    }

    @Override
    public byte[] getBody() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            if (mProgressBar != null)
                mProgressBar.setMax((int) mBuilder.build().getContentLength());
            mBuilder.build().writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream bos, building the multipart request.");
        }
        return bos.toByteArray();
    }

    private class CustomByteArrayOutputStream extends ByteArrayOutputStream {

        @Override
        public synchronized void write(byte[] buffer, int offset, int len) {
            super.write(buffer, offset, len);
            if (mProgressBar != null) {
                mTransferredBytes += len;
                mProgressBar.setProgress(mTransferredBytes);
            }

        }
    }
}
