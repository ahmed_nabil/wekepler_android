package com.wekepler.serviceslayer.entities.requests;

import com.android.volley.Response;

import java.util.Map;

/**
 * Created by Mohammad Sayed on 8/18/2015.
 */
public class GsonDeleteRequest<T> extends GsonRequest<T> {
    public GsonDeleteRequest(String url, Map headersMap, int bodyContentType, Class responseClass, Response.Listener listener, Response.ErrorListener errorListener) {
        super(Method.DELETE, url, headersMap, bodyContentType, null, responseClass, listener, errorListener);
    }
}