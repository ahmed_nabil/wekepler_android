package com.wekepler.serviceslayer;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ProgressBar;

import com.wekepler.R;
import com.wekepler.SharedPreferencesUtil;
import com.wekepler.modelslayer.OnServiceResponseListener;
import com.wekepler.serviceslayer.entities.responses.Token;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.parameters.UploadVideoParameters;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mohammad Sayed on 9/23/2015.
 */
public class UploadFileAsynctask extends AsyncTask<Void, Integer, String> {

    private Context mContext;
    private String mUrlString;
    private File mSourceFile;
    private List<NameValuePair> mParams;
    /*
    private String mTitle;
    private String mVideoDescription;
    private double mLatitude;
    private double mLongitude;
*/
    private ProgressBar mProgressBar;
    private OnServiceResponseListener mOnServiceResponseListener;
    private final String SUCCESS_TAG = "Success";
    private int mBytesTransferred;


    public UploadFileAsynctask(Context context, String urlString, File file, ProgressBar progressBar,
                               UploadVideoParameters uploadVideoParameters, OnServiceResponseListener onServiceResponseListener) {
        this.mContext = context;
        this.mUrlString = urlString;
        this.mSourceFile = file;
        this.mProgressBar = progressBar;
        this.mOnServiceResponseListener = onServiceResponseListener;
        //mParams = mapToNameValuePairList(uploadVideoParameters);
        /*this.mTitle = title;
        this.mVideoDescription = videoDescription;
        this.mLatitude = latitude;
        this.mLongitude = longitude;*/
    }

    /*private List<NameValuePair> mapToNameValuePairList(UploadVideoParameters parameters) {
        List<NameValuePair> pairs = new ArrayList<>();
        pairs.add(new BasicNameValuePair("Title", (String) parameters.getParametersMap().get("Title")));
        pairs.add(new BasicNameValuePair("CommentText", (String) parameters.getParametersMap().get("CommentText")));
        pairs.add(new BasicNameValuePair("latitude", Double.toString((double) parameters.getParametersMap().get("latitude"))));
        pairs.add(new BasicNameValuePair("longitude", Double.toString((double) parameters.getParametersMap().get("longitude"))));
        return pairs;
    }*/

    @Override
    protected void onPreExecute() {
        mProgressBar.setProgress(0);
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        mProgressBar.setProgress(progress[0]);
    }

    @Override
    protected String doInBackground(Void... params) {
        return upload();
    }

    private String upload() {
        String response = "no";
        if (!mSourceFile.isFile()) {
            return "not a file";
        }

        String tokenString;

        int loginType = SharedPreferencesUtil.getInstance(mContext).getInt(Constants.LOGIN_TYPE_TAG, Constants.LOGIN_TYPE_ACCOUNT_CODE);
        if (loginType == Constants.LOGIN_TYPE_ACCOUNT_CODE) {
            Token token = SharedPreferencesUtil.getInstance(mContext).getToken();
            tokenString = token.getAccessToken();
        } else
            tokenString = SharedPreferencesUtil.getInstance(mContext).getString(Constants.TOKEN_STRING_TAG, null);

        if (tokenString == null)
            return "token_invalid";

        /*HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(mUrlString);
        httppost.addHeader(mContext.getString(R.string.authorization), "bearer " + tokenString);*/


        try {

            MultipartEntityBuilder mBuilder = MultipartEntityBuilder.create();
            mBuilder.addBinaryBody("file", mSourceFile, ContentType.create("video/*"), mSourceFile.getName());
            mBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            mBuilder.addBinaryBody("upload_file", mSourceFile);
            HttpEntity entity = mBuilder.build();

            String boundary = "*****";
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024;

            HttpURLConnection httpUrlConnection = null;
            URL url = new URL(mUrlString);
            httpUrlConnection = (HttpURLConnection) url.openConnection();
            httpUrlConnection.setUseCaches(false);
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            httpUrlConnection.setChunkedStreamingMode(1024);
            httpUrlConnection.setRequestMethod("POST");
            httpUrlConnection.setRequestProperty("Connection", "Keep-Alive");
            httpUrlConnection.setRequestProperty("Content-Type",
                    "multipart/form-data; boundary=" + boundary);
            httpUrlConnection.setRequestProperty(mContext.getString(R.string.authorization), "bearer " + tokenString);

            //httpUrlConnection.setRequestProperty("Cache-Control", "no-cache");
            //httpUrlConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + this.boundary);

            DataOutputStream outputStream = new DataOutputStream(httpUrlConnection.getOutputStream());
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(outputStream, "UTF-8"));
            writer.write(getQuery(mParams));

            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
            FileInputStream fileInputStream = new FileInputStream(new File(
                    mSourceFile.getAbsolutePath()));

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // Read file
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            Log.d("Video length", bytesAvailable + "");

            mBytesTransferred = 0;

            try {
                while (bytesRead > 0) {
                    try {
                        outputStream.write(buffer, 0, bufferSize);
                        mBytesTransferred += bufferSize;
                        // Here you can call the method which updates progress
                        // be sure to wrap it so UI-updates are done on the main thread!
                        publishProgress(100 * mBytesTransferred / bytesAvailable);

                    } catch (OutOfMemoryError e) {
                        e.printStackTrace();
                        return "outofmemoryerror";
                    }
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }

            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + twoHyphens
                    + lineEnd);

            // Responses from the server (code and message)
            int serverResponseCode = httpUrlConnection.getResponseCode();
            String serverResponseMessage = httpUrlConnection.getResponseMessage();
            Log.d("Server Response Code", " " + serverResponseCode);
            Log.d("Server Response Message", serverResponseMessage);

            if (serverResponseCode == 200) {
                response = SUCCESS_TAG;
            } else {
                response = "false";
            }

            fileInputStream.close();
            outputStream.flush();

            //for android InputStream is = connection.getInputStream();
            InputStream is = httpUrlConnection.getInputStream();

            int ch;
            StringBuffer b = new StringBuffer();
            while ((ch = is.read()) != -1) {
                b.append((char) ch);
            }

            response = b.toString();
            Log.d("response string is", response); //Here is the actual output

            outputStream.close();
            outputStream = null;

        } catch (Exception ex) {
            // Exception handling
            response = "error";
            Log.d("Send file Exception", ex.getMessage());
            ex.printStackTrace();
        }


            /*CustomMultipart entity = new CustomMultipart(new CustomMultipart.ProgressListener() {

                @Override
                public void transferred(long num) {
                    publishProgress((int) ((num / (float) mTotalSize) * 100));
                }
            });


            /*entity.addPart("type", new StringBody("video"));
            entity.addPart("uploadedfile", new FileBody(mSourceFile));


            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            entity.writeTo(baos);
            baos.close();
            byte[] payload = baos.toByteArray();
            baos = null;
            OutputStream os = httpUrlConnection.getOutputStream();

            mTotalSize = payload.length;
            mBytesTransferred = 0;
            int chunkSize = 2000;

            while (mBytesTransferred < mTotalSize) {
                int nextChunkSize = mTotalSize - mBytesTransferred;
                if (nextChunkSize > chunkSize) {
                    nextChunkSize = chunkSize;
                }
                os.write(payload, mBytesTransferred, nextChunkSize); // TODO check outcome!
                mBytesTransferred += nextChunkSize;

                // Here you can call the method which updates progress
                // be sure to wrap it so UI-updates are done on the main thread!
                publishProgress(100 * mBytesTransferred / mTotalSize);
            }
            os.close();

            Log.d("Response Code", "" + httpUrlConnection.getResponseCode());
            Log.d("Response Message", httpUrlConnection.getResponseMessage());
            InputStream is = null;
            try {
                is = httpUrlConnection.getInputStream();
                int ch;
                StringBuffer sb = new StringBuffer();
                while ((ch = is.read()) != -1) {
                    sb.append((char) ch);
                }
                return SUCCESS_TAG;
                //return sb.toString();
            } catch (IOException e) {
                return e.getMessage();
            } finally {
                if (is != null) {
                    is.close();
                }
            }

            //mTotalSize = entity.getContentLength();
            //httppost.setEntity(entity);
            /*HttpResponse response = httpclient.execute(httppost);
            HttpEntity r_entity = response.getEntity();
            responseString = EntityUtils.toString(r_entity);

        } catch (IOException e) {
            responseString = e.toString();
        }*/

        return response;

    }


    @Override
    protected void onPostExecute(String result) {
        if (result.equals(SUCCESS_TAG))
            mOnServiceResponseListener.onSuccess();
        else
            mOnServiceResponseListener.onError(2, result);
    }


    private String getQuery(List<NameValuePair> params) {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        try {
            for (NameValuePair pair : params) {
                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "error";
        }

        return result.toString();
    }

}
