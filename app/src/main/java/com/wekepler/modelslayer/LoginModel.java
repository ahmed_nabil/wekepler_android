package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.LoginApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.requests.LoginRequest;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class LoginModel extends BaseModel {

    private LoginApiService mLoginApiService;

    public LoginModel(Context context, LoginRequest loginRequest, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.LOGIN_TAG);
        mLoginApiService = new LoginApiService(context, this);
        mLoginApiService.connect(loginRequest);
    }

    @Override
    public void onSuccess(Object... objects) {
        String operationTag = (String) objects[0];
        if (operationTag.equals(Constants.DATA_RETRIEVED_TAG))
            mLoginApiService.saveToken();
        else if (operationTag.equals(Constants.DATA_STORED_TAG))
            mOnModelResponseListener.onSuccess(Constants.LOGIN_TAG);
    }
}
