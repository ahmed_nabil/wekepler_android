package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.FeedOfCurrentUserApiService;
import com.wekepler.serviceslayer.apis.FriendVideosApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.Post;
import com.wekepler.viewcontrollerslayer.entities.parameters.VideosParameters;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class FriendVideosModel extends BaseModel {

    public FriendVideosModel(Context context, int friendId, VideosParameters videosParameters, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.VIDEOS_OF_FRIEND_TAG);
        FriendVideosApiService friendVideosApiService = new FriendVideosApiService(context, friendId, this);
        friendVideosApiService.connect(videosParameters);
    }

    @Override
    public void onSuccess(Object... objects) {
        ArrayList<Post> postsList = new ArrayList<Post>(Arrays.asList((Post[]) objects[0]));
        mOnModelResponseListener.onSuccess(Constants.VIDEOS_OF_FRIEND_TAG, postsList);
    }
}
