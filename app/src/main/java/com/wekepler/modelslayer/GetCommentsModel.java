package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.FeedOfCurrentUserApiService;
import com.wekepler.serviceslayer.apis.GetCommentsApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.Comment;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.Post;
import com.wekepler.viewcontrollerslayer.entities.parameters.VideosParameters;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class GetCommentsModel extends BaseModel {

    public GetCommentsModel(Context context, int videoId, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.GET_COMMENTS_TAG);
        GetCommentsApiService getCommentsApiService = new GetCommentsApiService(context, videoId, this);
        getCommentsApiService.connect();
    }

    @Override
    public void onSuccess(Object... objects) {
        ArrayList<Comment> commentsList = new ArrayList<>(Arrays.asList((Comment[]) objects[0]));
        mOnModelResponseListener.onSuccess(Constants.GET_COMMENTS_TAG, commentsList);
    }
}
