package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.DeleteVideoApiService;
import com.wekepler.serviceslayer.apis.GetCommentsApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.Comment;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class DeleteVideoModel extends BaseModel {

    public DeleteVideoModel(Context context, int videoId, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.DELETE_VIDEO_TAG);
        DeleteVideoApiService deleteVideoApiService = new DeleteVideoApiService(context, videoId, this);
        deleteVideoApiService.connect();
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.DELETE_VIDEO_TAG);
    }
}
