package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.BlockVideoApiService;
import com.wekepler.serviceslayer.apis.ReportVideoApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.Post;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class ReportVideoModel extends BaseModel {

    public ReportVideoModel(Context context, Post post, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.REPORT_VIDEO_TAG);
        ReportVideoApiService blockVideoApiService = new ReportVideoApiService(context, post.getId(), this);
        blockVideoApiService.connect();
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.REPORT_VIDEO_TAG);
    }

    @Override
    public void onError(int errorCode, String errorMessage) {
        int code = errorCode;
        String message = errorMessage;
        mOnModelResponseListener.onError(mOperationTag, "Code: " + errorCode + "\n" + "Message: " + errorMessage);
    }
}
