package com.wekepler.modelslayer;

import android.content.Context;
import android.widget.ImageButton;

import com.wekepler.serviceslayer.apis.LikeVideoApiService;
import com.wekepler.serviceslayer.apis.RepostVideoApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.adapters.PostItemHolder;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.Post;
import com.wekepler.viewcontrollerslayer.entities.requests.RepostVideoRequest;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class RepostVideoModel extends BaseModel {

    private Post mPost;

    public RepostVideoModel(Context context, Post post, RepostVideoRequest repostVideoRequest, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.REPOST_VIDEO_TAG);
        this.mPost = post;
        RepostVideoApiService repostVideoApiService = new RepostVideoApiService(context, repostVideoRequest, this);
        repostVideoApiService.connect();
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.REPOST_VIDEO_TAG, mPost);
    }

    @Override
    public void onError(int errorCode, String errorMessage) {
        int code = errorCode;
        String message = errorMessage;
        mOnModelResponseListener.onError(mOperationTag, "Code: " + errorCode + "\n" + "Message: " + errorMessage, mPost);
    }
}
