package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.FollowUserApiService;
import com.wekepler.serviceslayer.apis.LikeVideoApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.adapters.PostItemHolder;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.Post;
import com.wekepler.viewcontrollerslayer.entities.User;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class FollowUserModel extends BaseModel {

    private Post mPost;
    private PostItemHolder mPostItemHolder;

    public FollowUserModel(Context context, User user, Post post, PostItemHolder postItemHolder, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.FOLLOW_USER_TAG);
        this.mPost = post;
        this.mPostItemHolder = postItemHolder;
        String status = user.isFollowed() ? Constants.UNFOLLOW : Constants.FOLLOW;
        FollowUserApiService followUserApiService =
                new FollowUserApiService(context, post.getUser().getUserId(), status, this);
        followUserApiService.connect();
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.FOLLOW_USER_TAG, mPost);
    }

    @Override
    public void onError(int errorCode, String errorMessage) {
        int code = errorCode;
        String message = errorMessage;
        mOnModelResponseListener.onError(mOperationTag, "Code: " + errorCode + "\n" + "Message: " + errorMessage, mPost, mPostItemHolder);
    }
}
