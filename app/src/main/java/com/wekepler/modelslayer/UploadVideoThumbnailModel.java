package com.wekepler.modelslayer;

import android.content.Context;
import android.widget.ProgressBar;

import com.wekepler.serviceslayer.apis.UploadVideoThumbnailApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.parameters.UploadVideoParameters;

import java.io.File;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class UploadVideoThumbnailModel extends BaseModel {

    private UploadVideoThumbnailApiService mUploadVideoThumbnailApiService;

    public UploadVideoThumbnailModel(Context context, int videoId, String imageTitle, File videoFile, OnModelResponseListener onModelResponseListener, ProgressBar progressBar) {
        super(onModelResponseListener, Constants.UPLOAD_VIDEO_THUMBNAIL_TAG);
        mUploadVideoThumbnailApiService = new UploadVideoThumbnailApiService(context, videoId, progressBar, this);
        registerUploadReceiver();
        mUploadVideoThumbnailApiService.connect(imageTitle, videoFile);
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.UPLOAD_VIDEO_THUMBNAIL_TAG);
    }

    public void registerUploadReceiver() {
        mUploadVideoThumbnailApiService.registerUploadServiceReceiver();
    }

    public void unregisterUploadReceiver() {
        mUploadVideoThumbnailApiService.unregisterUploadServiceReceiver();
    }

}
