package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.LoginApiService;
import com.wekepler.serviceslayer.apis.RegisterExternalApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.requests.RegisterExternalRequest;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class RegisterExternalModel extends BaseModel {

    private RegisterExternalApiService mRegisterExternalApiService;

    public RegisterExternalModel(Context context, RegisterExternalRequest registerExternalRequest, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.REGISTER_EXTERNAL_TAG);
        mRegisterExternalApiService = new RegisterExternalApiService(context, this);
        mRegisterExternalApiService.connect(registerExternalRequest);
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.REGISTER_EXTERNAL_TAG);
    }
}
