package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.AddCommentToVideoApiService;
import com.wekepler.serviceslayer.apis.GetUserProfileApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class GetUserProfileModel extends BaseModel {


    public GetUserProfileModel(Context context, int userId, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.GET_USER_PROFILE_TAG);
        GetUserProfileApiService getUserProfileApiService = new GetUserProfileApiService(context, userId, this);
        getUserProfileApiService.connect();
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.GET_USER_PROFILE_TAG, objects[0]);
    }
}
