package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.AddCommentToVideoApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.requests.AddCommentRequest;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class AddCommentToVideoModel extends BaseModel {


    public AddCommentToVideoModel(Context context, AddCommentRequest addCommentRequest, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.ADD_COMMENT_VIDEO_TAG);
        AddCommentToVideoApiService addCommentToVideoApiService = new AddCommentToVideoApiService(context, addCommentRequest.getVideoId(), this);
        addCommentToVideoApiService.connect(addCommentRequest);
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.ADD_COMMENT_VIDEO_TAG);
    }
}
