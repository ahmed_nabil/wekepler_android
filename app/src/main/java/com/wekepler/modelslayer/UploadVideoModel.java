package com.wekepler.modelslayer;

import android.content.Context;
import android.widget.ProgressBar;

import com.wekepler.serviceslayer.apis.UploadVideoApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.parameters.UploadVideoParameters;

import java.io.File;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class UploadVideoModel extends BaseModel {

    private UploadVideoApiService mUploadVideoApiService;

    public UploadVideoModel(Context context, UploadVideoParameters uploadVideoParameters, File videoFile, OnModelResponseListener onModelResponseListener, ProgressBar progressBar) {
        super(onModelResponseListener, Constants.UPLOAD_VIDEO_TAG);
        mUploadVideoApiService = new UploadVideoApiService(context, progressBar, this);
        registerUploadReceiver();
        mUploadVideoApiService.connect(uploadVideoParameters, videoFile);
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.UPLOAD_VIDEO_TAG, objects[0]);
    }

    public void registerUploadReceiver() {
        mUploadVideoApiService.registerUploadServiceReceiver();
    }

    public void unregisterUploadReceiver() {
        mUploadVideoApiService.unregisterUploadServiceReceiver();
    }

}
