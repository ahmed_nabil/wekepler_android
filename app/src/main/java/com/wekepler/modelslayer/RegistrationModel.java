package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.LoginApiService;
import com.wekepler.serviceslayer.apis.RegistrationApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.requests.LoginRequest;
import com.wekepler.viewcontrollerslayer.entities.requests.RegistrationRequest;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class RegistrationModel extends BaseModel {

    public RegistrationModel(Context context, RegistrationRequest registrationRequest, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.REGISTRATION_TAG);

        RegistrationApiService registrationApiService = new RegistrationApiService(context, this);
        registrationApiService.connect(registrationRequest);
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.REGISTRATION_TAG);
    }
}
