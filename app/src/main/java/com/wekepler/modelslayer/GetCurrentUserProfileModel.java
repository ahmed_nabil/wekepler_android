package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.GetCurrentUserProfileApiService;
import com.wekepler.serviceslayer.apis.GetUserProfileApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class GetCurrentUserProfileModel extends BaseModel {


    public GetCurrentUserProfileModel(Context context, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.GET_CURRENT_USER_PROFILE_TAG);
        GetCurrentUserProfileApiService getCurrentUserProfileApiService = new GetCurrentUserProfileApiService(context, this);
        getCurrentUserProfileApiService.connect();
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.GET_CURRENT_USER_PROFILE_TAG, objects[0]);
    }
}
