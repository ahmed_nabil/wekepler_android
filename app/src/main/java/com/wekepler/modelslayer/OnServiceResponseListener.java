package com.wekepler.modelslayer;

import com.wekepler.serviceslayer.entities.ServerError;

/**
 * Created by Mohammad Sayed on 8/29/2015.
 */
public interface OnServiceResponseListener {
    public void onSuccess(Object... objects);

    public void onError(int errorCode, String errorMessage);
}
