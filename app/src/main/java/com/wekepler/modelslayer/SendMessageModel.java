package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.AddCommentToVideoApiService;
import com.wekepler.serviceslayer.apis.SendMessageApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.Message;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.requests.AddCommentRequest;
import com.wekepler.viewcontrollerslayer.entities.requests.SendMessageRequest;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class SendMessageModel extends BaseModel {


    public SendMessageModel(Context context, SendMessageRequest sendMessageRequest, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.SEND_MESSAGE_TAG);
        SendMessageApiService sendMessageApiService = new SendMessageApiService(context, this);
        sendMessageApiService.connect(sendMessageRequest);
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.SEND_MESSAGE_TAG, (Message) objects[0]);
    }
}
