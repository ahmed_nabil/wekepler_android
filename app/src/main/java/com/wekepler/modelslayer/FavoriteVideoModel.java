package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.FavoriteVideoApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.Post;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class FavoriteVideoModel extends BaseModel {

    public FavoriteVideoModel(Context context, Post post, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.FAVORITE_VIDEO_TAG);
        FavoriteVideoApiService favoriteVideoApiService = new FavoriteVideoApiService(context, post.getId(), this);
        favoriteVideoApiService.connect();
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.FAVORITE_VIDEO_TAG);
    }

    @Override
    public void onError(int errorCode, String errorMessage) {
        int code = errorCode;
        String message = errorMessage;
        mOnModelResponseListener.onError(mOperationTag, "Code: " + errorCode + "\n" + "Message: " + errorMessage);
    }
}
