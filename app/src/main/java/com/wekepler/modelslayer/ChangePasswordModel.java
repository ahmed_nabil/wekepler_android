package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.ChangePasswordApiService;
import com.wekepler.serviceslayer.apis.GetCurrentUserProfileApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.requests.ChangePasswordRequest;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class ChangePasswordModel extends BaseModel {


    public ChangePasswordModel(Context context, ChangePasswordRequest changePasswordRequest, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.CHANGE_PASSWORD_TAG);
        ChangePasswordApiService changePasswordApiService = new ChangePasswordApiService(context, this);
        changePasswordApiService.connect(changePasswordRequest);
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.CHANGE_PASSWORD_TAG);
    }
}
