package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.DeleteCommentApiService;
import com.wekepler.serviceslayer.apis.EditCommentApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.parameters.EditCommentParameters;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class DeleteCommentModel extends BaseModel {


    public DeleteCommentModel(Context context, int commentId, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.DELETE_COMMENT_TAG);
        DeleteCommentApiService editCommentApiService = new DeleteCommentApiService(context, commentId, this);
        editCommentApiService.connect();
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.DELETE_COMMENT_TAG);
    }
}
