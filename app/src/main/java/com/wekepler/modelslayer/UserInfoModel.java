package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.UserInfoApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class UserInfoModel extends BaseModel {

    public UserInfoModel(Context context, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.USER_INFO_TAG);

        UserInfoApiService userInfoApiService = new UserInfoApiService(context, this);
        userInfoApiService.connect();
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.USER_INFO_TAG, objects[0]);
    }
}
