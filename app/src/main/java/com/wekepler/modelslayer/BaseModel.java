package com.wekepler.modelslayer;

import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;

/**
 * Created by Mohammad Sayed on 8/29/2015.
 */
public abstract class BaseModel implements OnServiceResponseListener {

    protected String mOperationTag;
    protected OnModelResponseListener mOnModelResponseListener;

    public BaseModel(OnModelResponseListener onModelResponseListener, String operationTag) {
        this.mOnModelResponseListener = onModelResponseListener;
        this.mOperationTag = operationTag;
    }


    @Override
    public void onError(int errorCode, String errorMessage) {
        int code = errorCode;
        String message = errorMessage;
        mOnModelResponseListener.onError(mOperationTag, "Code: " + errorCode + "\n" + "Message: " + errorMessage);
    }

    public void setOperationTag(String operationTag) {
        this.mOperationTag = operationTag;
    }
}
