package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.GetCommentsApiService;
import com.wekepler.serviceslayer.apis.GetSentMessagesApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.Comment;
import com.wekepler.viewcontrollerslayer.entities.Message;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class GetSentMessagesModel extends BaseModel {

    public GetSentMessagesModel(Context context, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.GET_SENT_MESSAGES_TAG);
        GetSentMessagesApiService getCommentsApiService = new GetSentMessagesApiService(context, this);
        getCommentsApiService.connect();
    }

    @Override
    public void onSuccess(Object... objects) {
        //ArrayList<Comment> commentsList = new ArrayList<>(Arrays.asList((Comment[]) objects[0]));
        Message[] messages = (Message[]) objects[0];
        ArrayList<Message> messagesList = new ArrayList<>(Arrays.asList(messages));
        mOnModelResponseListener.onSuccess(Constants.GET_SENT_MESSAGES_TAG, (ArrayList) messagesList);
    }
}
