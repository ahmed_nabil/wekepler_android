package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.AddCommentToVideoApiService;
import com.wekepler.serviceslayer.apis.EditCommentApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.parameters.EditCommentParameters;
import com.wekepler.viewcontrollerslayer.entities.requests.AddCommentRequest;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class EditCommentModel extends BaseModel {


    public EditCommentModel(Context context, int commentId, EditCommentParameters editCommentParameters, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.EDIT_COMMENT_TAG);
        EditCommentApiService editCommentApiService = new EditCommentApiService(context, commentId, this);
        editCommentApiService.connect(editCommentParameters);
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.EDIT_COMMENT_TAG);
    }
}
