package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.LikeVideoApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.adapters.PostItemHolder;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.Post;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class LikeVideoModel extends BaseModel {

    private Post mPost;
    private PostItemHolder mPostItemHolder;

    public LikeVideoModel(Context context, Post post, PostItemHolder postItemHolder, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.LIKE_VIDEO_TAG);
        this.mPost = post;
        this.mPostItemHolder = postItemHolder;
        LikeVideoApiService likeVideoApiService = new LikeVideoApiService(context, post.getId(), this);
        likeVideoApiService.connect();
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.LIKE_VIDEO_TAG, mPost);
    }

    @Override
    public void onError(int errorCode, String errorMessage) {
        int code = errorCode;
        String message = errorMessage;
        mOnModelResponseListener.onError(mOperationTag, "Code: " + errorCode + "\n" + "Message: " + errorMessage, mPost, mPostItemHolder);
    }
}
