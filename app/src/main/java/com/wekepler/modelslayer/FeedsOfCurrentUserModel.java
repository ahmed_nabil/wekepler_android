package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.FeedOfCurrentUserApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.Post;
import com.wekepler.viewcontrollerslayer.entities.parameters.VideosParameters;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class FeedsOfCurrentUserModel extends BaseModel {

    public FeedsOfCurrentUserModel(Context context, VideosParameters videosParameters, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.FEEDS_OF_CURRENT_USER_TAG);
        FeedOfCurrentUserApiService feedOfCurrentUserApiService = new FeedOfCurrentUserApiService(context, this);
        feedOfCurrentUserApiService.connect(videosParameters);
    }

    @Override
    public void onSuccess(Object... objects) {
        ArrayList<Post> postsList = new ArrayList<Post>(Arrays.asList((Post[]) objects[0]));
        mOnModelResponseListener.onSuccess(Constants.FEEDS_OF_CURRENT_USER_TAG, postsList);
    }
}
