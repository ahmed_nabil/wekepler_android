package com.wekepler.modelslayer;

import android.content.Context;

import com.wekepler.serviceslayer.apis.BlockVideoApiService;
import com.wekepler.serviceslayer.apis.LikeVideoApiService;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.adapters.PostItemHolder;
import com.wekepler.viewcontrollerslayer.entities.OnModelResponseListener;
import com.wekepler.viewcontrollerslayer.entities.Post;

/**
 * Created by Mohammad Sayed on 9/4/2015.
 */
public class BlockVideoModel extends BaseModel {

    public BlockVideoModel(Context context, Post post, OnModelResponseListener onModelResponseListener) {
        super(onModelResponseListener, Constants.BLOCK_VIDEO_TAG);
        BlockVideoApiService blockVideoApiService = new BlockVideoApiService(context, post.getId(), this);
        blockVideoApiService.connect();
    }

    @Override
    public void onSuccess(Object... objects) {
        mOnModelResponseListener.onSuccess(Constants.BLOCK_VIDEO_TAG);
    }

    @Override
    public void onError(int errorCode, String errorMessage) {
        int code = errorCode;
        String message = errorMessage;
        mOnModelResponseListener.onError(mOperationTag, "Code: " + errorCode + "\n" + "Message: " + errorMessage);
    }
}
