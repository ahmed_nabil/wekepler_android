package com.wekepler;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.wekepler.serviceslayer.entities.responses.Token;
import com.wekepler.viewcontrollerslayer.Constants;
import com.wekepler.viewcontrollerslayer.entities.User;

/**
 * Created by Mohammad Sayed on 9/12/2015.
 */
public class SharedPreferencesUtil {

    private static SharedPreferencesUtil mSharedPreferencesUtil;
    private Context mApplicationContext;
    private final String SHARED_PREFERENCES_NAME = "wekepler_shared_preferences";
    private SharedPreferences mSharedPreferences;
    private Gson mGson;

    private SharedPreferencesUtil(Context applicationContext) {
        this.mApplicationContext = applicationContext;
        mSharedPreferences = mApplicationContext.getSharedPreferences(SHARED_PREFERENCES_NAME, mApplicationContext.MODE_PRIVATE);
        mGson = new Gson();
    }

    public static SharedPreferencesUtil getInstance(Context context) {
        if (mSharedPreferencesUtil == null) {
            synchronized (SharedPreferencesUtil.class) {
                if (mSharedPreferencesUtil == null)
                    mSharedPreferencesUtil = new SharedPreferencesUtil(context.getApplicationContext());
            }
        }
        return mSharedPreferencesUtil;
    }

    public void putString(String key, String value) {
        mSharedPreferences.edit().putString(key, value).commit();
    }

    public String getString(String key, String defaultValue) {
        return mSharedPreferences.getString(key, defaultValue);
    }

    public void putInt(String key, int value) {
        mSharedPreferences.edit().putInt(key, value).commit();
    }

    public int getInt(String key, int defaultValue) {
        return mSharedPreferences.getInt(key, defaultValue);
    }


    public void putToken(Token token) {
        putString(Constants.TOKEN_TAG, mGson.toJson(token));
    }

    public Token getToken() {
        String tokenString = getString(Constants.TOKEN_TAG, null);
        if (tokenString == null)
            return null;
        else
            return mGson.fromJson(tokenString, Token.class);
    }

    public void clearDataForLogout() {
        mSharedPreferences.edit().clear().commit();
    }

    /*public void putUser(User user) {
        mGson.
    }*/

}
